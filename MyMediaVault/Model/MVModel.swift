//
//  MVModel.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit
import RealmSwift

class MVModel: NSObject {

}

class FolderLocal: Object {
    @objc dynamic var identifier_unique = UUID().uuidString

    @objc dynamic var can_delete:Bool = true

    @objc dynamic var display_name:String? = nil

    @objc dynamic var folder_passcode:String? = nil
    @objc dynamic var folder_type:String = FolderType.normal.rawValue

    @objc dynamic var date_created:Date? = Date()
    @objc dynamic var date_updated:Date? = Date()
    @objc dynamic var is_fake:Bool =  SVSharedClass.shared.isFakePasscode

    var folder_typeEnum: FolderType {
      get {
        return (FolderType.init(rawValue: folder_type)) ?? FolderType.normal
      }
      set {
        folder_type = newValue.rawValue
      }
    }
    
    class var Main_folder:FolderLocal?{
        let realm = try! Realm()
        let result = realm.objects(FolderLocal.self).filter("is_fake == \(SVSharedClass.shared.isFakePasscode)")
        let arrFolder = Array(result).sorted(by: { (obj1, obj2) -> Bool in
            return obj1.date_created! < obj2.date_created!
        })

        let folderMain =  arrFolder.first(where: { (folder) -> Bool in
            return folder.folder_typeEnum == .main
        })
        
        return folderMain

    }
    class var arrFolderLocked:[FolderLocal]{
        let realm = try! Realm()
        let result = realm.objects(FolderLocal.self).filter("is_fake == \(SVSharedClass.shared.isFakePasscode)")
        var arrFolder = Array(result).sorted(by: { (obj1, obj2) -> Bool in
            return obj1.date_created! < obj2.date_created!
        })

        arrFolder =  arrFolder.filter { (folder) -> Bool in
            if let passcode = folder.folder_passcode,passcode.count > 0{
                return true
            }
            return false
        }
        
        return arrFolder

    }

    
    
    override class func primaryKey() -> String? {
        return "identifier_unique"
    }
    
    
    let arrMediaLocals = LinkingObjects(fromType: MediaLocal.self, property: "folder_local")

    func deleteMediaPermanently(arrMedia:[MediaLocal])  {
        
        for mediaLocal in arrMedia {
            mediaLocal.deleteMedia()
        }

    }
    
    func moveToTrashMedia(arrMedia:[MediaLocal]) {
        let realm = try! Realm()
        let result = realm.objects(FolderLocal.self).filter("is_fake == \(SVSharedClass.shared.isFakePasscode) and folder_type == '\(FolderType.trash.rawValue)'")
        
        guard let trashFolder = Array(result).first else {
            return
        }
      
        for mediaLocal in arrMedia {
            let realm = try! Realm()
            try! realm.write {
                mediaLocal.folder_local = trashFolder
                realm.add(mediaLocal)
            }
        }

    }
    
    class func RemovePasswordFromAllFolders()  {
        let arrLockedFolders = FolderLocal.arrFolderLocked
        
        for folder in arrLockedFolders {
            let realm = try! Realm()
            try! realm.write {
                folder.folder_passcode = ""
                realm.add(folder)
            }
        }

    }
   
}

class MediaLocal: Object {
    @objc dynamic var identifier_unique = UUID().uuidString
    
    ///0:photo or 1:video
    @objc dynamic var media_type:Int = 0
    
    @objc dynamic var thumb_image_name:String? = nil
    @objc dynamic var media_name:String? = nil

//    @objc dynamic var folder_id:String? = nil
    @objc dynamic var folder_local:FolderLocal? = nil

    @objc dynamic var date_created:Date? = Date()
    @objc dynamic var date_updated:Date? = Date()
    @objc dynamic var is_fake:Bool = SVSharedClass.shared.isFakePasscode

    override class func primaryKey() -> String? {
        return "identifier_unique"
    }
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? MediaLocal {
            return self.identifier_unique == object.identifier_unique
        }
        return false
    }
    
    func deleteMedia()  {
        
        let thumbPath = appDataFolder.appendingPathComponent(self.thumb_image_name!)
        let mediaPath = appDataFolder.appendingPathComponent(self.media_name!)
        if FileManager.default.fileExists(atPath: thumbPath.path) {
            try? FileManager.default.removeItem(at: thumbPath)
        }
        if FileManager.default.fileExists(atPath: mediaPath.path) {
            try? FileManager.default.removeItem(at: mediaPath)
        }
        
        let realm = try! Realm()
        try! realm.write {
            realm.delete(self)
        }

        
    }
   
}


class ContactLocal: Object {
    @objc dynamic var identifier_unique = UUID().uuidString
    
    @objc dynamic var first_name:String? = nil
    @objc dynamic var last_name:String? = nil
    @objc dynamic var company_name:String? = nil

    @objc dynamic var mobile_number:String? = nil
    @objc dynamic var home_number:String? = nil
    @objc dynamic var work_number:String? = nil

    @objc dynamic var email:String? = nil
    @objc dynamic var url:String? = nil
    @objc dynamic var note:String? = nil
    @objc dynamic var date_created:Date? = Date()
    @objc dynamic var date_updated:Date? = Date()
    @objc dynamic var is_fake:Bool =  SVSharedClass.shared.isFakePasscode

    override class func primaryKey() -> String? {
        return "identifier_unique"
    }
   
}

class PasswordLocal: Object {
    @objc dynamic var identifier_unique = UUID().uuidString
    
    @objc dynamic var url:String? = nil
    @objc dynamic var username:String? = nil
    @objc dynamic var password:String? = nil
    @objc dynamic var date_created:Date? = Date()
    @objc dynamic var date_updated:Date? = Date()
    @objc dynamic var is_fake:Bool =  SVSharedClass.shared.isFakePasscode

    override class func primaryKey() -> String? {
        return "identifier_unique"
    }
   
}

class NoteLocal: Object {
    @objc dynamic var identifier_unique = UUID().uuidString
    
    @objc dynamic var title:String? = nil
    @objc dynamic var note:String? = nil
    @objc dynamic var date_created:Date? = Date()
    @objc dynamic var date_updated:Date? = Date()
    @objc dynamic var is_fake:Bool =  SVSharedClass.shared.isFakePasscode

    override class func primaryKey() -> String? {
        return "identifier_unique"
    }
   
}

func randomString(length: Int) -> String {
  let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
  return String((0..<length).map{ _ in letters.randomElement()! })
}
func randomMasterKey() -> String {
  let letters = "0123456789"
  return String((0..<10).map{ _ in letters.randomElement()! })
}

class BreakInAlert: Object {
    @objc dynamic var identifier_unique = UUID().uuidString
    
    @objc dynamic var image_path:String? = nil
    @objc dynamic var wrong_passcode:String? = nil
    @objc dynamic var date_created:Date? = Date()

    //test ss
    //var image:UIImage?
    
    override class func primaryKey() -> String? {
        return "identifier_unique"
    }
   
}

enum FolderType:String {
    case main
    case normal
    case trash
    

}

class WebPageLocal: Object {
    @objc dynamic var identifier_unique = UUID().uuidString
    
    @objc dynamic var url:String? = nil
    @objc dynamic var image_path:String? = nil
    @objc dynamic var date_created:Date? = Date()

    
    var imageScreen:UIImage?{
        get{
            if let image_path = self.image_path{
                let imagePathDoc = appDataFolder.appendingPathComponent(image_path)
                if FileManager.default.fileExists(atPath: imagePathDoc.path) {
                   return UIImage.init(contentsOfFile: imagePathDoc.path)
                }
            }
            return nil
        }
        set{
            //remove old image
            self.removeScreenImageIfExist()
                        
            //set new image
            if let newValue = newValue {
                let names = SVSharedClass.shared.generateNames(for: 0)
                let file_name = names.file_name
                if let data = newValue.jpegData(compressionQuality: 1.0){
                    if FileManager.write(data: data, absolutePath: names.full_path.path) == true{
                        let realm = try! Realm()
                        try! realm.write {
                            self.image_path = file_name
                            realm.add(self)
                        }
                    }
                }
            }
        }
    }
    
    func removeScreenImageIfExist() {
        if let image_path = image_path {
            let imagePathDoc = appDataFolder.appendingPathComponent(image_path)
            if FileManager.default.fileExists(atPath: imagePathDoc.path) {
                try? FileManager.default.removeItem(at: imagePathDoc)
            }
        }
    }
    

    override class func primaryKey() -> String? {
        return "identifier_unique"
    }
   
}


extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        guard let index = firstIndex(of: object) else {return}
        remove(at: index)
    }
    
}


