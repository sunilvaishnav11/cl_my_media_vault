
import UIKit
import RealmSwift
import AVFoundation
import AVKit

class MR_BrowseMediaVC: SVBaseController {

    let collectionView:UICollectionView = UICollectionView.init(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var folderLocal:FolderLocal!
    var arrMedia:[MediaLocal] = []
    var initialIndex:Int = 0
    var selectedIndex:Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.initialIndex != -1 {
            self.collectionView.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {[weak self] in
                guard let self = self else { return }
                self.selectedIndex = self.initialIndex
                self.showSelectedIndex()
                self.initialIndex = -1
                self.collectionView.isHidden = false
            }
        }

    }
    deinit {
        folderLocal = nil
        arrMedia = []
        collectionView.removeFromSuperview()
        
    }
    override func setupUI() {
        super.setupUI()
        
        collectionView.register(CellBrowseMedia.self, forCellWithReuseIdentifier: "CellBrowseMedia")

        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.itemSize = AppDel.window!.frame.size;
        layout.sectionInset = .zero;
        
        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .RGB(245,246,247)
        self.view.backgroundColor = collectionView.backgroundColor
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isPagingEnabled = true
        
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (m) in
            m.left.top.right.equalToSuperview()
//            m.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottomMargin).offset(-50)
        }
        collectionView.reloadData()
        
        let toolBar = UIToolbar()
        view.addSubview(toolBar)
        toolBar.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview()
//            m.bottom.equalTo(self.view.snp.bottom)
            m.height.equalTo(50)
            m.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottomMargin)
            m.top.equalTo(self.collectionView.snp.bottom)
        }
        
        let barItemAction = UIBarButtonItem.init(barButtonSystemItem: .action) {[weak self] in
            guard let self = self else { return }
            self.actionShareTapped()
        }
        let barItem1 = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace) {
            
        }
        let moveItem = UIBarButtonItem.init(title: "Move", style: .plain) {[weak self] in
            guard let self = self else { return }
            self.actionMoveTapped()
        }
        let barItem2 = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace) {
            
        }

        let barItemDelete = UIBarButtonItem.init(barButtonSystemItem: .trash) {[weak self] in
            guard let self = self else { return }
            var msg = "Are you sure you want to delete this media?"
            if self.folderLocal.folder_typeEnum == FolderType.trash{
                msg = "Are you sure you want to permanently delete this media?\nThis action cannot be undone"
            }
            let alert = UIAlertController.actionSheetController(withTitle: msg)
            alert.addCancelButton()
            alert.addDestructiveButton(withTitle: "Delete") { (action) in
                self.actionDeleteMediaTapped()
            }
            alert.show()
        }
        barItemDelete.tintColor = UIColor.red
        toolBar.items = [barItemAction,barItem1,moveItem,barItem2,barItemDelete]
        
        
    }
    
    func actionDeleteMediaTapped()  {
        let mediaLocal = self.arrMedia[self.selectedIndex]
        
        self.arrMedia.remove(object: mediaLocal)
        if self.folderLocal.folder_typeEnum == FolderType.trash {
            self.folderLocal.deleteMediaPermanently(arrMedia: [mediaLocal])
            self.navigationController?.view.makeToast("Deleted")
        }else{
            self.folderLocal.moveToTrashMedia(arrMedia: [mediaLocal])
            self.view.makeToast("Moved to Trash")
        }
        if self.arrMedia.count == 0 {
             self.navigationController?.popViewController(animated: true)
        }
        self.collectionView.reloadData()

    }
    /*
    func actionMoreOptionTapped()  {
        
        let action = UIAlertController.actionSheetController(withTitle: "Actions")
        action.addButton(withTitle: "Export") {[weak self] (actoin) in
            guard let self = self else { return }
            self.actionExportTapped()
        }
        action.addButton(withTitle: "Share") { [weak self](actoin) in
            guard let self = self else { return }
            self.actionShareTapped()
        }
        action.addButton(withTitle: "Move") { [weak self](actoin) in
            guard let self = self else { return }
            self.actionMoveTapped()
        }
        action.addCancelButton()
        action.show()


    }*/
    
    func actionExportTapped()  {
        
    }
    func actionShareTapped()  {
        
        let mediaLocal = self.arrMedia[self.selectedIndex]
        var contentToShare:[URL] = []
        contentToShare.append(appDataFolder.appendingPathComponent(mediaLocal.media_name!))

        let imageToShare = contentToShare
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)

    }
    func actionMoveTapped()  {
        let selectFolder = SelectFolder()
        selectFolder.selectedFolder = folderLocal
        let navi = UINavigationController.init(rootViewController: selectFolder)

        selectFolder.blockDidSelectFolder = { [weak self] folder in
            guard let self = self else { return }
            if let folder = folder {
                self.moveFileInFolder(folder: folder)
            }
            navi.dismiss(animated: true, completion: nil)
        }
        self.present(navi, animated: true, completion: nil)
    }
    
    func moveFileInFolder(folder:FolderLocal)    {
        let mediaLocal = self.arrMedia[self.selectedIndex]
        
        let realm = try! Realm()
        try! realm.write {
            mediaLocal.folder_local = folder
            realm.add(mediaLocal)
        }
        
        self.arrMedia.remove(object: mediaLocal)
        self.collectionView.reloadData()

        if self.arrMedia.count == 0 {
            self.navigationController?.popViewController(animated: true)
        }
        


    }

    
    func showSelectedIndex()  {
        print(IndexPath.init(row: selectedIndex, section:0))
        self.collectionView.contentOffset = CGPoint.init(x: selectedIndex * Int(collectionView.frame.width), y: 0)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MR_BrowseMediaVC:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrMedia.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellBrowseMedia", for: indexPath) as! CellBrowseMedia

        let media = arrMedia[indexPath.row]

        cell.media = media
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        self.title = "\(indexPath.row+1) of \(self.arrMedia.count)"
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let media = arrMedia[indexPath.row]
        if media.media_type == 0{
            
        }else if media.media_type == 1{
            let url_video = appDataFolder.appendingPathComponent(media.media_name!)

            let player = AVPlayer(url: url_video)
            let playerController = AVPlayerViewController()
            playerController.player = player
            present(playerController, animated: true) {
                player.play()
            }
        }
    }
}
class CellBrowseMedia: UICollectionViewCell,UIScrollViewDelegate {
    let imgThumb:UIImageView = UIImageView()
    let imgPlay:UIImageView = UIImageView()
    let scrContentView:ImageScrollView = ImageScrollView()

    var media:MediaLocal!{
        didSet{
            self.updateUI()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupUI(){
        addSubview(scrContentView)
        scrContentView.snp.makeConstraints { (m) in
            m.edges.equalToSuperview()
        }
        
        scrContentView.minimumZoomScale = 0.5
        scrContentView.maximumZoomScale = 10
        scrContentView.setup()
        /*
        scrContentView.addSubview(self.imgThumb)
        scrContentView.isUserInteractionEnabled = true
        imgThumb.contentMode = .scaleAspectFit
        imgThumb.clipsToBounds = true
        
        
        self.imgThumb.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.size.equalToSuperview()
        }
        imgThumb.isUserInteractionEnabled = true
        */
        
        self.addSubview(imgPlay)
        imgPlay.snp.makeConstraints { (m) in
            m.center.equalToSuperview()
            m.size.equalTo(70)
        }
        imgPlay.setImage(UIImage.init(named: "play_button")!)
        imgPlay.contentMode = .scaleAspectFit
                
    }

    func updateUI()  {
        if media.media_type == 0 {
            scrContentView.isUserInteractionEnabled = true

            imgPlay.isHidden = true
            let media_url = appDataFolder.appendingPathComponent(media.media_name!)
            scrContentView.display(image: UIImage.init(contentsOfFile: media_url.path)!)
//            imgThumb.setImage(UIImage.init(named: thumb.path)!)
        }else{
            scrContentView.isUserInteractionEnabled = false

            imgPlay.isHidden = false
            let media_thumb_url = appDataFolder.appendingPathComponent(media.thumb_image_name!)
            scrContentView.display(image: UIImage.init(contentsOfFile: media_thumb_url.path)!)
//            imgThumb.setImage(UIImage.init(named: thumb.path)!)
        }
    }
}


class SelectFolder: SVBaseController {
    let tblFolders:UITableView = UITableView()
    var arrFolders:[FolderLocal] = []
    var selectedFolder:FolderLocal?
    var blockDidSelectFolder: ((FolderLocal?) -> Void)?
    let btnAddNewFolder:LGButton = LGButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.title = "Select Folder"
        addRightBarButton(title: "Done")
        self.setupUI()
        self.updateUI()
    }
    
    @objc override func actionRightBarTapped(sender: UIBarButtonItem) {
        if let blockDidSelectFolder = blockDidSelectFolder {
            if let selectedFolder = self.selectedFolder{
                blockDidSelectFolder(selectedFolder)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUI()
    }
    
    override func setupUI() {
        super.setupUI()
        
        view.addSubview(btnAddNewFolder)
        btnAddNewFolder.snp.makeConstraints { (m) in
            m.left.equalToSuperview().offset(10)
            m.right.equalToSuperview().offset(-10)
            m.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin).offset(10)
            m.height.equalTo(50)
        }
        
        
        view.addSubview(tblFolders)
        tblFolders.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview()
            m.top.equalTo(btnAddNewFolder.snp.bottom).offset(5)
            m.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottomMargin)
        }
        
//        btnAddNewFolder.setTitle("Add New Folder", for: .normal)
//        btnAddNewFolder.setTitleColor(.appBlue, for: .normal)
        btnAddNewFolder.tintColor = .appBlue
//        btnAddNewFolder.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        
        btnAddNewFolder.leftImageSrc = UIImage.init(named: "add_new")
        btnAddNewFolder.leftImageColor = .appBlue
        btnAddNewFolder.leftImageHeight = 30
        btnAddNewFolder.leftImageWidth = 30
        btnAddNewFolder.spacingTitleIcon = 30
        btnAddNewFolder.titleString = "Add New Folder"
        btnAddNewFolder.titleColor = .appBlue
        btnAddNewFolder.titleFontName = "HelveticaNeue-Bold"
        btnAddNewFolder.leftAligned = true
        btnAddNewFolder.titleFontSize = 16
        btnAddNewFolder.bgColor = .clear
        btnAddNewFolder.addTarget(self, action: #selector(actionAddNewFolder), for: .touchUpInside)
        
        
        tblFolders.delegate = self
        tblFolders.dataSource = self
        tblFolders.tableFooterView = UIView()
        tblFolders.register(CellSelectFolder.self, forCellReuseIdentifier: "CellSelectFolder")
        tblFolders.separatorStyle = .singleLine
        tblFolders.separatorInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 0)

        tblFolders.reloadData()
        
        
    }
    
    @objc func actionAddNewFolder()  {
        let alert = UIAlertController.alertViewController(withTitle: "Create New Folder", message: "Enter name of\nnew folder")
        alert.addTextField { (txt) in
            txt.placeholder = "Enter name"
            txt.font = UIFont.systemFont(ofSize: 17)
            txt.autocorrectionType = .no
            txt.autocapitalizationType = .words
            txt.delegate = self
            txt.tag = 100

            txt.snp.makeConstraints { (make) in
                make.height.equalTo(27)
            }
        }
        alert.addCancelButton()
        alert.addButton(withTitle: "OK") { (action) in
            if let name = alert.textFields?.first!.text,name.isEmpty == false{
                self.createFolderNamed(name: name)
            }else{
                UIAlertController.showOKAlertViewController(withTitle: "Alert!", message: "Invalid Folder Name")
            }
        }
        alert.show()

    }
    func createFolderNamed(name:String)  {
           let folder = FolderLocal()
           
           let realm = try! Realm()
           try! realm.write {
               folder.display_name = name
               folder.can_delete = true
               realm.add(folder)
           }
           
           self.updateUI()

       }
    
    override func updateUI()  {
        let realm = try! Realm()
        let result = realm.objects(FolderLocal.self).filter("is_fake == \(SVSharedClass.shared.isFakePasscode)")
        self.arrFolders = Array(result).sorted(by: { (obj1, obj2) -> Bool in
            return obj1.date_created! < obj2.date_created!
        })
        self.arrFolders = self.arrFolders.filter({ (folder) -> Bool in
            if folder.folder_typeEnum == .trash{
                return false
            }
            return true
        })
        self.tblFolders.reloadData()

    }
    

}
extension SelectFolder:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 100 {
            guard let preText = textField.text as NSString?,
                preText.replacingCharacters(in: range, with: string).count <= 25 else {
                return false
            }
        }
        return true
    }
}

extension SelectFolder: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFolders.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellSelectFolder", for: indexPath)
        let folder = arrFolders[indexPath.row]
        cell.imageView?.image = UIImage.init(named: "folder_icon")
        cell.imageView?.contentMode = .scaleAspectFit
        cell.textLabel?.text = folder.display_name ?? ""
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
        if selectedFolder == folder {
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let folder = arrFolders[indexPath.row]
        selectedFolder = folder
        self.tblFolders.reloadData()
    }
    
    class CellSelectFolder: UITableViewCell {
        override func layoutSubviews() {
            super.layoutSubviews()
            self.imageView?.bounds = .init(x: 0, y: 0, width: 35, height: 35)
        }
    }

}
