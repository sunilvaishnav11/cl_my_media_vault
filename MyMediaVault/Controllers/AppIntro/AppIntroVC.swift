//
//  AppIntroVC.swift
//  MyMediaVault
//
//  Created by Macbook on 29/11/21.
//

import UIKit

class AppIntroVC: SVBaseController {
    
    let page1 = Page1()

    var naviContainer:UINavigationController?
    
    let btnContinue:UIButton = UIButton()
    let imgArrow:UIImageView = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor  = .white
        self.setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseIn,.autoreverse,.repeat], animations: {
            self.imgArrow.transform = CGAffineTransform(translationX: 6, y: 0)
        }, completion: nil)

    }

    override func setupUI() {

        self.naviContainer = UINavigationController.init(rootViewController: page1)
        self.naviContainer!.isNavigationBarHidden = true
        self.view.addSubview(self.naviContainer!.view)
        addChild(self.naviContainer!)
        self.naviContainer!.view.snp.makeConstraints { (m) in
            m.left.top.right.equalToSuperview()
            m.height.equalToSuperview().multipliedBy(0.80)
        }
        
        
        
        view.addSubview(btnContinue)
        btnContinue.snp.makeConstraints { (make) in
            make.bottomMargin.equalTo(self.view.snp.bottomMargin).offset(-50)
            //make.width.equalToSuperview().multipliedBy(0.77)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(60)
            make.centerX.equalToSuperview()
        }
        btnContinue.backgroundColor = UIColor.appBlue
        btnContinue.setTitle("Continue", for: .normal)
        btnContinue.setTitleColor(UIColor.white, for: .normal)
        btnContinue.addTarget(self, action: #selector(btnContinueTapped), for: .touchUpInside)
        btnContinue.layer.cornerRadius = 6
        btnContinue.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25)
        btnContinue.addSubview(imgArrow)
        if #available(iOS 13.0, *) {
            imgArrow.image = UIImage.init(systemName: "arrow.right.square.fill")?.withRenderingMode(.alwaysTemplate)
        } else {
            // Fallback on earlier versions
        }
        imgArrow.isUserInteractionEnabled = false
        imgArrow.tintColor = .white
        imgArrow.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-20)
            make.size.equalTo(27)
        }
    }
    override func updateUI() {

    }

    @objc func btnContinueTapped()  {
        let page = self.naviContainer?.visibleViewController as? IntroPageBase
        if (page!.isKind(of: Page4.self)) {
            let sales = SalesVC()
            sales.blockCompletion = {
                AppDel.showPasscodeSceen()
            }
            sales.view.backgroundColor = .white
            sales.btnClose.tintColor = UIColor.darkGray
            sales.btnRestore.setTitleColor(UIColor.darkGray, for: .normal)
            self.navigationController?.pushViewController(sales, animated: false)

        }else{
            page?.btnContinueTapped()
        }
    }
}


class Page1: IntroPageBase {
    override func setupUI() {
        super.setupUI()
        self.viewDevice.imgDeviceContent.image = UIImage.init(named: "1ssx.png")
        lblTitle.text = "Lock Anything"
        lblMessage.text = "Securely store photos & video folder wise"

    }
    override func btnContinueTapped() {
        self.navigationController?.pushViewController(Page2(), animated: true)
    }
}

class Page2: IntroPageBase {
    override func setupUI() {
        super.setupUI()
        self.viewDevice.imgDeviceContent.image = UIImage.init(named: "2ssx")
        lblTitle.text = "Smart Gallery"
        lblMessage.text = "Easly Manage Your Photo"
    }
    override func btnContinueTapped() {
        self.navigationController?.pushViewController(Page3(), animated: true)
    }
}

class Page3: IntroPageBase {
    override func setupUI() {
        super.setupUI()
        self.viewDevice.imgDeviceContent.image = UIImage.init(named: "3ssx")
        lblTitle.text = "Private Browse"
        lblMessage.text = "Securely Browse the internet"
    }
    override func btnContinueTapped() {
        self.navigationController?.pushViewController(Page4(), animated: true)
    }

}
class Page4: IntroPageBase {
    override func setupUI() {
        super.setupUI()
        self.viewDevice.imgDeviceContent.image = UIImage.init(named: "4ssx")
        lblTitle.text = "Break-in Alerts"
        lblMessage.text = "Snap a picture of the intruder"
   }
    override func btnContinueTapped() {
        
    }

}
