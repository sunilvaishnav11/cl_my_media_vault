//
//  IntroPageBase.swift
//  MyMediaVault
//
//  Created by Macbook on 30/11/21.
//

import UIKit

class IntroPageBase: UIViewController {

    let imgBackground:UIImageView = UIImageView()
    let imgContainerScreen:UIView = UIView()
    let imgScreen:UIImageView = UIImageView()
    let btnContinue:UIButton = UIButton()
    let imgArrow:UIImageView = UIImageView()

    let viewDevice:ViewDevice = ViewDevice()
    
    let lblTitle:UILabel = UILabel()
    let lblMessage:UILabel = UILabel()
    
    let gradient = CAGradientLayer()
    let vBottomFade = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnContinue.isHidden = true
        
        self.view.backgroundColor = .white
        self.navigationController?.isNavigationBarHidden = true
        self.setupUI()
        
        
    }
    
    
    func setupUI()  {
        self.view.addSubview(imgBackground)
        imgBackground.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            if Device.size() >= .screen5_8Inch{
                make.topMargin.equalTo(self.view.snp.topMargin).offset(20)
            }else{
                make.topMargin.equalTo(self.view.snp.topMargin).offset(-20)
            }

            make.height.equalTo(self.imgBackground.snp.width).multipliedBy(1.281)
        }
        imgBackground.backgroundColor = .clear
        
//        imgBackground.image = UIImage.init(named: "walk_through")
        imgBackground.isUserInteractionEnabled = false
        
        
        view.addSubview(imgContainerScreen)
        imgContainerScreen.snp.makeConstraints { (make) in
            make.bottom.equalTo(imgBackground.snp.bottom)
            make.centerX.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6822916667)
            make.height.equalTo(imgContainerScreen.snp.width).multipliedBy(1.34351145)
        }

        imgContainerScreen.superview?.sendSubviewToBack(imgContainerScreen)
        imgScreen.contentMode = .scaleAspectFill
        imgContainerScreen.clipsToBounds = true

        imgContainerScreen.addSubview(imgScreen)
        imgScreen.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(imgScreen.snp.width).multipliedBy(1.34351145)
        }
        
        imgContainerScreen.isHidden = true
        

        view.addSubview(viewDevice)
        viewDevice.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.topMargin.equalTo(self.view.snp.topMargin).offset(20)
            
            make.width.equalToSuperview().offset(-120)
            make.height.equalTo(imgContainerScreen.snp.width).multipliedBy(2.164) //1.778
        }
        
        

        
        self.view.addSubview(vBottomFade)
        vBottomFade.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.35)
        }
        
        vBottomFade.frame = vBottomFade.bounds
        vBottomFade.backgroundColor = .white
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor , UIColor.black.cgColor]
        gradient.locations = [0, 0.4, 1] //[0, 0.1, 0.5, 1]
        vBottomFade.layer.mask = gradient

        view.addSubview(btnContinue)
        btnContinue.snp.makeConstraints { (make) in
            make.bottomMargin.equalTo(self.view.snp.bottomMargin).offset(-50)
            //make.width.equalToSuperview().multipliedBy(0.77)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(60)
            make.centerX.equalToSuperview()
        }
        btnContinue.backgroundColor = UIColor.appBlue
        btnContinue.setTitle("Continue", for: .normal)
        btnContinue.setTitleColor(UIColor.white, for: .normal)
        btnContinue.addTarget(self, action: #selector(btnContinueTapped), for: .touchUpInside)
        btnContinue.layer.cornerRadius = 6
//        btnContinue.layer.shadowColor = UIColor.black.cgColor
//        btnContinue.layer.shadowRadius = 10
//        btnContinue.layer.shadowOpacity = 0.2
//        btnContinue.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        btnContinue.titleLabel?.font = UIFont.boldSystemFont(ofSize: 19)
        
        if #available(iOS 13.0, *) {
            imgArrow.image = UIImage.init(systemName: "arrow.right.square.fill")?.withRenderingMode(.alwaysTemplate)
        } else {
            // Fallback on earlier versions
        }
        imgArrow.isUserInteractionEnabled = false
        imgArrow.tintColor = .white
        btnContinue.addSubview(imgArrow)
        imgArrow.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-20)
            make.size.equalTo(27)
        }
        
        view.addSubview(lblTitle)
        view.addSubview(lblMessage)
        
        
        lblTitle.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.bottom.equalTo(self.lblMessage.snp.top).offset(-5)
        }

        lblMessage.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.bottom.equalToSuperview().offset(-15)
        }
        
        lblTitle.font = UIFont.boldSystemFont(ofSize: 29)
        lblTitle.textColor = UIColor.black
        lblTitle.numberOfLines = 0
        lblTitle.textAlignment = .center

        lblMessage.font = UIFont.systemFont(ofSize: 16)
        lblMessage.textColor = UIColor.RGB(142, 148, 164)
        lblMessage.numberOfLines = 0
        lblMessage.textAlignment = .center


    }
    func updateUI() {

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseIn,.autoreverse,.repeat], animations: {
            self.imgArrow.transform = CGAffineTransform(translationX: 6, y: 0)
        }, completion: nil)

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gradient.frame = vBottomFade.bounds
    }
    
    @objc func btnContinueTapped()  {
        
    }

    func setImage(_ image:UIImage?)  {
           if let image = image{
               imgScreen.image = image
               imgScreen.snp.removeConstraints()
               imgScreen.snp.makeConstraints { (make) in
                   make.left.top.right.equalToSuperview()
                   make.height.equalTo(imgScreen.snp.width).multipliedBy(image.size.height/image.size.width)
               }
            imgScreen.isHidden = true
            self.viewDevice.imgDeviceContent.image = image;

           }
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class ViewDevice: UIView {
    
    var vContainer:UIView = UIView()
    var imgDeviceContent:UIImageView = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup()  {
        self.backgroundColor = .clear

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 7
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize.init(width: 0, height: -3)


        addSubview(vContainer)
        vContainer.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        vContainer.layer.cornerRadius  = 25
        vContainer.backgroundColor = .white
        
        vContainer.backgroundColor = UIColor.white //.RGB(222, 223, 225)
        imgDeviceContent.backgroundColor = .white

        vContainer.addSubview(imgDeviceContent)
        imgDeviceContent.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(15)
            make.right.equalTo(-15)
            make.bottom.equalTo(-15)
        }
        
        let topNotch = UIView()
        vContainer.addSubview(topNotch)
        topNotch.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.5)
            make.height.equalTo(35)
        }
        topNotch.backgroundColor = vContainer.backgroundColor
        topNotch.layer.cornerRadius = 12
        
        if Device.size() <= Size.screen5_5Inch {
            topNotch.isHidden = true
        }

        imgDeviceContent.layer.cornerRadius  = 20
        imgDeviceContent.clipsToBounds = true
        imgDeviceContent.contentMode  = .scaleAspectFill
        
       
    }

//    override func layoutSubviews() {
//        super.layoutSubviews()
//        gradient.frame = vBottomFade.bounds
//    }
    
}
