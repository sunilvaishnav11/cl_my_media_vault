//
//  AddPasswordVC.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit
import RealmSwift

class MR_AddPasswordVC: SVBaseController {
    let urlField = InpputFieldView()
    let usernameField = InpputFieldView()
    let passwordField = InpputFieldView()

    var passwordLocal:PasswordLocal!
    let scrView = UIScrollView()
    let vContent = IQPreviousNextView()
    var isForDisplayPurpose:Bool = false
    var isUpdate:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        if isForDisplayPurpose {
            self.title = ""
//            self.btnAdd?.isHidden = true
        }else{
            
            self.title = "Add Password"
            if isUpdate == true {
                self.title = "Update Password"
            }
//            self.btnAdd?.isHidden = false
//            addRightBarButton(title: "Done")
        }
        
        self.addLeftBarButton(title: "Cancel")
        self.setupUI()
        self.loadPasswordInformation()

    }
    
   
    override func setupUI() {
        
        view.addSubview(scrView)
        scrView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.topMargin.equalTo(self.view.snp.topMargin)
            // make.bottomMargin.equalTo(self.view.snp.bottomMargin).offset(-20)
        }
        
        scrView.addSubview(vContent)
        vContent.snp.makeConstraints { (m) in
            m.left.top.right.bottom.equalToSuperview()
            m.width.equalTo(self.view)
        }

        vContent.addSubview(urlField)
        vContent.addSubview(usernameField)
        vContent.addSubview(passwordField)

        urlField.lblTitle.text = "Website"
        urlField.txtValue.placeholder = "Website i.e. Facebook"
        urlField.txtValue.autocapitalizationType = .words

        usernameField.lblTitle.text = "Username or email"
        usernameField.txtValue.placeholder = "Username or email"

        passwordField.lblTitle.text = "Password"
        passwordField.txtValue.placeholder = "Password"

        usernameField.txtValue.autocorrectionType = .no
        usernameField.txtValue.autocapitalizationType = .none

        passwordField.txtValue.autocorrectionType = .no
        passwordField.txtValue.autocapitalizationType = .none

        if isForDisplayPurpose == true {
            
            let arrField = [urlField,usernameField,passwordField]
            arrField.forEach { (field) in
                field.changeEditingMode(isEditing: false)
            }
        }
        
        urlField.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview().inset(20)
            m.top.equalTo(20)
        }
        
        usernameField.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview().inset(20)
            m.top.equalTo(urlField.snp.bottom).offset(10)
        }
        
        passwordField.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview().inset(20)
            m.top.equalTo(usernameField.snp.bottom).offset(10)
            m.bottom.equalToSuperview().offset(-40)
        }
        
        if isForDisplayPurpose == false {
            self.insertAddButtonUnder(title: "DONE", viewTop: self.scrView)
        }else{
            self.insertNoteUnder(note: "Tap on field value to copy", viewTop: self.scrView)
        }

       scrView.addGestureRecognizer(UITapGestureRecognizer.init(handler: { [weak self] (tap) in
           guard let self = self else { return }
           self.view.endEditing(true)
       }))

    }
    override func actionLeftBarTapped(sender: UIBarButtonItem) {
           self.dismiss(animated: true, completion: nil)
    }
    func loadPasswordInformation()  {
        self.urlField.txtValue.text = self.passwordLocal.url ?? ""
        self.usernameField.txtValue.text  = self.passwordLocal.username ?? ""
        self.passwordField.txtValue.text  = self.passwordLocal.password ?? ""

    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.view.endEditing(true)
    }

        
    override func btnAddTapped() {

        self.view.endEditing(true)
        
        
        let url = self.urlField.txtValue.text
        let username = self.usernameField.txtValue.text
        let password = self.passwordField.txtValue.text

        if url?.isEmpty != true ||  username?.isEmpty != true || password?.isEmpty != true{
            let realm = try! Realm()
            try! realm.write {
                passwordLocal.url = url
                passwordLocal.username = username
                passwordLocal.password = password
                passwordLocal.date_updated = Date()

                realm.add(passwordLocal)
            }
            

            self.doAfterDelay(inMain: 1) {
                SVSharedClass.shared.appRequestReview()
            }
            self.dismiss(animated: true, completion: nil)

        }


    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class InpputFieldView: UIView {
    let lblTitle:UILabel = UILabel()
    let txtValue:UITextField = UITextField()
    let box = UIView()
    let bottomLine = UIView()
    var tapGesture:UITapGestureRecognizer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func changeEditingMode(isEditing:Bool)  {
        if isEditing == true {
            self.box.layer.borderWidth = 0.5
            self.lblTitle.textColor = .black
            self.txtValue.isUserInteractionEnabled = true
            self.removeGestureRecognizer(tapGesture)
            
        }else{
            self.box.layer.borderWidth = 0
            self.lblTitle.textColor = .lightGray
            self.txtValue.isUserInteractionEnabled = false
            self.addGestureRecognizer(tapGesture)
        }

        box.layer.borderWidth = 0
        box.layer.cornerRadius = 0


    }
    
    func setupUI()  {
        self.addSubview(lblTitle)
        self.addSubview(txtValue)
        self.addSubview(bottomLine)
        
        lblTitle.snp.makeConstraints { (m) in
            m.left.top.right.equalToSuperview()
            m.height.equalTo(25)
        }
        lblTitle.textColor = .black
        
        
        addSubview(box)
        box.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview()
            m.top.equalTo(lblTitle.snp.bottom).offset(5)
            m.height.equalTo(40)
            m.bottom.equalToSuperview()
        }
        
        bottomLine.snp.makeConstraints { (m) in
            m.left.equalTo(box.snp.left)
            m.right.equalTo(box.snp.right)
            m.top.equalTo(box.snp.bottom)
            m.height.equalTo(0.5)
        }
        bottomLine.backgroundColor = .appBlue
        
        box.backgroundColor = UIColor.RGB(238,239,243)
        
        box.layer.borderColor = UIColor.lightGray.cgColor
        box.layer.borderWidth = 0.5
        box.layer.cornerRadius = 4
        
        box.addSubview(txtValue)
        txtValue.snp.makeConstraints { (m) in
            m.edges.equalToSuperview().inset(5)
        }
        txtValue.textColor = .black
        txtValue.autocorrectionType = .no
        txtValue.autocapitalizationType = .none
        txtValue.clearButtonMode = .whileEditing

        
        tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapGestureDidRecognize(gesture:)))
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.numberOfTapsRequired = 1
        
        box.layer.borderWidth = 0
        box.layer.cornerRadius = 0

        box.backgroundColor = .clear
    }
    
    
    @objc func tapGestureDidRecognize(gesture:UITapGestureRecognizer)  {
        UIPasteboard.general.string = self.txtValue.text
        SVProgressHUD.showSuccess(withStatus: "Copy to clipboard")
        SVProgressHUD.dismiss(withDelay: 1)
        
    }
}
