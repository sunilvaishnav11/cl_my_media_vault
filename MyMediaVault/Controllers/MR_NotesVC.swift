//
//  NotesVC.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit
import RealmSwift

class MR_NotesVC: SVBaseController {

    let tableNote:UITableView = UITableView()
    var arrNotes:[NoteLocal] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Notes"
        self.addRightBarButton(image: UIImage.init(systemName: "plus")!)
        setupUI()
        
        tableNote.register(CellNoteInfo.self, forCellReuseIdentifier: "CellNoteInfo")
        tableNote.delegate = self
        tableNote.dataSource = self
        tableNote.tableFooterView = UIView()
        
        tableNote.separatorStyle = .singleLine
        tableNote.separatorInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 0)
        
        tableNote.emptyDataSetSource = self
        tableNote.emptyDataSetDelegate = self
        tableNote.tableFooterView = UIView()

    }
    
    override func actionRightBarTapped(sender: UIBarButtonItem) {
        self.btnAddTapped()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateUI()
    }
    override func setupUI() {
        view.addSubview(tableNote)
        tableNote.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
        }
        self.insertAddButtonUnder(title: "Add Note", viewTop: tableNote)


    }
    override func updateUI() {
        let realm = try! Realm()
        let result = realm.objects(NoteLocal.self).filter("is_fake == \(SVSharedClass.shared.isFakePasscode)")
        self.arrNotes = Array(result).sorted(by: { (obj1, obj2) -> Bool in
            return obj1.date_updated! > obj2.date_updated!
        })

        tableNote.reloadData()

    }
    override func btnAddTapped() {
        let addNew = MR_AddNotesVC()
        let noteLocal = NoteLocal()
        noteLocal.date_created = Date()
        addNew.noteLocal = noteLocal
        let navi = UINavigationController.init(rootViewController: addNew)
        navi.modalPresentationStyle = .fullScreen
        self.present(navi, animated: true, completion: nil)

    }


}

extension MR_NotesVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotes.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.arrNotes.count > 0,SVSharedClass.shared.launchCountValue < ConstantValue.swipeHelpShowCount {
            return 50
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.arrNotes.count > 0,SVSharedClass.shared.launchCountValue < ConstantValue.swipeHelpShowCount {
            let lblTitle = UILabel()
            lblTitle.text = ConstantValue.swipeHelpText
            lblTitle.font = UIFont.systemFont(ofSize: 14)
            lblTitle.textColor = .darkGray
            lblTitle.textAlignment = .center
            lblTitle.backgroundColor = .white
            return lblTitle
        }
        return nil
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellNoteInfo", for: indexPath) as! CellNoteInfo
        cell.noteLocal = arrNotes[indexPath.row]
        cell.delegate = self

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let noteLocal = arrNotes[indexPath.row]

        let addNew = MR_AddNotesVC()
        addNew.noteLocal = noteLocal
        let navi = UINavigationController.init(rootViewController: addNew)
        navi.modalPresentationStyle = .fullScreen
        self.present(navi, animated: true, completion: nil)
//        self.navigationController?.pushViewController(addNew, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }

        
        let editAction = SwipeAction(style: .default, title: nil) {  [weak self]  action, indexPath in
            // handle action by updating model with deletion
            guard let self = self else { return }
            self.cellEditTappedAt(indexPath: indexPath)
        }
        editAction.image = UIImage(named: "edit_contact2")?.withRenderingMode(.alwaysTemplate)
        editAction.textColor = .black
        editAction.backgroundColor = .appBlue

        
        let deleteAction = SwipeAction(style: .destructive, title: nil) { [weak self] action, indexPath in
            // handle action by updating model with deletion
            guard let self = self else { return }
            self.cellDeleteTappedAt(indexPath: indexPath)
            
        }
        deleteAction.textColor = .black

        // customize the action appearance
        deleteAction.image = UIImage(named: "delete_trash")?.withRenderingMode(.alwaysTemplate)

        return [deleteAction,editAction]
    }
    
    func cellEditTappedAt(indexPath:IndexPath)  {
        let noteLocal = arrNotes[indexPath.row]
        
        let addNew = MR_AddNotesVC()
        addNew.noteLocal = noteLocal
        let navi = UINavigationController.init(rootViewController: addNew)
        navi.modalPresentationStyle = .fullScreen
        self.present(navi, animated: true, completion: nil)
//        self.navigationController?.pushViewController(addNew, animated: true)
        
    }
    func cellDeleteTappedAt(indexPath:IndexPath)  {
        let noteLocal = arrNotes[indexPath.row]
        
        let realm = try! Realm()
        try! realm.write {
            realm.delete(noteLocal)
        }
        self.updateUI()
    }
    
   
}

extension MR_NotesVC : SwipeTableViewCellDelegate{
    
  func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .none
        options.transitionStyle = .drag
        return options
    }
}

extension MR_NotesVC:DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let nameBuilder = AttributedStringBuilder()
        nameBuilder.text("No notes added", attributes: [.font(UIFont.systemFont(ofSize: 30)),.textColor(.black),.alignment(.center)])
        
        return nameBuilder.attributedString

    }
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let nameBuilder = AttributedStringBuilder()
        nameBuilder.text("Start adding your\nPrivate Notes", attributes: [.font(UIFont.systemFont(ofSize: 18)),.textColor(.darkGray),.alignment(.center)])
        
        return nameBuilder.attributedString

    }
    
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        if self.arrNotes.count > 0 {
            return false
        }
        return true
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "no_notes")
    }

}



class CellNoteInfo: SwipeTableViewCell {
    var lblTitle:UILabel = UILabel()
    var lblNote:UILabel = UILabel()
    var lblDate:UILabel = UILabel()
    
    var noteLocal:NoteLocal!{
        didSet{
            updateUI()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // code common to all your cells goes here
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setupUI()   {
        contentView.addSubview(lblTitle)
        contentView.addSubview(lblNote)
        contentView.addSubview(lblDate)

        let leftM = 30
        lblNote.snp.makeConstraints { (m) in
            m.centerY.equalToSuperview()
            m.left.equalTo(leftM)
        }

        lblTitle.snp.makeConstraints { (m) in
            m.left.equalTo(leftM)
            m.bottom.equalTo(lblNote.snp.top)
        }
        lblDate.snp.makeConstraints { (m) in
            m.top.equalTo(lblNote.snp.bottom)
            m.left.equalTo(leftM)
        }
        
        lblTitle.textAlignment = .left
        lblTitle.textColor = .black
        lblTitle.font = UIFont.boldSystemFont(ofSize:  17)
        
        lblNote.textAlignment = .left
        lblNote.textColor = .RGB(138,138,145)
        lblNote.font = UIFont.boldSystemFont(ofSize:  15)
        
        lblDate.textAlignment = .left
        lblDate.textColor = .RGB(138,138,145)
        lblDate.font = UIFont.systemFont(ofSize:  13)
        
    }
    
    
    func updateUI()  {
        self.lblTitle.text = (self.noteLocal.title ?? "")
        if self.lblTitle.text == nil || self.lblTitle.text!.count == 0 {
             self.lblTitle.text = (self.noteLocal.note ?? "-")
        }
//        self.lblNote.text = (self.noteLocal.note ?? "-")
        if let date = self.noteLocal.date_created {
            self.lblDate.text = date.format(with: "dd MMM yyyy, hh:mm a")
        }else{
            self.lblDate.text = "-"
        }
        
    }
}
