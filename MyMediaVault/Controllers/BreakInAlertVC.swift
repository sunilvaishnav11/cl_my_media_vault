//
//  BreakInAlertVC.swift
//  SecretVault
//
//  Created by Macbook on 15/08/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit
import RealmSwift
//import MediaBrowser
import DKCamera
class BreakInAlertVC: SVBaseController {

    let lblNote:UILabel = UILabel()
    let switchEnableBreakInAlert:UISwitch = UISwitch()
    let tblBreakIn:UITableView = UITableView()
    var arrBreakInAlert:[BreakInAlert] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.title = "Break-In Alert"
        self.setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUI()
        

//        DKCamera.checkCameraPermission { (success) in
//
//        }
    }
    
    override func setupUI() {
        super.setupUI()
        
        view.addSubview(lblNote)
        lblNote.snp.makeConstraints { (m) in
            m.top.equalTo(self.view.snp.top)
            m.left.equalTo(20)
            m.right.equalTo(-70)
            m.height.equalTo(70)
        }
        
        view.addSubview(switchEnableBreakInAlert)
        switchEnableBreakInAlert.snp.makeConstraints { (m) in
            m.left.equalTo(self.lblNote.snp.right)
            m.right.equalToSuperview()
            m.centerY.equalTo(self.lblNote.snp.centerY)
        }
        
        view.addSubview(tblBreakIn)
        tblBreakIn.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview()
            m.top.equalTo(lblNote.snp.bottom)
            m.bottomMargin.equalTo(self.view.snp.bottomMargin)
        }
        
        lblNote.textAlignment = .left
        lblNote.textColor = .darkGray
        lblNote.font = UIFont.boldSystemFont(ofSize: 15)
        lblNote.text = "Break-in Alert"
        lblNote.addBottomBorderWithHeight(height: 0.5, color: .darkGray, leftOffset: -20, rightOffset: -70, bottomOffset: 1)
        
        switchEnableBreakInAlert.onTintColor = UIColor.appBlue
        switchEnableBreakInAlert.addTarget(self, action: #selector(actionSwitchBrekinChange), for: .valueChanged)
        
        tblBreakIn.register(CellBreakInItem.self, forCellReuseIdentifier: "CellBreakInItem")
        tblBreakIn.delegate = self
        tblBreakIn.dataSource = self
        tblBreakIn.tableFooterView = UIView()
        
        tblBreakIn.separatorStyle = .singleLine
        tblBreakIn.separatorInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 0)
        
        tblBreakIn.emptyDataSetSource = self
        tblBreakIn.emptyDataSetDelegate = self
        tblBreakIn.tableFooterView = UIView()

        tblBreakIn.reloadData()
        
        
    }
    
    override func updateUI()  {
        
        self.switchEnableBreakInAlert.isOn = SVSharedClass.shared.enableBreakInAlert

        self.arrBreakInAlert = BreakInAlertVC.GetBreakInAlerts()

        /*
        //test ss
        self.arrBreakInAlert = []
        
        var record = BreakInAlert()
        record.date_created = Date().add(3.days).add(3.hours)
        record.wrong_passcode = "4589"
//        record.image = UIImage.init(named: "test1.jpg")
        self.arrBreakInAlert.append(record)
        
        record = BreakInAlert()
        record.date_created = Date().add(3.days).add(4.hours).add(1.seconds)
        record.wrong_passcode = "1100"
//        record.image = UIImage.init(named: "test2.jpg")
        self.arrBreakInAlert.append(record)
        
        record = BreakInAlert()
        record.date_created = Date().add(3.days).add(4.hours).add(30.minutes)
        record.wrong_passcode = "0000"
//        record.image = UIImage.init(named: "test3.jpg")
        self.arrBreakInAlert.append(record)
        
        record = BreakInAlert()
        record.date_created = Date().add(2.days).subtract(4.hours)
        record.wrong_passcode = "1999"
//        record.image = UIImage.init(named: "test4.jpg")
        self.arrBreakInAlert.append(record)
        
        record = BreakInAlert()
        record.date_created = Date().add(2.days).add(4.hours).add(1.minutes)
        record.wrong_passcode = "3476"
//        record.image = UIImage.init(named: "test2.jpg")
        self.arrBreakInAlert.append(record)
        
        record = BreakInAlert()
        record.date_created = Date().add(1.days).subtract(7.hours).add(10.seconds)
        record.wrong_passcode = "2020"
//        record.image = UIImage.init(named: "test5.jpg")
        self.arrBreakInAlert.append(record)
        */
        
        tblBreakIn.reloadData()

    }
    class func GetBreakInAlerts() -> [BreakInAlert] {
        
        let realm = try! Realm()
        let result = realm.objects(BreakInAlert.self)
        return Array(result).sorted(by: { (obj1, obj2) -> Bool in
            return obj1.date_created! > obj2.date_created!
        })

        
    }
    
    @objc func actionSwitchBrekinChange()  {
        SVSharedClass.shared.enableBreakInAlert = self.switchEnableBreakInAlert.isOn
    }
    
    func cellDeleteTappedAt(indexPath:IndexPath)  {
        let breakIn = arrBreakInAlert[indexPath.row]
        
        let full_path = appDataFolder.appendingPathComponent(breakIn.image_path ?? "")
        if FileManager.default.fileExists(atPath: full_path.path) == true{
           try? FileManager.default.removeItem(at: full_path)
        }
        let realm = try! Realm()
        try! realm.write {
            realm.delete(breakIn)
        }
        self.updateUI()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BreakInAlertVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBreakInAlert.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellBreakInItem", for: indexPath) as! CellBreakInItem
        cell.breakInAlert = arrBreakInAlert[indexPath.row]
        cell.delegate = self

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let breakIn = arrBreakInAlert[indexPath.row]
        
//        let breakInDetail = BreakInImageFull()
//        breakInDetail.breakInAlert = breakIn
//        self.navigationController?.pushViewController(breakInDetail, animated: true)
        
        /*
        let browser = MediaBrowser(delegate: self)
        browser.enableGrid = false
        browser.navigationBarStyle = .default
        browser.navigationBarTextColor =  UIColor.black
        browser.navigationBarTintColor = .white
        browser.setCurrentIndex(at: indexPath.row)
        browser.alwaysShowControls = true
        
        browser.displayActionButton = true
        self.navigationController?.pushViewController(browser, animated: true)
        */
        
        
    }

}
/*
extension BreakInAlertVC:MediaBrowserDelegate{
    func numberOfMedia(in mediaBrowser: MediaBrowser) -> Int {
                return self.arrBreakInAlert.count

    }
    
    func media(for mediaBrowser: MediaBrowser, at index: Int) -> Media {
        let mediaLocal = self.arrBreakInAlert[index]
        
        if index < self.arrBreakInAlert.count {
            if mediaLocal.image_path != nil {
                return Media.init(url: appDataFolder.appendingPathComponent(mediaLocal.image_path!))
            }else{
                return Media.init(image: UIImage.init(named: "no_profile")!)
            }
        }
        return Media.init()

    }
    func title(for mediaBrowser: MediaBrowser, at index: Int) -> String? {
        return "\(index+1)/\(arrBreakInAlert.count)"
    }
    

}
*/

extension BreakInAlertVC : SwipeTableViewCellDelegate{
 
  func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .none
        options.transitionStyle = .drag
        return options
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        
        let deleteAction = SwipeAction(style: .destructive, title: nil) { [weak self] action, indexPath in
            // handle action by updating model with deletion
            guard let self = self else { return }
            self.cellDeleteTappedAt(indexPath: indexPath)
            
        }
        deleteAction.textColor = .black

        // customize the action appearance
        deleteAction.image = UIImage(named: "delete_trash")?.withRenderingMode(.alwaysTemplate)

        return [deleteAction]
    }

}
extension BreakInAlertVC:DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "no_breakin_attempt")
    }

    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let nameBuilder = AttributedStringBuilder()
        nameBuilder.text("No Break-In Attempts", attributes: [.font(UIFont.systemFont(ofSize: 27)),.textColor(.black),.alignment(.center)])
        
        return nameBuilder.attributedString

    }
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let nameBuilder = AttributedStringBuilder()
        nameBuilder.text("When someone tries to open app with wrong passcode\nIt will appear here with passcode used.", attributes: [.font(UIFont.systemFont(ofSize: 18)),.textColor(.darkGray),.alignment(.center)])
        
        return nameBuilder.attributedString

    }
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> NSAttributedString? {
        let strBtn = AttributedStringBuilder()
        strBtn.text("Try yourself",attributes: [.textColor(.blue),.font(.boldSystemFont(ofSize: 15))])
        
        return strBtn.attributedString
    }
    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
        
        let alert = UIAlertController.alertViewController(withTitle: "Try Yourself", message: "When passcode screen appears, first time enter random wrong passcode and second time enter your original passcode to return here.")
        alert.addCancelButton()
        alert.addButton(withTitle: "Try") { (action) in
            AppDel.showPasscodeController()
        }
        alert.show()
        
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        if self.arrBreakInAlert.count > 0 {
            return false
        }
        return true
    }
    

}


class CellBreakInItem: SwipeTableViewCell {
    var imgPhoto:UIImageView = UIImageView()
    var lblPasscode:UILabel = UILabel()
    var lblDate:UILabel = UILabel()
    
    var breakInAlert:BreakInAlert!{
        didSet{
            updateUI()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // code common to all your cells goes here
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setupUI()   {
        self.separatorInset = UIEdgeInsets.init(top: 0, left: 1400, bottom: 0, right: 0)
        contentView.addSubview(imgPhoto)
        contentView.addSubview(lblPasscode)
        contentView.addSubview(lblDate)
        
        imgPhoto.snp.makeConstraints { (m) in
            m.size.equalTo(70)
            m.left.equalTo(10)
            m.centerY.equalToSuperview()
        }
        lblDate.snp.makeConstraints { (m) in
            m.bottom.equalTo(self.contentView.snp.centerY)
//            m.left.equalTo(imgPhoto.snp.right).offset(10)
            m.left.equalTo(10)
}
        lblPasscode.snp.makeConstraints { (m) in
            m.top.equalTo(lblDate.snp.bottom)
            m.left.equalTo(lblDate.snp.left)
        }
        imgPhoto.isHidden = true

//        lblDate.textco
        lblPasscode.textAlignment = .left
        lblPasscode.textColor = .black
        
        lblDate.textAlignment = .left
        lblDate.textColor = .RGB(138,138,145)
        lblDate.font = UIFont.boldSystemFont(ofSize:  17)
        

        
    }
    func updateUI()  {
        let fullPath = appDataFolder.appendingPathComponent(self.breakInAlert.image_path ?? "")
        self.imgPhoto.image = UIImage.init(contentsOfFile: fullPath.path)
        if self.imgPhoto.image == nil {
            self.imgPhoto.image = UIImage.init(named: "no_profile")
        }
        self.imgPhoto.contentMode = .scaleAspectFit
        

        let code = (self.breakInAlert.wrong_passcode ?? "")
        let buildPasscode = AttributedStringBuilder.init()
        buildPasscode.defaultAttributes = [.textColor(.appLightGray),.font(UIFont.systemFont(ofSize: 15))]
        let attPasscode = buildPasscode.text("Passcode: ").text(code, attributes: [.textColor(.black),.font(UIFont.boldSystemFont(ofSize: 15))]).attributedString
        
        self.lblPasscode.attributedText = attPasscode//"Passcode: " + (self.breakInAlert.wrong_passcode ?? "")
        self.lblDate.text = self.breakInAlert.date_created!.format(with: "dd MMM, hh:mm:ss a")
        
        
        //test ss
//        self.imgPhoto.contentMode = .scaleAspectFill
//        self.imgPhoto.clipsToBounds  = true
//        self.imgPhoto.image = Toucan.init(image: self.breakInAlert.image!).resizeByCropping(.init(width: 200, height: 200)).image
        
        
    }
    
}

class BreakInImageFull: SVBaseController {
    let scrContentView:ImageScrollView = ImageScrollView()
    var breakInAlert:BreakInAlert!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override func setupUI() {
        super.setupUI()
        
        view.addSubview(scrContentView)
        scrContentView.snp.makeConstraints { (m) in
            m.edges.equalToSuperview()
        }
        
        scrContentView.minimumZoomScale = 0.5
        scrContentView.maximumZoomScale = 10
        scrContentView.setup()
        
        let fullPath = appDataFolder.appendingPathComponent(self.breakInAlert.image_path ?? "")
        if let image = UIImage.init(contentsOfFile: fullPath.path){
            scrContentView.display(image: image)
        }


    }
}
