//
//  SettingsVC.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit
import SafariServices
import Messages
import MessageUI
import CoreServices


//test change this
var app_id = "1529858048"
var url_terms = "https://firebasestorage.googleapis.com/v0/b/photovault-a9e80.appspot.com/o/terms.pdf?alt=media&token=1da45eb5-f7e9-47dd-942c-1888b784f259"
var url_privacy = "https://firebasestorage.googleapis.com/v0/b/photovault-a9e80.appspot.com/o/privacy.pdf?alt=media&token=96fbaad9-ef8c-4218-83fc-012fe080d667"
//var url_subscription = "https://docs.google.com/document/d/1VWdEbExUCUr8_Wkm6hiAlN_U03uv-0j53AjXdrzFA0s"

class SettingMenu: NSObject {
    var name:String = ""
    var icon:UIImage = UIImage()
    var keyId:String = ""

}
private struct KeySettings {
    static let secure_browser:String = "secure_browser"
    static let change_icon:String = "change_icon"
    static let remove_after_import:String = "remove_after_import"
    static let change_passcode:String = "change_passcode"
    static let master_recovery_key:String = "master_recovery_key"
    static let touch_id_enable:String = "touch_id_enable"
    static let breank_in_alert_enable:String = "breank_in_alert_enable"
    static let fake_passcode:String = "fake_passcode"
    static let facedwon_lock:String = "facedwon_lock"
    static let import_export:String = "import_export"
    static let rate_us:String = "rate_us"
    static let share_app:String = "share_app"
    static let contact_us:String = "contact_us"
    static let privacy:String = "privacy"
    static let terms:String = "terms"
    static let premium_vault:String = "preimum_vault"
    
}
 

class SettingsVC: SVBaseController {
    
    let tblSettings:UITableView = UITableView()
    var arrSettings:[[SettingMenu]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Settings"
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func setupUI() {
        super.setupUI()
        
        //For iOS 9 and Above
        if #available(iOS 9, *) {
//            self.tblSettings.cellLayoutMarginsFollowReadableWidth = false
        }
        
        //---menu
        var arrSection:[SettingMenu] = []
        
        var menu = SettingMenu()
        menu.name = "Remove After Import"
        menu.keyId = KeySettings.remove_after_import
        arrSection.append(menu)

        arrSettings.append(arrSection)
        
        //---
        arrSection = []
        menu = SettingMenu()
        menu.name = "Change Passcode"
        menu.keyId = KeySettings.change_passcode
        arrSection.append(menu)
        
        menu = SettingMenu()
        menu.name = "Master Recovery Key"
        menu.keyId = KeySettings.master_recovery_key
        arrSection.append(menu)
        
        menu = SettingMenu()
        menu.name = "Touch ID"
        menu.keyId = KeySettings.touch_id_enable
        arrSection.append(menu)
        
        menu = SettingMenu()
        menu.name = "Break-In Alert"
        menu.keyId = KeySettings.breank_in_alert_enable
        arrSection.append(menu)
        
        menu = SettingMenu()
        menu.name = "Fake Passcode"
        menu.keyId = KeySettings.fake_passcode
        arrSection.append(menu)

        menu = SettingMenu()
        menu.name = "Facedown Lock"
        menu.keyId = KeySettings.facedwon_lock
        arrSection.append(menu)

//        menu = SettingMenu()
//        menu.name = "Export/Import Data"
//        menu.keyId = KeySettings.import_export
//        arrSection.append(menu)
        
        arrSettings.append(arrSection)
        
        //---
        arrSection = []
        menu = SettingMenu()
        menu.name = "Rate US"
        menu.keyId = KeySettings.rate_us
        arrSection.append(menu)
        
        menu = SettingMenu()
        menu.name = "Share App"
        menu.keyId = KeySettings.share_app
        arrSection.append(menu)
        
        menu = SettingMenu()
        menu.name = "Contact Us"
        menu.keyId = KeySettings.contact_us
        arrSection.append(menu)
        
        menu = SettingMenu()
        menu.name = "Privacy Policy"
        menu.keyId = KeySettings.privacy
        arrSection.append(menu)

        menu = SettingMenu()
        menu.name = "Terms of Use"
        menu.keyId = KeySettings.terms
        arrSection.append(menu)

        arrSettings.append(arrSection)



        view.addSubview(tblSettings)
        tblSettings.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        tblSettings.backgroundColor = .appBackgroundtable
        tblSettings.separatorStyle =  .none
        tblSettings.register(CellSetting.self, forCellReuseIdentifier: "CellSetting")
        tblSettings.delegate = self
        tblSettings.dataSource = self
        tblSettings.tableFooterView = UIView()
        
//        MultiPeer.instance.delegate = self
//        MultiPeer.instance.initialize(serviceType: "vaulttransfer")
//        MultiPeer.instance.autoConnect()

        
    }
    
    
    override func updateUI()  {
     
        self.tblSettings.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SettingsVC:MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?){

        switch result {
        case .cancelled:
            print("Cancelled")
        case .sent:
            print("Sent")
        case .failed:
            print("Error")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    
}

extension SettingsVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSettings.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettings[section].count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = ViewHeaderSetting()
        if section == 0 {
            header.lblName.text = "General".uppercased()
        }else if section == 1{
            header.lblName.text = "Safety".uppercased()
        }else if section == 2{
            header.lblName.text = "Other".uppercased()
        }

        return header
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellSetting", for: indexPath) as! CellSetting
        let menu = arrSettings[indexPath.section][indexPath.row]

        cell.lblName.text = menu.name
//        cell.imgView.setImage(self.arrSettings[indexPath.row].icon, for: .normal)
        
        let keyID = menu.keyId
        
        
        
        if keyID == KeySettings.remove_after_import || keyID == KeySettings.touch_id_enable{
            cell.switchOption.isHidden = false
            cell.imgArrow.isHidden = true
            cell.accessoryType = .none
        }else{
            cell.imgArrow.isHidden = false
            cell.switchOption.isHidden = true
            //cell.accessoryType = .disclosureIndicator
        }
        

        if keyID == KeySettings.remove_after_import{
            cell.switchOption.isOn = SVSharedClass.shared.enableRemoveAfterImport
            cell.blockSwitchChanged = {
                SVSharedClass.shared.enableRemoveAfterImport = cell.switchOption.isOn
            }

        }else if keyID == KeySettings.touch_id_enable{
            cell.switchOption.isOn = SVSharedClass.shared.enableTouchID

            cell.blockSwitchChanged = {
                SVSharedClass.shared.enableTouchID = cell.switchOption.isOn
            }

        }
//        if keyID == KeySettings.breank_in_alert_enable{
//            cell.switchOption.isOn = SVSharedClass.shared.enableBreakInAlert
//            cell.blockSwitchChanged = {
//                SVSharedClass.shared.enableBreakInAlert = cell.switchOption.isOn
//            }
//        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menu = arrSettings[indexPath.section][indexPath.row]
        tblSettings.deselectRow(at: indexPath, animated: false)
        

        if menu.keyId == KeySettings.secure_browser {
            //self.openSecureBrowser()
        }
        if menu.keyId == KeySettings.facedwon_lock {
            self.openFaceDownLockOption()
        }
        if menu.keyId == KeySettings.change_icon {
            self.openChangeIconOption()
        }
        if menu.keyId == KeySettings.fake_passcode {
            self.openFakePasscodeOption()
        }
        if menu.keyId == KeySettings.change_passcode {
            AppDel.showSetFirstTimePasscodeController(isChaningPasscode: true)
        }
        if menu.keyId == KeySettings.breank_in_alert_enable {
            self.openBrekInAlertOption()
        }
        if menu.keyId == KeySettings.master_recovery_key {
            self.openMasterKeyOption()
        }
        if menu.keyId == KeySettings.import_export {
            self.opneImportExportOption()
        }
        if menu.keyId == KeySettings.premium_vault {
            //self.openPremiumVaultSalesPage()
        }

        
        if menu.keyId == KeySettings.rate_us {
            let strRate = "https://itunes.apple.com/app/id\(app_id)?mt=8&action=write-review"
            if let rateURL = URL.init(string: strRate) {
                UIApplication.shared.open(rateURL, options: [:], completionHandler: nil)
            }
        }
        
        if menu.keyId == KeySettings.share_app {
            
            let urlStr = "https://apps.apple.com/us/app/id\(app_id)"
            let ac = UIActivityViewController(activityItems: [urlStr], applicationActivities: nil)
            if let popoverController = ac.popoverPresentationController {
                popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
                popoverController.sourceView = self.view
                popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            }
            self.present(ac, animated: true)

        }
        if menu.keyId == KeySettings.contact_us {
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self

                mail.setToRecipients(["handmadeapps.ios@gmail.com"])
                mail.setSubject("Photo Vault Help")
                mail.setMessageBody("", isHTML: true)
                
                present(mail, animated: true, completion: nil)
            } else {
                print("Cannot send mail")
                // give feedback to the user
            }

        }
        if menu.keyId == KeySettings.privacy {
            self.openSafari(strURL: url_privacy)
        }
        if menu.keyId == KeySettings.terms {
            self.openSafari(strURL: url_terms)
        }



    }
    
    func openFaceDownLockOption()  {
        let facedown = FaceDownDetailVC()
        facedown.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(facedown, animated: true)
    }
    func openChangeIconOption()  {
//        let changeIcon = ChangeIconVC()
//        changeIcon.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(changeIcon, animated: true)
    }
    func openFakePasscodeOption()  {
        let fake = FakePasscodeVC()
        fake.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(fake, animated: true)
    }
    func openBrekInAlertOption()  {
        if ISalesManager.shared.isSubscribed() == false{
            SalesVC.PresentSubscriptionPage(self)
            return
        }

        let breakIn = BreakInAlertVC()
        breakIn.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(breakIn, animated: true)
    }
    func openMasterKeyOption()  {
        let mastervc = MasterKeyVC()
        mastervc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(mastervc, animated: true)
    }
    func opneImportExportOption()  {
        
        let alert = UIAlertController.init(title: "Choose option", message: nil, preferredStyle: .actionSheet)
        alert.addButton(withTitle: "Export") { (action) in
            let zipPath = AppFolder.Documents.url.appendingPathComponent("export.zip")
            
            if FileManager.default.fileExists(atPath: zipPath.path) == true{
                try? FileManager.default.removeItem(at: zipPath)
            }

            SVProgressHUD.show()
//            try? FileManager.default.zipItem(at: appDataFolder, to: zipPath)
            try? FileManager.default.zipItem(at: appDataFolder, to: zipPath, shouldKeepParent: false, compressionMethod: .none, progress: nil)
            print(appDataFolder.absoluteString)
            SVProgressHUD.dismiss()

            let activityViewController = UIActivityViewController(activityItems: [zipPath], applicationActivities: nil)
            activityViewController.excludedActivityTypes = [.airDrop, .postToTwitter]
            self.present(activityViewController, animated: true, completion: nil)
            
//            let types: [String] = [kUTTypeZipArchive as String]
//            let importMenu = UIDocumentMenuViewController(documentTypes: types, in: .import)
//            self.present(importMenu, animated: true, completion: nil)
            

            /*
            let zipPath = AppFolder.Documents.url.appendingPathComponent("export.zip")
            
            if FileManager.default.fileExists(atPath: zipPath.path) == true{
                try? FileManager.default.removeItem(at: zipPath)
            }

            try? FileManager.default.zipItem(at: AppFolder.Documents.url, to: zipPath)
            
            
            if let data = try? Data.init(contentsOf: zipPath, options: .alwaysMapped){
                MultiPeer.instance.stopSearching()
                MultiPeer.instance.send(data: data, type: 0)
                MultiPeer.instance.autoConnect()

            }*/
//            let progress = sendResourceAtURL(zipPath, withName: "Export.zip") { (error) in
//                print(error)
//                if error == nil{
//                    self.do {
//                        self.view.makeToast("sent")
//                    }
//                }
//            }
            
        }
        alert.addButton(withTitle: "Import") { [weak self](action) in
            guard let self = self else { return }

            let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.zip-archive"], in: .import)

            documentPicker.delegate = self
            self.present(documentPicker, animated: true, completion: nil)

            
//            onFinishReceivingResource = {(myPeerID,resourceName, peer, localURL) in
//                print(localURL)
//
//                try? FileManager.default.unzipItem(at: localURL!, to: AppFolder.Documents.url)
//
//                self.do {
//                    self.view.makeToast("Received \(resourceName)")
//                }
//            }
        }
        alert.addCancelButton()
        alert.show()
        
    }

    
    
    func openSafari(strURL:String)  {
        if let url = URL.init(string: strURL){
            let safari = SFSafariViewController.init(url: url)
            self.present(safari, animated: true, completion: nil)
            //self.navigationController?.pushViewController(safari, animated: true)
        }
    }



}
extension SettingsVC: UIDocumentPickerDelegate{
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let zipPath = AppFolder.Documents.url.appendingPathComponent("export.zip")
        
        if FileManager.default.fileExists(atPath: zipPath.path) == true{
            try? FileManager.default.removeItem(at: zipPath)
        }
        
        try? FileManager.default.copyItem(at: url, to: zipPath)
        
        //        //delete files
        //        if FileManager.default.fileExists(atPath: appDataFolder.path) == true{
        //            try? FileManager.default.removeItem(at: appDataFolder)
        //        }
        let tmpAppFolder  = AppFolder.Documents.appending("tmp_appdata/").url
        
        SVProgressHUD.show()
        //unzip
        try? FileManager.default.unzipItem(at: zipPath, to: tmpAppFolder)
        
        //check if tmpAppFolder contain .real files
        if FileManager.default.fileExists(atPath: tmpAppFolder.appendingPathComponent("default.realm").path) == true{
            
            //if yes
            //delete current and rename tmpAppfolder to  Appfolder
            let alert = UIAlertController.alertViewController(withTitle: "Alert", message: "This action will remove all content and replace it with imported contents. This action cannot be undone.")
            alert.addCancelButton(withTitle: "Cancel") { (action) in
                //if no remove ziped folder
                try? FileManager.default.removeItem(at: tmpAppFolder)
            }
            alert.addButton(withTitle: "Import") {[weak self] (action) in
                guard let self = self else { return }

                if FileManager.default.fileExists(atPath: appDataFolder.path) == true{
                    try? FileManager.default.removeItem(at: appDataFolder)
                }
                try? FileManager.default.moveItem(at: tmpAppFolder, to: appDataFolder)
                
                let alert = UIAlertController.alertViewController(withTitle: "Import Success", message: "You need to restart app to apply changes. Tapping on Restart will close the app. After that tap on app icon to open app.")
                alert.addButton(withTitle: "Restart", action: { (action) in
                    let array:[String] = []
                    let second = array[0] //crash
                })
                alert.show()

            }
            alert.show()
            
        }else{
            self.view.makeToast("Invalid Format")
            //if no remove ziped folder
            try? FileManager.default.removeItem(at: tmpAppFolder)
        }
        
        try? FileManager.default.removeItem(at: zipPath)
        
        print(AppFolder.Documents.url)
        SVProgressHUD.dismiss()

    }
}
/*
extension SettingsVC:MultiPeerDelegate{
    func multiPeer(didReceiveData data: Data, ofType type: UInt32) {
        
        let zipPath = AppFolder.Documents.url.appendingPathComponent("export.zip")
        if FileManager.default.fileExists(atPath: zipPath.path) == true{
            try? FileManager.default.removeItem(at: zipPath)
        }
        
        try? data.write(to: zipPath)
        if FileManager.default.fileExists(atPath: zipPath.path) == true{
            try? FileManager.default.unzipItem(at: zipPath, to: AppFolder.Documents.url)
        }
        
        let urlFolder = AppFolder.Documents.appending("Documents").url
        try? FileManager.default.contentsOfDirectory(at: urlFolder, includingPropertiesForKeys: nil, options: .skipsHiddenFiles).forEach { (url) in
            
            let finalURL = AppFolder.Documents.url.appendingPathComponent(url.lastPathComponent)
            try? FileManager.default.removeItem(at: finalURL)
            try? FileManager.default.moveItem(at: url, to: finalURL)
        }
        
        
        

    }
    func multiPeer(connectedDevicesChanged devices: [String]) {
        print(devices)
    }
}*/
class CellSetting: UITableViewCell {
    var contentViewCell:UIView = UIView()
    var imgView:UIButton = UIButton()
    var lblName:UILabel = UILabel()
    var lblValue:UILabel = UILabel()
    var switchOption:UISwitch = UISwitch()
    var imgArrow:UIImageView = UIImageView()

    var blockSwitchChanged:BlockCompletion?
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
//        self.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        contentView.addSubview(contentViewCell)
        contentViewCell.snp.makeConstraints { (m) in
            m.width.equalToSuperview().offset(-30)
            m.centerX.equalToSuperview()
            m.height.equalToSuperview().offset(-10)
            m.centerY.equalToSuperview()
        }
        self.backgroundColor = .clear
        contentView.backgroundColor = .clear
        contentViewCell.backgroundColor  = .white
        contentViewCell.layer.cornerRadius = 30
        contentViewCell.clipsToBounds = true
             
        contentViewCell.addSubview(imgView)
        imgView.snp.makeConstraints { (make) in
            make.size.equalTo(50)
            make.centerY.equalToSuperview()
            make.left.equalTo(20)
        }
        imgView.layer.cornerRadius = 25
//        imgView.layer.borderColor = UIColor.RGB(248, 250, 252).cgColor//UIColor.white.withAlphaComponent(0.5).cgColor
//        imgView.layer.borderWidth = 4
//        imgView.setImage(UIImage.init(named: "pin_user"), for: .normal)
        let offset:CGFloat = 11.0
        imgView.imageEdgeInsets = UIEdgeInsets.init(top: offset, left: offset, bottom: offset, right: offset)
        imgView.isUserInteractionEnabled = false
        imgView.contentMode = .scaleAspectFit
        //#colorLiteral(red: 0.9999235272, green: 1, blue: 0.9998828769, alpha: 1)
        imgView.backgroundColor = UIColor.RGB(237, 239, 241)//.withAlphaComponent(0.5)
        imgView.tintColor = UIColor.black
        
        imgView.isHidden = true
        contentViewCell.addSubview(lblName)
        lblName.snp.makeConstraints { (make) in
//            make.left.equalTo(imgView.snp.right).offset(15)
            make.left.equalTo(20)
            make.centerY.equalToSuperview()
        }
        lblName.textColor = UIColor.darkText
        lblName.font = UIFont.boldSystemFont(ofSize: 14)
        lblName.textAlignment = .left
        
        contentViewCell.addSubview(switchOption)
        switchOption.snp.makeConstraints { (make) in
            make.right.equalToSuperview().inset(10)
            make.centerY.equalToSuperview()
        }
        switchOption.isHidden = true
        switchOption.onTintColor = UIColor.appBlue
        
        switchOption.addTarget(self, action: #selector(switchValueChanged), for: .valueChanged)
        
        contentViewCell.addSubview(imgArrow)
        imgArrow.snp.makeConstraints { (m) in
            m.size.equalTo(22)
            m.centerY.equalToSuperview()
            m.right.equalToSuperview().inset(10)
        }
        imgArrow.tintColor = UIColor.appBlue
        imgArrow.image = UIImage.init(systemName: "chevron.forward")?.withRenderingMode(.alwaysTemplate)
        imgArrow.contentMode = .scaleAspectFit
        imgArrow.isHidden = true
        
        contentViewCell.addSubview(lblValue)
        lblValue.snp.makeConstraints { (m) in
            m.right.equalToSuperview().inset(10)
            m.centerY.equalToSuperview()
        }
        lblValue.isHidden = true
        lblValue.textAlignment = .right
        lblValue.textColor = .darkGray
        lblValue.font = UIFont.systemFont(ofSize: 15)
        
    }
    
    @objc func switchValueChanged() {
        DispatchQueue.main.async {
            if let block = self.blockSwitchChanged {
                block()
            }

        }
    }

//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        // Remove seperator inset
//        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
//            cell.separatorInset = .zero
//        }
//        // Prevent the cell from inheriting the Table View's margin settings
//        if cell.responds(to: #selector(setter: UITableViewCell.preservesSuperviewLayoutMargins)) {
//            cell.preservesSuperviewLayoutMargins = false
//        }
//        // Explictly set your cell's layout margins
//        if cell.responds(to: #selector(setter: UITableViewCell.layoutMargins)) {
//            cell.layoutMargins = .zero
//        }
//    }
}

private class ViewHeaderSetting: UIView {
    var lblName:UILabel = UILabel()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        self.addSubview(lblName)
        lblName.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.centerY.equalToSuperview().offset(10)
        }
        lblName.textAlignment = .left
        lblName.font = UIFont.boldSystemFont(ofSize: 19)
        lblName.textColor = UIColor.RGB(123,124,125)
        
        self.backgroundColor = .appBackgroundtable
    }
}

