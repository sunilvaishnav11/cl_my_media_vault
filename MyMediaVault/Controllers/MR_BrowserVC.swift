//
//  BrowserVC.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit
import WebKit
import RealmSwift

class MR_BrowserVC: SVBaseController,UISearchBarDelegate {
    let webTitleBar:WebTitleBar = WebTitleBar()
    var webView:WKWebView = WKWebView()
    var bottomBar:BottomBarActionView = BottomBarActionView()
    
    
    var currentURL:URL?
    var webpageLocal:WebPageLocal!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //-------------
        let realm = try! Realm()
        let result = realm.objects(WebPageLocal.self)
        self.webpageLocal = result.first
        
        if result.count == 0 {
            self.webpageLocal = WebPageLocal.init()
            self.webpageLocal.url = SVSharedClass.shared.homeURL
            let realm = try! Realm()
            try! realm.write {
                realm.add(self.webpageLocal)
            }
        }
        //-------------
        
        self.addRightBarButton(image: UIImage.init(named: "tab_window") ?? UIImage())
        self.navigationItem.rightBarButtonItem?.tintColor = .appBlue

        self.view.backgroundColor = .appBackgroundtable
        self.setupUI()
        updateUI()
        
        self.bottomBar.btnDownload.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func setupUI() {
        super.setupUI()
        
        view.addSubview(webTitleBar)
        webTitleBar.snp.makeConstraints { (m) in
            m.left.top.right.equalToSuperview()
            m.height.equalTo(50)
        }
        
        view.addSubview(webView)
        webView.snp.makeConstraints { (m) in
            m.top.equalTo(webTitleBar.snp.bottom)
            m.left.right.equalToSuperview()
        }
        
        view.addSubview(bottomBar)
        bottomBar.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview()
            m.top.equalTo(webView.snp.bottom)
            m.height.equalTo(50)
            m.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)//.offset(-30)
        }

        
        webTitleBar.searchBar.delegate = self

        
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        self.webView.configuration.websiteDataStore = .nonPersistent()
        
        self.webTitleBar.btnReload.addTarget(self, action: #selector(btnReloadTapped), for: .touchUpInside)
        self.bottomBar.btnPreviouse.addTarget(self, action: #selector(btnPreviouseTapped), for: .touchUpInside)
        self.bottomBar.btnNext.addTarget(self, action: #selector(btnNextTapped), for: .touchUpInside)
        self.bottomBar.btnHome.addTarget(self, action: #selector(btnHomeTapped), for: .touchUpInside)
        self.bottomBar.btnDownload.addTarget(self, action: #selector(btnDownloadTapped), for: .touchUpInside)

        let bnt1 = ButtonKeyboardShortcut(title: "www.")
        let bnt2 = ButtonKeyboardShortcut(title: "/")
        let bnt3 = ButtonKeyboardShortcut(title: ".")
        let bnt4 = ButtonKeyboardShortcut(title: ".com")
        let btns = [bnt1,bnt2,bnt3,bnt4]
        btns.forEach { (btn) in
            btn.onTap {[weak self] in
                guard let self = self else { return }
                
                if  self.webTitleBar.searchBar.textField.selectedTextRange != nil{
                    if let selectedText = self.webTitleBar.searchBar.textField.text(in: self.webTitleBar.searchBar.textField.selectedTextRange!), selectedText.count > 0{
                        self.webTitleBar.searchBar.text = ""
                    }

                }
                self.webTitleBar.searchBar.text = (self.webTitleBar.searchBar.text ?? "") + btn.titleLabel!.text!
            }
        }
        let stack =  UIStackView.init(arrangedSubviews:btns)
        stack.axis = .horizontal
        stack.spacing  = 10
        
        stack.distribution = .fillEqually
//        stack.snp.makeConstraints { (m) in
//            m.width.equalTo(AppDel.window!.frame.width)
//            m.height.equalTo(50)
//        }
        let bgStack = UIView()
        stack.addSubview(bgStack)
        bgStack.snp.makeConstraints { (m) in
            m.edges.equalToSuperview()
        }
        bgStack.superview?.sendSubviewToBack(bgStack)
        
        bgStack.backgroundColor = .white
        stack.frame = CGRect.init(x: 0, y: 0, width: (AppDel.window!.frame.width), height: 40.0)
        webTitleBar.searchBar.inputAccessoryView = stack
        webTitleBar.searchBar.inputAccessoryView?.backgroundColor = .white
    }
    
    override func actionRightBarTapped(sender: UIBarButtonItem) {
//        self.imgWeb.image = webpagelocal.imageScreen
        
        self.updateSceenshot()
        
        let menu = MenuBrowserVC()
        menu.selWebpagelocal = self.webpageLocal
        menu.blockWebPageSelect = {[weak self ]webpageSelect in
            guard let self = self else { return }
            if let webpageSelect = webpageSelect {
                if self.webpageLocal != webpageSelect{
                    self.webpageLocal = webpageSelect
                    if let strUrl = self.webpageLocal.url {
                        if let url = URL.init(string: strUrl) {
                            self.showURL(url:url)
                        }
                    }
                }
            }else{
                self.webpageLocal = WebPageLocal.init()
                self.webpageLocal.url = SVSharedClass.shared.homeURL
                let realm = try! Realm()
                try! realm.write {
                    realm.add(self.webpageLocal)
                }
                if let strUrl = self.webpageLocal.url {
                    if let url = URL.init(string: strUrl) {
                        self.showURL(url:url)
                    }
                }
            }
            
        }
        self.navigationController?.pushViewController(menu, animated: true)
    }
    
    override func updateUI()  {
        //load last webpage local
        
        if let strUrl = self.webpageLocal.url {
            if let url = URL.init(string: strUrl) {
                self.showURL(url:url)
            }
        }

        
    }
    

    
    func updateSceenshot()  {
        let imageScreen = webView.takeScreenshot()
        self.webpageLocal.imageScreen = imageScreen
    }
    
    class ButtonKeyboardShortcut: UIButton {
        public override init(frame: CGRect) {
            super.init(frame: frame)
            setupUI()
        }
        
        required public init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            setupUI()
        }
        convenience init(title:String) {
            self.init(frame: .zero)
            self.setTitle(title, for: .normal)
        }
        func setupUI() {
            self.backgroundColor = UIColor.appBackgroundtable.withAlphaComponent(0.8)
            self.setTitleColor(.black, for: .normal)
        }
    }
    
    func showURL(url:URL) {
        self.currentURL = url
        
        var request = URLRequest.init(url: url)
        request.cachePolicy = .reloadIgnoringLocalCacheData
        webView.load(request)
        self.webTitleBar.searchBar.text = webView.url?.absoluteString
        self.webTitleBar.searchBar.setShowsCancelButton(false, animated: true)
        self.webTitleBar.searchBar.resignFirstResponder()
        self.webTitleBar.btnReload.isHidden = false

        self.updateToolbarState()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.webTitleBar.btnReload.isHidden = true
        self.webTitleBar.searchBar.textField.selectAll(nil)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text{
            if var url = URL.init(string: text),text.contains("."){
                if text.contains("http") == false {
                    if let urlhttp = URL.init(string: "http://" + text),UIApplication.shared.canOpenURL(urlhttp) == true{
                        url = urlhttp
                        self.showURL(url: url)
                    }
                }
            }else{
                //search
                //https://www.google.com/search?client=firefox-b-d&q=test+apple
                if let query = text.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                    let str = "https://www.google.com/search?q=\(query)"
                    if let url = URL.init(string: str){
                        self.showURL(url: url)
                    }
                }
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        self.webTitleBar.btnReload.isHidden = false
    }

    
    @objc func btnReloadTapped()  {
        self.webView.reload()
        self.updateToolbarState()
    }
    
    @objc func btnPreviouseTapped()  {
        self.webView.goBack()
        self.updateToolbarState()
    }
        
    @objc func btnNextTapped()  {
        self.webView.goForward()
        self.updateToolbarState()

    }
    @objc func btnHomeTapped()  {
        

        let realm = try! Realm()
        try! realm.write {
            webpageLocal.url = SVSharedClass.shared.homeURL
            self.showURL(url: URL.init(string: webpageLocal.url!)!)
            self.updateToolbarState()
        }
    }

    @objc func btnDownloadTapped()  {
        

    }
    func updateToolbarState()  {
        let canGoBack = self.webView.canGoBack
        let canGoForward = self.webView.canGoForward
         
        self.bottomBar.btnPreviouse.isEnabled = canGoBack
        self.bottomBar.btnNext.isEnabled = canGoForward

        if let strUrl = webView.url{
            self.webTitleBar.searchBar.text = strUrl.absoluteString
        }


    }
    
    
    


}
extension MR_BrowserVC:WKUIDelegate{
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        if navigationResponse.response.url != nil {
            self.webTitleBar.searchBar.text = navigationResponse.response.url!.absoluteString
            

        }
        decisionHandler(.allow)
    }

}
extension MR_BrowserVC:WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.updateToolbarState()
        
        self.bottomBar.btnDownload.isEnabled = false

    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.bottomBar.btnDownload.isEnabled = true

        if let strUrl = webView.url?.absoluteString{
            if self.isVisible() == true {
                let realm = try! Realm()
                try! realm.write {
                    self.webpageLocal.url = strUrl
                }

                self.updateSceenshot()
                
            }
        }

        
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        self.updateToolbarState()
        
        self.bottomBar.btnDownload.isEnabled = true

        /*
        if error != nil {
            if let text = self.webTitleBar.searchBar.text{
                self.webTitleBar.searchBar.text = ""
                if let query = text.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                    let str = "https://www.google.com/search?q=\(query)"
                    if let url = URL.init(string: str){
                        self.showURL(url: url)
                    }
                }
            }
        }
        */
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.updateToolbarState()

    }
}

class WebTitleBar: UIView {
 
    let btnReload:UIButton = UIButton()
    let searchBar:UISearchBar = UISearchBar()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        /*
        addSubview(btnReload)
        btnReload.snp.makeConstraints { (m) in
            m.left.equalToSuperview()
            m.height.equalTo(self.snp.height)
            m.width.equalTo(self.snp.height).offset(-10)
            m.centerY.equalToSuperview()
        }
        
        addSubview(searchBar)
        searchBar.snp.makeConstraints { (m) in
            m.left.equalTo(btnReload.snp.right)
            m.right.equalToSuperview()
            m.centerY.equalToSuperview()
            m.height.equalToSuperview()
        }
        */
        
        let stack = UIStackView.init(arrangedSubviews: [btnReload,searchBar])
        addSubview(stack)
        stack.snp.makeConstraints { (m) in
            m.edges.equalToSuperview()
        }
        stack.distribution = .fill
        stack.axis = .horizontal
        
        btnReload.snp.makeConstraints { (m) in
            m.width.equalTo(self.snp.height).offset(-10)
        }
        

        
        btnReload.setImage(UIImage.init(named: "refresh"), for: .normal)
        
        
        searchBar.textField.rightViewMode = .whileEditing
        searchBar.backgroundImage = UIImage()
        searchBar.barTintColor = .appBackgroundtable
        searchBar.autocorrectionType  = .no
        searchBar.autocapitalizationType = .none
        
    }
}

class BottomBarActionView: UIView {
 
    let btnPreviouse:UIButton = UIButton()
    let btnNext:UIButton = UIButton()
    let btnDownload:UIButton = UIButton()
    let btnHome:UIButton = UIButton()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        backgroundColor = .appBackgroundtable
        addSubview(btnPreviouse)
        addSubview(btnNext)
        addSubview(btnHome)
        addSubview(btnDownload)
        
        btnPreviouse.snp.makeConstraints { (m) in
            m.height.equalTo(37)
            m.width.equalTo(45)
            m.left.equalTo(5)
            m.centerY.equalToSuperview()
        }
        btnNext.snp.makeConstraints { (m) in
            m.height.equalTo(37)
            m.width.equalTo(45)
            m.left.equalTo(btnPreviouse.snp.right).offset(5)
            m.centerY.equalToSuperview()
        }
        
        btnHome.snp.makeConstraints { (m) in
            m.height.equalTo(37)
            m.width.equalTo(45)
            m.right.equalToSuperview().offset(-5)
            m.centerY.equalToSuperview()
        }
        
        btnDownload.snp.makeConstraints { (m) in
            m.height.equalTo(37)
            m.width.equalTo(45)
            m.right.equalTo(btnHome.snp.left).offset(-10)
            m.centerY.equalToSuperview()
        }
        btnPreviouse.setImage(UIImage.init(named: "previouse"), for: .normal)
        btnNext.setImage(UIImage.init(named: "next"), for: .normal)
        btnHome.setImage(UIImage.init(named: "home"), for: .normal)
        btnDownload.setImage(UIImage.init(named: "download_img"), for: .normal)

        btnNext.imageView?.contentMode = .scaleAspectFit
        btnPreviouse.imageView?.contentMode = .scaleAspectFit
        btnHome.imageView?.contentMode = .scaleAspectFit
        btnDownload.imageView?.contentMode = .scaleAspectFit

        btnNext.imageEdgeInsets = .init(top: 5, left: 10, bottom: 5, right: 10)
        btnPreviouse.imageEdgeInsets = .init(top: 5, left: 10, bottom: 5, right: 10)
        btnHome.imageEdgeInsets = .init(top: 5, left: 10, bottom: 5, right: 10)
        btnDownload.imageEdgeInsets = .init(top: 5, left: 10, bottom: 5, right: 10)

    }
}

class MenuBrowserVC: SVBaseController,UICollectionViewDelegate,UICollectionViewDataSource,CollectionViewWaterfallLayoutDelegate {
    let collectionView:UICollectionView = UICollectionView.init(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var arrWebpages:[WebPageLocal] = []
    var blockWebPageSelect: ((WebPageLocal?) -> Void)!

    var selWebpagelocal:WebPageLocal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Tabs"
        self.view.backgroundColor = .appBackgroundtable
        self.setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.updateUI()
    }
    
    override func updateUI()  {
        let realm = try! Realm()
        let result = realm.objects(WebPageLocal.self)
        self.arrWebpages = Array(result).sorted(by: { (obj1, obj2) -> Bool in
            return obj1.date_created! < obj2.date_created!
        })

        self.collectionView.reloadData()
    }
    override func setupUI() {
        super.setupUI()
        
        view.backgroundColor = .white
        
        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = .init(top: 1, left: 2, bottom: 2, right: 2)
        layout.headerInset = .init(top: 5, left: 0, bottom: 0, right: 0)
        layout.footerInset = .init(top: 0, left: 0, bottom: 5, right: 0)
        layout.headerHeight = 8
        layout.footerHeight = 8
        layout.minimumColumnSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.columnCount = 2
        
        collectionView.register(CellWebPage.self, forCellWithReuseIdentifier: "CellWebPage")
        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .RGB(245,246,247)
        self.view.backgroundColor = collectionView.backgroundColor
        collectionView.delegate = self
        collectionView.dataSource = self
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (arrWebpages.count+1)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellWebPage", for: indexPath) as! CellWebPage
        if indexPath.row == arrWebpages.count {
            //last
            cell.webpagelocal = nil
            cell.isSelectedCell = false
        }else{
            cell.webpagelocal = arrWebpages[indexPath.row]
            if cell.webpagelocal?.identifier_unique == self.selWebpagelocal?.identifier_unique {
                cell.isSelectedCell = true
            }else{
                cell.isSelectedCell = false
            }
            cell.btnRemove.onTap {[weak self] in
                guard let self = self else { return }
                //self.blockWebPageRemove(self.arrWebpages[indexPath.row]);
                //delete
                cell.webpagelocal.removeScreenImageIfExist()
                let realm = try! Realm()
                try! realm.write {
                    realm.delete(cell.webpagelocal)
                }
                
                self.updateUI()

                if self.arrWebpages.count > 0{
                    self.selWebpagelocal = self.arrWebpages.first
                    self.collectionView.reloadData()

                    self.blockWebPageSelect(self.selWebpagelocal)
                    
                }else{
                    self.blockWebPageSelect(nil)
                    self.navigationController?.popViewController(animated: true)
                }

            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == arrWebpages.count {
            //last
            blockWebPageSelect(nil)
        }else{
            self.selWebpagelocal = arrWebpages[indexPath.row]
            self.collectionView.reloadData()
            blockWebPageSelect(arrWebpages[indexPath.row])
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.navigationController?.popViewController(animated: true)
        }


    }
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = self.view.window!.frame.width/CGFloat(2)
        return .init(width: width, height: width*1.34)
    }

    class CellWebPage: UICollectionViewCell {
        let vContainer:UIView = UIView()
        let imgWeb:UIImageView = UIImageView()
        let btnAdd = UIButton()
        let btnRemove = UIButton()
        var isSelectedCell:Bool = false{
            didSet{
                self.updateSelection()
            }
        }
        var webpagelocal:WebPageLocal!{
            didSet{
                self.updateUI()
            }
        }

        override init(frame: CGRect) {
            super.init(frame: frame)
            self.setupUI()
        }
        
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
        }
        
        func setupUI(){
            
            self.contentView.addSubview(vContainer)
            vContainer.snp.makeConstraints { (m) in
                m.edges.equalToSuperview().inset(20)
            }
            
            self.vContainer.addSubview(btnAdd)
            btnAdd.snp.makeConstraints { (m) in
                m.edges.equalToSuperview()
            }
            btnAdd.setImage(UIImage.init(named: "add_new"), for: .normal)
            
            self.vContainer.addSubview(self.imgWeb)
            imgWeb.snp.makeConstraints { (m) in
                m.edges.equalToSuperview()
            }
            
            self.contentView.addSubview(btnRemove)
            btnRemove.snp.makeConstraints { (m) in
                m.size.equalTo(30)
                m.centerX.equalTo(self.vContainer.snp.left)//.offset(4)
                m.centerY.equalTo(self.vContainer.snp.top)//.offset(-4)
            }
            btnRemove.setImage(UIImage.init(named: "close"), for: .normal)
            btnRemove.imageEdgeInsets = .zero
            btnRemove.backgroundColor = .RGB(208,208,208)
            btnRemove.layer.cornerRadius = 15
            btnRemove.layer.masksToBounds = true
            
            imgWeb.contentMode = .scaleAspectFill
            imgWeb.clipsToBounds = true
            imgWeb.layer.masksToBounds = true

            btnAdd.backgroundColor = .white
            imgWeb.backgroundColor = .white
            
            
            self.contentView.backgroundColor = UIColor.appBackgroundtable
            vContainer.backgroundColor = UIColor.white
            vContainer.layer.shadowColor = UIColor.lightGray.cgColor
            vContainer.layer.shadowOffset = CGSize(width: 1, height: 2.0)
            vContainer.layer.shadowRadius = 4
            vContainer.layer.shadowOpacity = 0.5
//            vContainer.layer.masksToBounds = false
//            vContainer.layer.cornerRadius = 5
//            vContainer.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: vContainer.layer.cornerRadius).cgPath

        }
        
        func updateUI()  {
            if self.webpagelocal != nil{
                btnAdd.isHidden = true
                imgWeb.isHidden = false
                btnRemove.isHidden = false

                self.imgWeb.image = webpagelocal.imageScreen
                
            }else{
                btnAdd.isHidden = false
                imgWeb.isHidden = true
                btnRemove.isHidden = true
            }
            btnAdd.isUserInteractionEnabled = false
            
        }
        
        func updateSelection()  {
            if self.isSelectedCell == true {
                self.contentView.backgroundColor = .appBlue
            }else{
                self.contentView.backgroundColor = .appBackgroundtable
            }
        }
        
            override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
                if self.btnAdd.isHidden == true {
                    self.contentView.backgroundColor = .appBlue
                }
                super.touchesBegan(touches, with: event)
            }
            override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
                self.contentView.backgroundColor = .appBackgroundtable
                super.touchesEnded(touches, with: event)

            }
            override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
                self.contentView.backgroundColor = .appBackgroundtable
                super.touchesCancelled(touches, with: event)
            }

    }
    
    
    
}


class WebpageImageVC: SVBaseController {
    let collectionView:UICollectionView = UICollectionView.init(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var array:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupUI()

    }
    override func setupUI() {
        super.setupUI()
        
        collectionView.register(CellWebpageImage.self, forCellWithReuseIdentifier: "CellWebpageImage")

        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = .init(top: 0, left: 5, bottom: 0, right: 5)
        layout.headerInset = .init(top: 5, left: 0, bottom: 0, right: 0)
        layout.footerInset = .init(top: 0, left: 0, bottom: 5, right: 0)
        layout.headerHeight = 5
        layout.footerHeight = 5
        layout.minimumColumnSpacing = 8
        layout.minimumInteritemSpacing = 8
        layout.columnCount = 2
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        
        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .RGB(245,246,247)
        self.view.backgroundColor = collectionView.backgroundColor
        collectionView.delegate = self
        collectionView.dataSource = self

        
        
        /*
        tableView.addElements(array, cell: UITableViewCell.self) { (item, cell, index) in
            if let url = URL.init(string: item){
                print(index)
                let cellNew =  cell
                cell.textLabel?.text = item
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                    cellNew.imageView?.kf.setImage(with: url)
                }
            }
            
            
//            if let data = try? Data.init(contentsOf: !),let image  = UIImage.init(data:data){
//                cell.imageView?.image = image
//                cell.textLabel?.text = item
//            }
        }.heightForRowAt { (index) -> CGFloat in
            return 60
        }
        */
        collectionView.reloadData()
        
    }
    
}

extension WebpageImageVC:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellWebpageImage", for: indexPath) as! CellWebpageImage
        
        var strUrl = array[indexPath.row]
        if strUrl.contains("base64") {
            if let url = URL.init(string: strUrl){
                if let data = try? Data.init(contentsOf: url){
                    cell.imgIcon.image = UIImage.init(data:data)
                }
            }
        }else{
            if strUrl.contains("http") == false{
                strUrl = "http:"+strUrl
            }
            if let url = URL.init(string: strUrl){
                cell.imgIcon.kf.setImage(with: url)
            }


        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? CellWebpageImage{
            if let image = cell.imgIcon.image{
                SVProgressHUD.show(withStatus: "Downloading...")
                SVSharedClass.shared.downloadAndSaveToMainFolder(image: image).continueWith { (task)  in
                    SVProgressHUD.dismiss()
                }
            }
        }
        
        
    }
}
extension WebpageImageVC:CollectionViewWaterfallLayoutDelegate{
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width =  self.view.window!.frame.width/CGFloat(2)
        return .init(width: width, height: width )
    }
}

class CellWebpageImage: UICollectionViewCell {
    let imgIcon:UIImageView = UIImageView()
    var lblName: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupUI(){
        self.contentView.addSubview(self.imgIcon)
        self.imgIcon.snp.makeConstraints { (m) in
            m.edges.equalToSuperview()
        }
        self.imgIcon.backgroundColor = .white
        self.imgIcon.layer.cornerRadius = 10
        self.imgIcon.layer.masksToBounds = true
        
        self.imgIcon.contentMode = .scaleAspectFill
    }
   
    
}
