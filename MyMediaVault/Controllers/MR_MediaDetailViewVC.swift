//
//  MediaDetailVC.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit
import RealmSwift
import DKImagePickerController
import Photos
import BoltsSwift

class MR_MediaDetailViewVC: SVBaseController {

    var folderLocal:FolderLocal!
    let clcMediaDetail:UICollectionView = UICollectionView.init(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var arrMedia:[MediaLocal] = []
    var toolbarEditMode:UIToolbar = UIToolbar()
    
    var arrSelMedia:[MediaLocal] = []{
        didSet{
            if arrSelMedia.count > 0 {
                self.btnDelete?.isUserInteractionEnabled = true
                self.btnDelete?.titleString = "Delete \(arrSelMedia.count) item" + ((arrSelMedia.count > 1 ) ? "s" : "")
            }else{
                self.btnDelete?.isUserInteractionEnabled = false
                self.btnDelete?.titleString = "Delete"
            }
        }
    }
    var isEditOn:Bool = false
    
    override func viewDidLoad() {

        super.viewDidLoad()
        self.title = folderLocal.display_name ?? ""

        clcMediaDetail.register(CellMedia.self, forCellWithReuseIdentifier: "CellMedia")
        self.setupUI()
        
        addRightBarButton(title: "Edit")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(self.toolbarEditMode)
        
    }
    override func setupUI() {
        super.setupUI()
        
        print(AppFolder.Documents.baseURL.path)

        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = .init(top: 1, left: 2, bottom: 2, right: 2)
        layout.headerInset = .init(top: 5, left: 0, bottom: 0, right: 0)
        layout.footerInset = .init(top: 0, left: 0, bottom: 5, right: 0)
        layout.headerHeight = 8
        layout.footerHeight = 8
        layout.minimumColumnSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.columnCount = 3
        
        clcMediaDetail.collectionViewLayout = layout
        clcMediaDetail.backgroundColor = .RGB(245,246,247)
        self.view.backgroundColor = clcMediaDetail.backgroundColor
        clcMediaDetail.delegate = self
        clcMediaDetail.dataSource = self
        
        
        view.addSubview(clcMediaDetail)
        clcMediaDetail.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
        }
        
        /*
        let action = UIBarButtonItem.init(barButtonSystemItem: .action, handler: {[weak self] in
            let msg = "Actions"
            let alert = UIAlertController.actionSheetController(withTitle: msg)
            alert.addButton(withTitle: "Export") {[weak self] (action) in
                guard let self = self else { return }
                self.actionShareTapped()
            }
            alert.addButton(withTitle: "Move") { [weak self] (action) in
                guard let self = self else { return }
                self.actionMoveTapped()
            }
            alert.addCancelButton()
            alert.show()

        })
        let space = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, handler: {
            
        })
        let delete = UIBarButtonItem.init(barButtonSystemItem: .trash, handler: {[weak self] in
            guard let self = self else { return }
            self.btnDeleteTapped()
        })
        */
        let barItemAction = UIBarButtonItem.init(barButtonSystemItem: .action) {[weak self] in
            guard let self = self else { return }
            self.actionShareTapped()
        }
        let barItem1 = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace) {

        }
        let moveItem = UIBarButtonItem.init(title: "Move", style: .plain) {[weak self] in
            guard let self = self else { return }
            self.actionMoveTapped()
        }
        let barItem2 = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace) {

        }

        let barItemDelete = UIBarButtonItem.init(barButtonSystemItem: .trash) {[weak self] in
            guard let self = self else { return }
            guard self.arrSelMedia.count > 0 else {
                return
            }
            
            var msg = "Are you sure you want to delete \(self.arrSelMedia.count) item" + ((self.arrSelMedia.count > 1 ) ? "s?" : "?")
            if self.folderLocal.folder_typeEnum == FolderType.trash{
                msg = "Are you sure you want to permanently delete \(self.arrSelMedia.count) item" + ((self.arrSelMedia.count > 1 ) ? "s?" : "?")
            }
            let alert = UIAlertController.actionSheetController(withTitle: msg)
            alert.addCancelButton()
            alert.addDestructiveButton(withTitle: "Delete") {[weak self] (action) in
                guard let self = self else { return }
                self.actionDeleteTapped()
            }
            alert.show()
        }
        barItemDelete.tintColor = UIColor.red
        toolbarEditMode.items = [barItemAction,barItem1,moveItem,barItem2,barItemDelete]

        
        toolbarEditMode.backgroundColor = .white
//        toolbarEditMode.items = [action,space,delete]

        self.view.addSubview(toolbarEditMode)
        toolbarEditMode.snp.makeConstraints { (m) in
            m.width.equalToSuperview()
            m.centerX.equalToSuperview()
//            m.height.equalTo(self.tabBarController!.tabBar.snp.height)
//            m.bottom.equalTo(self.tabBarController!.tabBar.snp.bottom)
            m.bottom.equalToSuperview()
            m.height.equalTo(49)
            
        }
        toolbarEditMode.isHidden = true
        toolbarEditMode.isTranslucent = false
        self.insertAddButtonUnder(title: "Add Media", viewTop: clcMediaDetail)
//        self.insertDeleteButtonUnder(title: "Delete", viewTop: collectionView)
    }
    
    override func updateUI()  {
        super.setupUI()
        

        self.arrMedia = Array(folderLocal.arrMediaLocals.sorted(byKeyPath: "date_created", ascending: false))
        self.setEditinModeTo(on: false)

        self.clcMediaDetail.reloadData()

        
    }
    
    override func actionRightBarTapped(sender: UIBarButtonItem) {
        self.setEditinModeTo(on: !isEditOn)
        self.clcMediaDetail.reloadData()
        

    }
    
    func setEditinModeTo(on:Bool)  {
        if on == true {
            addRightBarButton(title: "Cancel")
            isEditOn = true
            btnDelete?.isHidden = false
            btnAdd?.isHidden = true
            clcMediaDetail.allowsMultipleSelection = true
            self.constHeightBtnAdd?.update(offset: 0)
            toolbarEditMode.isHidden = false
            self.tabBarController?.tabBar.isHidden = true
            toolbarEditMode.superview?.bringSubviewToFront(toolbarEditMode)
            toolbarEditMode.snp.remakeConstraints { (m) in
                m.width.equalToSuperview()
                m.centerX.equalToSuperview()
                m.bottom.equalTo(self.view.snp.bottomMargin)
                m.height.equalTo(49)
            }

        }else{
            addRightBarButton(title: "Edit")
            isEditOn = false
            btnDelete?.isHidden = true
            btnAdd?.isHidden = false
            clcMediaDetail.allowsMultipleSelection = false
            self.constHeightBtnAdd?.update(offset: 50)

            toolbarEditMode.isHidden = true
            self.tabBarController?.tabBar.isHidden = false
        }
        if self.folderLocal.folder_typeEnum == FolderType.trash{
            btnAdd?.isHidden = true
        }
        if self.arrMedia.count == 0 {
            self.navigationItem.rightBarButtonItem = nil
        }
        arrSelMedia = []
    }
    override func viewWillDisappear(_ animated: Bool) {
        if (self.isMovingFromParent == true)
        {
            setEditinModeTo(on: false);
        }
        else
        {

        }
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
     override func btnAddTapped()  {
        let actionController = YoutubeActionController()
        

//        actionController.addAction(Action(ActionData(title: "Camera", image: UIImage(named: "capature_new")!), style: .default, handler: {[weak self] action in
//            guard let self = self else { return }
//            self.btnCameraTapped()
//
//        }))
        actionController.addAction(Action(ActionData(title: "Media Library", image: UIImage(named: "media_library")!), style: .default, handler: {[weak self] action in
            guard let self = self else { return }
            self.btnMediaLibraryTapped()

        }))

        actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "close")!), style: .cancel, handler: nil))
        
        
        present(actionController, animated: true, completion: nil)

    }
    
    func actionShareTapped()  {
        guard self.arrSelMedia.count > 0 else {
            self.view.makeToast("Please select media first")
            return
        }

        let contentToShare:[URL] = self.arrSelMedia.map({ appDataFolder.appendingPathComponent($0.media_name!) })

        let imageToShare = contentToShare
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

        // present the view controller
        self.present(activityViewController, animated: true, completion: {
            
        })


    }
    func actionMoveTapped()  {
        guard self.arrSelMedia.count > 0 else {
            self.view.makeToast("Please select media first")
            return
        }

        let selectFolder = SelectFolder()
        selectFolder.selectedFolder = folderLocal
        let navi = UINavigationController.init(rootViewController: selectFolder)

        selectFolder.blockDidSelectFolder = { [weak self] folder in
            guard let self = self else { return }
            if let folder = folder {
                for mediaLocal in self.arrSelMedia {
                    let realm = try! Realm()
                    try! realm.write {
                        mediaLocal.folder_local = folder
                        realm.add(mediaLocal)
                    }
                }
                self.setEditinModeTo(on: false)
                self.updateUI()
            }
            navi.dismiss(animated: true, completion: nil)
        }
        self.present(navi, animated: true, completion: nil)
        //self.navigationController?.pushViewController(selectFolder, animated: true)

    }
    func actionDeleteTapped() {
        /*
        guard self.arrSelMedia.count > 0 else {
            return
        }
        let block = {[weak self] in
            guard let self = self else { return }
            if self.folderLocal.folder_typeEnum == FolderType.trash {
                self.folderLocal.deleteMediaPermanently(arrMedia: self.arrSelMedia)
            }else{
                self.folderLocal.moveToTrashMedia(arrMedia: self.arrSelMedia)
            }
            self.setEditinModeTo(on: false)
            self.updateUI()
        }
        
        var msg = "Are you sure you want to delete \(arrSelMedia.count) item" + ((arrSelMedia.count > 1 ) ? "s?" : "?")
        if self.folderLocal.folder_typeEnum == FolderType.trash{
            msg = "Are you sure you want to permanently delete \(arrSelMedia.count) item" + ((arrSelMedia.count > 1 ) ? "s?" : "?")
        }

        let alert = UIAlertController.actionSheetController(withTitle: msg)
        alert.addCancelButton()
        alert.addDestructiveButton(withTitle: "Delete") { (action) in
            block()
        }
        alert.show()
        */
        if self.folderLocal.folder_typeEnum == FolderType.trash {
            self.folderLocal.deleteMediaPermanently(arrMedia: self.arrSelMedia)
            self.navigationController?.view.makeToast("Deleted")
        }else{
            self.folderLocal.moveToTrashMedia(arrMedia: self.arrSelMedia)
            self.navigationController?.view.makeToast("Moved to Trash")
        }
        self.updateUI()


    }
    
    //MARK:- --------Action---------------------------------------------------------

    func btnCameraTapped()  {
        
    }
    
    func btnMediaLibraryTapped()  {
        let pickerController = DKImagePickerController()
        pickerController.sourceType = .both
        pickerController.allowSwipeToSelect = true
        pickerController.modalPresentationStyle = .fullScreen
        pickerController.showsCancelButton = true
        pickerController.didSelectAssets = { (assets: [DKAsset]) in
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                SVProgressHUD.show(withStatus: "Importing...")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.importAssetMediaInMainFolder(assets: assets)
                }
            }
        }

        self.present(pickerController, animated: true, completion: nil)
    }
    
    func importAssetMediaInMainFolder(assets:[DKAsset])  {
        
        if ISalesManager.shared.isSubscribed() == false{
            if  self.folderLocal.arrMediaLocals.count + assets.count > 2 {
                SVProgressHUD.dismiss()
                SalesVC.PresentSubscriptionPage(self)
                return
            }
        }

        var task:Task<MediaLocal?> = Task.init(nil)
        for asset in assets {
            
            task = task.continueWithTask(continuation: { (task) in
                self.updateUI()
                return SVSharedClass.shared.importAssetMedia(asset: asset, in: self.folderLocal)
            })
            
        }
        
        task.continueWith { (task)  in
            SVProgressHUD.dismiss()
            self.updateUI()
            let allAssets = assets.compactMap({$0.originalAsset})
            SVSharedClass.shared.deleteAfterImportIfEnabled(assets: allAssets)
            
            self.doAfterDelay(inMain: 1) {
                SVSharedClass.shared.appRequestReview()
            }


        }
        

        /*
        for asset in assets {
            
            if asset.type == .photo {

                let file_name = randomString(length: 10) + ".jpeg"
                let thumb_file_name = "thumb_" + file_name

                let full_path = AppFolder.Documents.url.appendingPathComponent(file_name)
                let thumb_full_path = AppFolder.Documents.url.appendingPathComponent(thumb_file_name)
                
                let options = PHImageRequestOptions()
                options.isSynchronous = true
                
                asset.fetchOriginalImage(options: options) { (image, info) in
                    let isDegraded = (info?[PHImageResultIsDegradedKey] as? Bool) ?? false
                    if isDegraded {
                       return
                    }
                    if let image = image,let dataImage = image.jpegData(compressionQuality: 1.0){
                        if let dataThumb = Toucan.init(image: image).resize(.init(width: 200, height: 200), fitMode: .clip).image?.jpegData(compressionQuality: 1.0){

                            if FileManager.write(data: dataThumb, absolutePath: thumb_full_path.path) == true{
                                if FileManager.write(data: dataImage, absolutePath: full_path.path) == true{
                                    //both thumb and original write successfully
                                    //save to db
                                    DispatchQueue.main.async {
                                        let realm = try! Realm()
                                        let media = MediaLocal()
                                        media.media_type = 0
                                        media.thumb_image_name = thumb_file_name
                                        media.media_name = file_name
                                        media.folder_local = self.folderLocal
                                        try! realm.write {
                                            realm.add(media)
                                        }
                                        self.updateUI()
                                                                                
                                    }

                                }
                            }
                        }
                    }
                }
            }else if asset.type == .video{

                let name = randomString(length: 10)
                let file_name = name + ".mov"
                let thumb_file_name = "thumb_" + name + ".jpeg"

                let full_path = AppFolder.Documents.url.appendingPathComponent(file_name)
                let thumb_full_path = AppFolder.Documents.url.appendingPathComponent(thumb_file_name)
                
                let options = PHImageRequestOptions()
                options.isSynchronous = true
                           
                asset.fetchImage(with: CGSize(width: 200, height: 200)) { (image, info) in
                    let isDegraded = (info?[PHImageResultIsDegradedKey] as? Bool) ?? false
                    if isDegraded {
                       return
                    }
                    if let image = image,let dataThumb = image.jpegData(compressionQuality: 1.0){
                        if FileManager.write(data: dataThumb, absolutePath: thumb_full_path.path) == true{
                            
                            asset.fetchAVAsset { (avasset, nil) in
                                if let urlAsset = avasset as? AVURLAsset {
                                    let localVideoUrl = urlAsset.url
                                    
                                    if let videoData = try? Data.init(contentsOf: localVideoUrl){
                                        if FileManager.write(data: videoData, absolutePath: full_path.path){
                                            
                                            DispatchQueue.main.async {
                                                let realm = try! Realm()
                                                let media = MediaLocal()
                                                media.media_type = 1
                                                media.thumb_image_name = thumb_file_name
                                                media.media_name = file_name
                                                media.folder_local = self.folderLocal
                                                try! realm.write {
                                                    realm.add(media)
                                                }
                                                
                                                self.updateUI()

                                            }

                                        }
                                    }

                                }
                            }
                        }
                        
                    }
                    
                }
                
            }

        }

        let allAssets = assets.compactMap({$0.originalAsset})
        SVSharedClass.shared.deleteAfterImportIfEnabled(assets: allAssets)
        */
    }
    
//    func deleteMedia(mediaLocal:MediaLocal)  {
//        let block = {
//            mediaLocal.deleteMedia()
//            self.updateUI()
//        }
//
//
//        let alert = UIAlertController.actionSheetController(withTitle: "Are you sure you want to delete this media?")
//        alert.addCancelButton()
//        alert.addDestructiveButton(withTitle: "Delete") { (action) in
//            block()
//        }
//        alert.show()
//
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MR_MediaDetailViewVC:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMedia.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellMedia", for: indexPath) as! CellMedia
        cell.media = arrMedia[indexPath.row]
//        cell.btnDelete.onTap {[weak self] in
//            guard let self = self else { return }
//            self.deleteMedia(mediaLocal: self.arrMedia[indexPath.row])
//        }
        if self.isEditOn {
            cell.isSelectedItem = arrSelMedia.contains(cell.media)
        }else{
            cell.isSelectedItem = false
        }
        
        //test ss
//        if indexPath.row == 5 || indexPath.row == 8 {
//            cell.imgType.isHidden = false
//        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if self.isEditOn == true {
            let media = arrMedia[indexPath.row]
            if arrSelMedia.contains(media) {
                (collectionView.cellForItem(at: indexPath) as? CellMedia)?.isSelectedItem = false
                arrSelMedia.remove(object: media)
            }else{
                arrSelMedia.append(media)
                (collectionView.cellForItem(at: indexPath) as? CellMedia)?.isSelectedItem = true
            }
            //            self.collectionView.reloadData()
            return
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.isEditOn == true {
            let media = arrMedia[indexPath.row]
            if arrSelMedia.contains(media) {
                (collectionView.cellForItem(at: indexPath) as? CellMedia)?.isSelectedItem = false
                arrSelMedia.remove(object: media)
            }else{
                arrSelMedia.append(media)
                (collectionView.cellForItem(at: indexPath) as? CellMedia)?.isSelectedItem = true
            }
//            self.collectionView.reloadData()
            return
        }
        
        if ISalesManager.shared.isSubscribed() == false{
            SalesVC.PresentSubscriptionPage(self)
            return
        }


        let browser = MR_BrowseMediaVC()
        browser.folderLocal = folderLocal
        browser.arrMedia = arrMedia
        browser.hidesBottomBarWhenPushed = true
        browser.initialIndex = indexPath.row
        self.navigationController?.pushViewController(browser, animated: true)
        

        
        
    }
    func collectionView(_ collectionView: UICollectionView, shouldBeginMultipleSelectionInteractionAt indexPath: IndexPath) -> Bool {
        return true
    }
    func collectionView(_ collectionView: UICollectionView, didBeginMultipleSelectionInteractionAt indexPath: IndexPath) {
    }
    func collectionViewDidEndMultipleSelectionInteraction(_ collectionView: UICollectionView) {
    }
    
}

extension MR_MediaDetailViewVC:CollectionViewWaterfallLayoutDelegate{
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = self.view.window!.frame.width/CGFloat(3)
        return .init(width: width, height: width)
    }
}

//extension FolderDetailVC:MediaBrowserDelegate{
//    func numberOfMedia(in mediaBrowser: MediaBrowser) -> Int {
//                return self.arrMedia.count
//
//    }
//
//    /*
//    func actionButtonPressed(at photoIndex: Int, in mediaBrowser: MediaBrowser, sender: Any? = nil) {
//        let action = UIAlertController.actionSheetController(withTitle: "Actions")
//        action.addButton(withTitle: "Export") { (actoin) in
//
//        }
//        action.addButton(withTitle: "Share") { (actoin) in
//
//        }
//        action.addButton(withTitle: "Move") { (actoin) in
//
//        }
//        action.addButton(withTitle: "Delete") { (action) in
//           let mediaLocal = self.arrMedia[photoIndex]
//            self.arrMedia.remove(object: mediaLocal)
//            mediaLocal.deleteMedia()
//            if photoIndex+1 < self.arrMedia.count{
//                mediaBrowser.setCurrentIndex(at: photoIndex+1)
//                mediaBrowser.reloadData()
//            }else if photoIndex-1 > -1{
//                mediaBrowser.setCurrentIndex(at: photoIndex-1)
//                mediaBrowser.reloadData()
//            }else{
//                self.navigationController?.popViewController(animated: true)
//            }
//
//        }
//        action.addCancelButton()
//        action.show()
//    }
//     */
//
//    func media(for mediaBrowser: MediaBrowser, at index: Int) -> Media {
//        let mediaLocal = self.arrMedia[index]
//
//        if index < self.arrMedia.count {
//          if mediaLocal.media_type == 0{
//              return Media.init(url: appDataFolder.appendingPathComponent(mediaLocal.media_name!))
//          }else if mediaLocal.media_type == 1{
//              return Media.init(videoURL: appDataFolder.appendingPathComponent(mediaLocal.media_name!), previewImageURL: appDataFolder.appendingPathComponent(mediaLocal.thumb_image_name!))
//
//          }
//        }
//        return Media.init()
//
//    }
//
//
//}

class CellMedia: UICollectionViewCell {
    let imgIcon:UIImageView = UIImageView()
    let imgType:UIImageView = UIImageView()
    let btnDelete:UIButton = UIButton()
    var isSelectedItem:Bool = false{
        didSet{
            self.updateSelected()
        }
    }
    
    var media:MediaLocal!{
        didSet{
            self.updateUI()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupUI(){
        self.contentView.addSubview(self.imgIcon)

        imgIcon.contentMode = .scaleAspectFill
        imgIcon.clipsToBounds = true
        
        self.imgIcon.snp.makeConstraints { (m) in
            m.edges.equalToSuperview()
        }
        
        self.contentView.addSubview(imgType)
        imgType.snp.makeConstraints { (m) in
            m.width.equalTo(30)
            m.height.equalTo(25)
            m.bottom.equalTo(-5)
            m.left.equalTo(5)
        }
        imgType.contentMode = .scaleAspectFit
        imgType.isHidden = true
        imgType.image = UIImage.init(named: "video_media")

        imgType.layer.shadowColor = UIColor.lightGray.cgColor
        imgType.layer.shadowOffset = CGSize(width: 1, height: 2.0)
        imgType.layer.shadowRadius = 3
        imgType.layer.shadowOpacity = 01
        
        
        self.contentView.addSubview(btnDelete)
        btnDelete.snp.makeConstraints { (m) in
            m.width.equalTo(27)
            m.height.equalTo(27)
            m.right.equalTo(-5)
            m.top.equalTo(5)
        }
        
        btnDelete.setImage(UIImage.init(named: "ic_checkbox_on"), for: .normal)
//        btnDelete.setImage(UIImage.init(named: "trash_icon"), for: .selected)
        

        
        self.contentView.backgroundColor = .white
//        self.contentView.layer.cornerRadius =  5
        self.contentView.clipsToBounds = true
        
//        self.layer.shadowColor = UIColor.lightGray.cgColor
//        self.layer.shadowOffset = CGSize(width: 1, height: 2.0)
//        self.layer.shadowRadius = 4
//        self.layer.shadowOpacity = 0.5
//        self.layer.masksToBounds = false
//        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
    }
    
    func updateUI()  {
        let fullPath = appDataFolder.appendingPathComponent(media.thumb_image_name!)
        self.imgIcon.image = UIImage.init(contentsOfFile: fullPath.path)
        
        if media.media_type == 1 {
            imgType.isHidden = false
        }else{
            imgType.isHidden = true
        }
    }
    func updateSelected()  {
        //todo
        self.isSelected = isSelectedItem
        if isSelectedItem == true {
            btnDelete.isHidden = false
            self.alpha = 0.6
        }else{
            btnDelete.isHidden = true
            self.alpha = 1.0
        }
    }
}


extension UIImage{
    func imageWith(newSize: CGSize) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: newSize)
        let image = renderer.image { _ in
            self.draw(in: CGRect.init(origin: CGPoint.zero, size: newSize))
        }

        return image.withRenderingMode(self.renderingMode)
    }

}
