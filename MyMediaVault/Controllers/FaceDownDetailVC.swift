//
//  FaceDownDetailVC.swift
//  SecretVault
//
//  Created by Macbook on 12/08/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit

//class FaceDownOption: NSObject {
//    var name:String = ""
//    var icon:UIImage = UIImage()
//    var keyId:String = ""
//
//}

enum FaceDownOption:String {
    case disable
    case safari
    case music
    case mail
    case messages
    
    case contacts //contacts://
    case map //map://
    case calendar  //calshow://
    case whatsapp // whatsapp://
    case instagram //instagram://
    case facebook //fb://

    
    var display_name:String{
        return self.rawValue.capitalized
    }
    

}
//struct KeyFaceDownOption {
//    static let disable:String = "fd_disable"
//    static let safari:String = "fd_safari"
//    static let music:String = "fd_music"
//    static let mail:String = "fd_mail"
//    static let message:String = "fd_message"
//
//}
//

class FaceDownDetailVC: SVBaseController {

    let tblSettings:UITableView = UITableView()
    var arrFacedownOption:[FaceDownOption] = []
    let lblNote = UILabel()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Facedown"
        
        
        setupUI()
        updateUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
    override func setupUI() {
        super.setupUI()
        
        //For iOS 9 and Above
        if #available(iOS 9, *) {
//            self.tblSettings.cellLayoutMarginsFollowReadableWidth = false
        }
        

        
        //---menu
        let array:[FaceDownOption] = [FaceDownOption.disable,FaceDownOption.safari,FaceDownOption.music,FaceDownOption.mail,FaceDownOption.messages,FaceDownOption.map,FaceDownOption.calendar,FaceDownOption.whatsapp,FaceDownOption.instagram,FaceDownOption.facebook]
/*
        array.append()
        var menu:FaceDownOption = FaceDownOption()
        menu.name = "Disable"
        menu.keyId = "fd_disable"
        array.append(menu)
        
        menu = FaceDownOption()
        menu.name = "Safari"
        menu.keyId = "fd_safari"
        array.append(menu)
        
        menu = FaceDownOption()
        menu.name = "Music"
        menu.keyId = "fd_music"
        array.append(menu)
        
        menu = FaceDownOption()
        menu.name = "Mail"
        menu.keyId = "fd_mail"
        array.append(menu)
        
        menu = FaceDownOption()
        menu.name = "Messages"
        menu.keyId = "fd_message"
        array.append(menu)
        */
        
        self.arrFacedownOption = array

        view.addSubview(lblNote)
        lblNote.snp.makeConstraints { (m) in
            m.topMargin.equalTo(self.view.snp.topMargin).offset(10)
            m.left.right.equalToSuperview().inset(10)
            m.height.equalTo(70)
        }
        
        view.addSubview(tblSettings)
        tblSettings.snp.makeConstraints { (make) in
            make.top.equalTo(lblNote.snp.bottom)
            make.left.bottom.right.equalToSuperview()
        }
        
        lblNote.backgroundColor = .white
        lblNote.textColor = .darkGray
        lblNote.textAlignment = .center
        lblNote.font = UIFont.systemFont(ofSize: 14)
        lblNote.numberOfLines = 0
        lblNote.text = "Our app will automatically switch to selected\napplication, after turning the device facedown."
        
        tblSettings.backgroundColor = .white

        tblSettings.delegate = self
        tblSettings.dataSource = self
        tblSettings.tableFooterView = UIView()
        
    }
    
    
    override func updateUI()  {
     
        self.tblSettings.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FaceDownDetailVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFacedownOption.count
    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 50
//    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let lblTitle = UILabel()
        lblTitle.font = UIFont.systemFont(ofSize: 13)
        lblTitle.textColor = .darkGray
        lblTitle.textAlignment = .center
        lblTitle.backgroundColor = .white
        lblTitle.numberOfLines = 0
        lblTitle.snp.makeConstraints { (m) in
            m.width.equalTo(AppDel.window!.frame.width - 40)
        }
        lblTitle.text = "*This apps may require permission for very first time to open. Please make sure that this apps are installed on your device as well."
        
        let vfooter = UIView()
        vfooter.addSubview(lblTitle)
        lblTitle.snp.makeConstraints { (m) in
            m.center.equalToSuperview()
            m.top.equalToSuperview()
            m.width.equalToSuperview().offset(-30)
        }
        vfooter.backgroundColor = .white
        return vfooter

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell")
        let menu = arrFacedownOption[indexPath.row]

        cell.textLabel?.text = menu.display_name
        if menu == FaceDownOption.whatsapp ||  menu == FaceDownOption.instagram || menu == FaceDownOption.facebook{
            cell.textLabel?.text = menu.display_name + "*"
        }
        
        if SVSharedClass.shared.facedown_option.rawValue == menu.rawValue {
            cell.accessoryType = .checkmark
            cell.textLabel?.textColor = .blue
        }else{
            cell.accessoryType = .none
            cell.textLabel?.textColor = .black
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menu = arrFacedownOption[indexPath.row]
        tblSettings.deselectRow(at: indexPath, animated: false)
        
        let newOption = FaceDownOption.init(rawValue: menu.rawValue) ?? FaceDownOption.disable
        var showTestingNote:Bool = false

        if SVSharedClass.shared.facedown_option == .disable,newOption != .disable  {
            showTestingNote = true
        }
        if showTestingNote == true{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.view.makeToast("Test this by turning device to facedown - screen on surface side")
            }
        }
        SVSharedClass.shared.facedown_option = newOption

        self.tblSettings.reloadData()
        
        
    }

}
