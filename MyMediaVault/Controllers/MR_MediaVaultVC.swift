//
//  PhotosVC.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit

import UIKit
import RealmSwift
import Photos
import DKImagePickerController
import BoltsSwift

class MR_MediaVaultVC: SVBaseController {

    var arrFolder:[FolderLocal] = []
    let clcMediaList:UICollectionView = UICollectionView.init(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var showTipOnViewAppear:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Media"
        
        print(AppFolder.Documents.url.path)
        SVSharedClass.shared.createDefaultFolderIfNeeded()

        self.setupUI()
                
        self.showTip()
        
        if SVSharedClass.shared.launchCountValue > 1 {
            self.doAfterDelay(inMain: 1) {
                SVSharedClass.shared.appRequestReview()
            }
        }
        
        
    }
    
    func showTip()  {
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUI()
    }
    override func actionRightBarTapped(sender: UIBarButtonItem) {


    }

    override func setupUI() {
        clcMediaList.register(CellFolder.self, forCellWithReuseIdentifier: "CellFolder")

        let layoutCollectionview = CollectionViewWaterfallLayout()
        layoutCollectionview.sectionInset = .init(top: 0, left: 10, bottom: 0, right: 10)
        layoutCollectionview.headerInset = .init(top: 10, left: 0, bottom: 0, right: 0)
        layoutCollectionview.footerInset = .init(top: 0, left: 0, bottom: 10, right: 0)
        layoutCollectionview.headerHeight = 8
        layoutCollectionview.footerHeight = 8
        layoutCollectionview.minimumColumnSpacing = 15
        layoutCollectionview.minimumInteritemSpacing = 12
        layoutCollectionview.columnCount = 1
        
        
        clcMediaList.collectionViewLayout = layoutCollectionview
        clcMediaList.backgroundColor = .RGB(245,246,247)
        self.view.backgroundColor = clcMediaList.backgroundColor
        clcMediaList.delegate = self
        clcMediaList.dataSource = self
        
//        headerNote.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath

        
//        view.addSubview(headerNote)
//        headerNote.snp.makeConstraints { (m) in
//            m.top.equalTo(20)
//            m.left.right.equalToSuperview().inset(10)
////            m.height.equalTo(100)
//
//        }
        
        view.addSubview(clcMediaList)
        clcMediaList.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview().offset(10)
        }

        self.insertAddButtonUnder(title: "Add Media", viewTop: clcMediaList)

    }
    override func updateUI()  {
        
        let realm = try! Realm()
        let result = realm.objects(FolderLocal.self).filter("is_fake == \(SVSharedClass.shared.isFakePasscode)")
        self.arrFolder = Array(result).sorted(by: { (obj1, obj2) -> Bool in
            return obj1.date_created! < obj2.date_created!
        })
        self.arrFolder = self.arrFolder.filter({ (folder) -> Bool in
            if folder.folder_typeEnum == .trash,folder.arrMediaLocals.count == 0{
                return false
            }
            return true
        })
        
        //move trash to last position
        if let trashFolder =  self.arrFolder.first(where: {$0.folder_typeEnum == .trash}){
            self.arrFolder.remove(object: trashFolder)
            self.arrFolder.append(trashFolder)
        }

        
        
        self.clcMediaList.reloadData()
    }

    
    

    @objc func actionHeaderTapped()  {


    }
    @objc func actionCloseHeaderTapped()  {

    }
    
    override func btnAddTapped()  {
        
        let actionController = YoutubeActionController()
        
        actionController.addAction(Action(ActionData(title: "New Folder", image: UIImage(named: "add_new")!), style: .default, handler: {[weak self] action  in
            guard let self = self else { return }
            self.btnNewAlbumTapped()
        }))
//        actionController.addAction(Action(ActionData(title: "Camera", image: UIImage(named: "capature_new")!), style: .default, handler: {[weak self] action in
//            guard let self = self else { return }
//            self.btnCameraTapped()
//
//        }))
        actionController.addAction(Action(ActionData(title: "Media Library", image: UIImage(named: "media_library")!), style: .default, handler: {[weak self] action in
            guard let self = self else { return }
            self.btnMediaLibraryTapped()

        }))
        

        actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "close")!), style: .cancel, handler: nil))
        
        
        present(actionController, animated: true, completion: nil)

    }
    
    //MARK:- --------Action---------------------------------------------------------

    func btnNewAlbumTapped()  {
        let alert = UIAlertController.alertViewController(withTitle: "Create New Folder", message: "Enter name of\nnew folder")
        alert.addTextField { (txt) in
            txt.placeholder = "Enter name"
            txt.font = UIFont.systemFont(ofSize: 17)
            txt.autocorrectionType = .no
            txt.autocapitalizationType = .words
            txt.tag = 100
            txt.delegate = self
            txt.snp.makeConstraints { (make) in
                make.height.equalTo(27)
            }
        }
        alert.addCancelButton()
        alert.addButton(withTitle: "OK") { (action) in
            if let name = alert.textFields?.first!.text,name.isEmpty == false{
                self.createFolderNamed(name: name)
            }else{
                UIAlertController.showOKAlertViewController(withTitle: "Alert!", message: "Invalid Folder Name")
            }
        }
        alert.show()
    }
    
    func btnMediaLibraryTapped()  {
        let pickerController = DKImagePickerController()
        pickerController.sourceType = .both
        pickerController.allowSwipeToSelect = true
        pickerController.modalPresentationStyle = .fullScreen
        pickerController.showsCancelButton = true

        pickerController.didSelectAssets = { (assets: [DKAsset]) in
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                SVProgressHUD.show(withStatus: "Importing...")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.importAssetMediaInMainFolder(assets: assets)
                }
            }
        }

        self.present(pickerController, animated: true, completion: nil)
    }
    
    func importAssetMediaInMainFolder(assets:[DKAsset])  {
        
        guard let folder = self.arrFolder.first else {
            SVProgressHUD.dismiss()
            return
        }

        if ISalesManager.shared.isSubscribed() == false{
            if folder.arrMediaLocals.count + assets.count > 2 {
                SVProgressHUD.dismiss()
                SalesVC.PresentSubscriptionPage(self)
                return
            }
        }

        var task:Task<MediaLocal?> = Task.init(nil)
        for asset in assets {
            task = task.continueWithTask(continuation: { (task) in
                return SVSharedClass.shared.importAssetMedia(asset: asset, in: folder)
            })
        }
        
        task.continueWith { (task)  in
            SVProgressHUD.dismiss()
            self.updateUI()
            let allAssets = assets.compactMap({$0.originalAsset})
            SVSharedClass.shared.deleteAfterImportIfEnabled(assets: allAssets)
            
            self.doAfterDelay(inMain: 1) {
                SVSharedClass.shared.appRequestReview()
            }

        }
        

    }

    
    //MARK:- --------Task---------------------------------------------------------
    func createFolderNamed(name:String)  {
        let folder = FolderLocal()
        
        let realm = try! Realm()
        try! realm.write {
            folder.display_name = name
            folder.can_delete = true
            realm.add(folder)
        }
        
        self.updateUI()

    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}
extension MR_MediaVaultVC:GCDWebUploaderDelegate{
    
    func webUploader(_ uploader: GCDWebUploader, didDownloadFileAtPath path: String) {
        print("didDownloadFileAtPath : \(path)")
    }
    func webUploader(_ uploader: GCDWebUploader, didUploadFileAtPath path: String) {
        print("didUploadFileAtPath \(path)")
        
        guard let folder = SVSharedClass.shared.folder_received else {
            return
        }
        


        if let fileName = path.components(separatedBy: "/").last?.lowercased(){
            if fileName.contains(".jpeg") || fileName.contains(".jpg") || fileName.contains(".png") {
                
                let file_name = randomString(length: 10) + ".jpeg"
                let thumb_file_name = "thumb_" + file_name
                
                let full_path = appDataFolder.appendingPathComponent(file_name)
                let thumb_full_path = appDataFolder.appendingPathComponent(thumb_file_name)

                if let image = UIImage.init(contentsOfFile: path){
                    if let dataImage = image.jpegData(compressionQuality: 1.0){
                        if let dataThumb = Toucan.init(image: image).resize(.init(width: 400, height: 400), fitMode: .clip).image?.jpegData(compressionQuality: 1.0){
                            
                            if FileManager.write(data: dataThumb, absolutePath: thumb_full_path.path) == true{
                                if FileManager.write(data: dataImage, absolutePath: full_path.path) == true{
                                    //both thumb and original write successfully
                                    //save to db
                                    DispatchQueue.main.async {
                                        let realm = try! Realm()
                                        let media = MediaLocal()
                                        media.media_type = 0
                                        media.thumb_image_name = thumb_file_name
                                        media.media_name = file_name
                                        media.folder_local = folder
                                        try! realm.write {
                                            realm.add(media)
                                        }
                                        self.navigationController?.view.makeToast("\(fileName) Received")

                                    }
                                }
                            }
                        }
                    }
                }

            }else if fileName.contains(".mov") || fileName.contains(".mp4") {
                let name = randomString(length: 10)
                let file_name = name + ".mov"
                let thumb_file_name = "thumb_" + name + ".jpeg"
                
                let full_path = appDataFolder.appendingPathComponent(file_name)
                let thumb_full_path = appDataFolder.appendingPathComponent(thumb_file_name)
                
                let asset = AVURLAsset.init(url: URL.init(fileURLWithPath: path))
                
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                if let cgImage = try? imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil){
                    let image = UIImage(cgImage: cgImage)
                    if let dataThumb = image.jpegData(compressionQuality: 1.0){
                        if FileManager.write(data: dataThumb, absolutePath: thumb_full_path.path) == true{
                            
                            let localVideoUrl = asset.url
                            
                            if FileManager.moveFile(sourceAbsolutePath: localVideoUrl.path, destinationAbsolutePath: full_path.path) == true{

                                DispatchQueue.main.async {
                                    let realm = try! Realm()
                                    let media = MediaLocal()
                                    media.media_type = 1
                                    media.thumb_image_name = thumb_file_name
                                    media.media_name = file_name
                                    media.folder_local = folder
                                    try! realm.write {
                                        realm.add(media)
                                    }
                                    
                                    self.navigationController?.view.makeToast("\(fileName) Received")
                                }

                            }

                        }
                    }


                }

            }
        }
        
        

    }
    func webUploader(_ uploader: GCDWebUploader, didMoveItemFromPath fromPath: String, toPath: String) {
        
    }
    func webUploader(_ uploader: GCDWebUploader, didDeleteItemAtPath path: String) {
        
    }
    func webUploader(_ uploader: GCDWebUploader, didCreateDirectoryAtPath path: String) {
        
    }

}

extension MR_MediaVaultVC:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFolder.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellFolder", for: indexPath) as! CellFolder
        cell.folder = arrFolder[indexPath.row]
//        cell.imgProfile.isHidden = true
        
        cell.btnMore.onTap {[weak self] in
            guard let self = self else { return }
            
            if cell.folder.folder_typeEnum == .trash{
                let alert = UIAlertController.actionSheetController(withTitle: "Option")
                alert.addButton(withTitle: "Empty Trash") { (action) in
                    self.actionEmptyTrashFolder(folder: cell.folder)
                }
                alert.addCancelButton()
                alert.show()

                return
            }
            
            let alert = UIAlertController.actionSheetController(withTitle: "Option")
            alert.addButton(withTitle: "Rename") { (action) in
                if cell.folder.folder_passcode != nil, cell.folder.folder_passcode!.isEmpty == false {
                    self.actionValidatePasscodeForFolder(for: cell.folder) { [weak self](success) in
                        guard let self = self else { return }
                        if success == true{
                            self.actionRenameFolder(for: cell.folder)
                        }
                    }
                }else{
                    self.actionRenameFolder(for: cell.folder)
                }
            }
            if cell.folder.folder_passcode != nil{
                alert.addButton(withTitle: "Remove Folder Passcode") { (action) in
                    self.actionRemovePasscodeFolder(for: cell.folder)
                }
            }else{
                alert.addButton(withTitle: "Set Folder Passcode") { (action) in
                    self.actionSetPasscodeFolder(for: cell.folder)
                }
            }
            if cell.folder.can_delete == true{
                alert.addDestructiveButton(withTitle: "Delete") { (action) in
                    
                    if cell.folder.folder_passcode != nil, cell.folder.folder_passcode!.isEmpty == false {
                        self.actionValidatePasscodeForFolder(for: cell.folder) { [weak self](success) in
                            guard let self = self else { return }
                            if success == true{
                                self.actionDeleteFolder(for: cell.folder)
                            }
                        }
                    }else{
                        self.actionDeleteFolder(for: cell.folder)
                    }
                }
            }
            alert.addCancelButton()
            alert.show()
            
        }
        return cell
    }
    
    func actionRenameFolder(for folder:FolderLocal)  {
        let alert = UIAlertController.alertViewController(withTitle: "Rename Folder")
        alert.addTextField { (txt) in
            txt.placeholder = "Enter name"
            txt.font = UIFont.systemFont(ofSize: 17)
            txt.autocorrectionType = .no
            txt.autocapitalizationType = .words
            txt.text = folder.display_name
            txt.delegate = self
            txt.tag = 100
            txt.snp.makeConstraints { (make) in
                make.height.equalTo(27)
            }
        }
        alert.addCancelButton()
        alert.addButton(withTitle: "OK") { (action) in
            if let name = alert.textFields?.first!.text,name.isEmpty == false{
                DispatchQueue.main.async {
                    let realm = try! Realm()
                    try! realm.write {
                        folder.display_name = name
                    }
                    self.updateUI()
                }
            }else{
                UIAlertController.showOKAlertViewController(withTitle: "Alert!", message: "Invalid Folder Name")
            }
        }
        
        alert.show()
        
    }
    func actionDeleteFolder(for folder:FolderLocal)  {
        let alert = UIAlertController.actionSheetController(withTitle: "Are you sure you want to delete folder?")
        alert.addCancelButton()
        
        alert.addDestructiveButton(withTitle: "Delete") { (action) in
            DispatchQueue.main.async {
                
                if SVSharedClass.shared.folder_received == folder{
                    SVSharedClass.shared.folder_received = nil
                }
                //-- delete media
                folder.moveToTrashMedia(arrMedia: Array(folder.arrMediaLocals))
//                Array(folder.arrMediaLocals).forEach({$0.deleteMedia()})
                
                //-- delete folder
                let realm = try! Realm()
                try! realm.write {
                    realm.delete(folder)
                }
                self.updateUI()
            }

        }
        
        alert.show()

    }
    func actionEmptyTrashFolder(folder:FolderLocal)  {
        let alert = UIAlertController.actionSheetController(withTitle: "Are you sure you want to empty trash folder? This action cannot be undone")
        alert.addCancelButton()
        alert.addDestructiveButton(withTitle: "Delete") {[weak self] (action) in
            guard let self = self else { return }
            folder.deleteMediaPermanently(arrMedia: Array(folder.arrMediaLocals))
            self.updateUI()

        }
        alert.show()

    }

    func actionSetPasscodeFolder(for folder:FolderLocal)  {
        self.showFolderPasscodeOption(title: "Lock Folder", strDescr: "Enter your passcode to lock this folder.", btnTitle: "OK") { (code) in
            DispatchQueue.main.async {
                let realm = try! Realm()
                try! realm.write {
                    folder.folder_passcode = code
                }
                self.updateUI()
                SVProgressHUD.showSuccess(withStatus: "Done")
            }
        }
    }
    func actionRemovePasscodeFolder(for folder:FolderLocal,title:String = "Remove Lock", strDescr:String = "Enter your passcode to remove the lock from this folder.", btnTitle:String = "OK")  {
        self.showFolderPasscodeOption(title: title, strDescr: strDescr, btnTitle: btnTitle) { (code) in
            DispatchQueue.main.async {
                if let existing = folder.folder_passcode,existing == code{
                    let realm = try! Realm()
                    try! realm.write {
                        folder.folder_passcode = nil
                    }
                    self.updateUI()
                    SVProgressHUD.showSuccess(withStatus: "Done")
                }else{
                    self.actionRemovePasscodeFolder(for: folder,strDescr: "That's not the correct passcode,\nPlease try again.")

                }
            }
        }
    }
    func actionValidatePasscodeForFolder(for folder:FolderLocal,title:String = "Locked Folder", strDescr:String = "To access locked folder, enter the folder password", btnTitle:String = "OK",completion:((Bool)->Void)?)  {
        self.showFolderPasscodeOption(title:title, strDescr: strDescr, btnTitle: btnTitle) { (code) in
            DispatchQueue.main.async {
                
                if let existing = folder.folder_passcode,existing == code{
                    completion?(true)
                }else{
                    self.actionValidatePasscodeForFolder(for: folder, strDescr: "That's not the correct passcode,\nPlease try again.", completion: completion)
                    //completion?(false)
                }
            }
        }
    }
    
    
    
    func showFolderPasscodeOption(title:String,strDescr:String?,btnTitle:String,completion:((String)->Void)?)  {
        let alert = UIAlertController.alertViewController(withTitle: title, message: strDescr)
        alert.addTextField { (txt) in
            txt.placeholder = "Passcode"
            txt.font = UIFont.systemFont(ofSize: 17)
            txt.autocorrectionType = .no
            txt.autocapitalizationType = .none
            txt.isSecureTextEntry = true
            txt.keyboardType = .numberPad
            txt.tag = 101
            txt.delegate = self
            txt.snp.makeConstraints { (make) in
                make.height.equalTo(27)
            }
        }
        alert.addButton(withTitle: "Cancel", action: nil)
        alert.addButton(withTitle: btnTitle) { (action) in
            if let code = alert.textFields?.first!.text,code.isEmpty == false{
                completion?(code)
            }
        }
        
        alert.show()

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let blockPush = {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.20) {
                let detail = MR_MediaDetailViewVC()
                detail.folderLocal = self.arrFolder[indexPath.row]
                self.navigationController?.pushViewController(detail, animated: true)
            }
        }
        let cell = collectionView.cellForItem(at: indexPath) as! CellFolder
        
        if cell.folder.folder_passcode != nil, cell.folder.folder_passcode!.isEmpty == false {
            self.actionValidatePasscodeForFolder(for: cell.folder) { (success) in
                if success == true{
                    blockPush()
                }
            }
        }else{
            blockPush()
        }
        
    }
}
extension MR_MediaVaultVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 100 {
            guard let preText = textField.text as NSString?,
                preText.replacingCharacters(in: range, with: string).count <= 25 else {
                return false
            }
        }
        if textField.tag == 101 {
            guard let preText = textField.text as NSString?,
                preText.replacingCharacters(in: range, with: string).count <= 6 else {
                return false
            }
        }
        return true
    }
}
extension MR_MediaVaultVC:CollectionViewWaterfallLayoutDelegate{
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width =  self.view.window!.frame.width/CGFloat(2)
        return .init(width: width, height: 45)
    }
}



class CellFolder: UICollectionViewCell {
    let imgBgBlue:UIImageView = UIImageView()
    var lblName: UILabel = UILabel()
    var btnMore: UIButton = UIButton()
    var imgLock = UIImageView()
    var folder:FolderLocal!{
        didSet{
            self.updateUI()
        }
    }
    let imgCenter = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupUI(){
        self.contentView.addSubview(self.imgBgBlue)
        self.contentView.addSubview(self.lblName)
        self.contentView.addSubview(self.btnMore)

        imgBgBlue.contentMode = .scaleAspectFill
        imgBgBlue.clipsToBounds = true
        
        lblName.textColor = .black
        lblName.textAlignment = .left
        lblName.numberOfLines = 0
        lblName.font = .boldSystemFont(ofSize: 16)
        
        self.btnMore.setImage(UIImage.init(named: "more_verti"), for: .normal)
        self.btnMore.imageView!.contentMode = .scaleAspectFit
        self.btnMore.contentEdgeInsets = .init(top: 0, left: 12, bottom: 0, right: 12)
        /*
        self.imgIcon.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-50)
        }
        self.lblName.snp.makeConstraints { (m) in
            m.left.equalToSuperview().offset(5)
            m.right.equalToSuperview().offset(-40)
            m.top.equalTo(self.imgIcon.snp.bottom)
            m.bottom.equalToSuperview()
        }
        self.btnMore.snp.makeConstraints { (m) in
            m.top.equalTo(imgIcon.snp.bottom)
            m.bottom.equalToSuperview()
            m.left.equalTo(lblName.snp.right)
            m.right.equalToSuperview()
        }
        */
        self.imgBgBlue.snp.makeConstraints { (make) in
            make.left.top.bottom.equalToSuperview()
            make.width.equalTo(self.imgBgBlue.snp.height)
        }
        self.lblName.snp.makeConstraints { (m) in
            m.left.equalTo(imgBgBlue.snp.right).offset(4)
            m.right.equalToSuperview().offset(-40)
            m.top.equalToSuperview()
            m.bottom.equalToSuperview()
        }
        self.btnMore.snp.makeConstraints { (m) in
            m.top.equalToSuperview()
            m.bottom.equalToSuperview()
            m.left.equalTo(lblName.snp.right)
            m.right.equalToSuperview()
        }
        
        self.btnMore.backgroundColor = .clear

        
        
        //---center icon
        imgCenter.image = UIImage.init(named: "folder_icon")
        imgBgBlue.addSubview(imgCenter)
        imgCenter.snp.makeConstraints { (m) in
            m.center.equalToSuperview()
            m.size.equalToSuperview().multipliedBy(0.65)
        }
        imgCenter.contentMode = .scaleAspectFit

        imgBgBlue.addSubview(imgLock)
        imgLock.snp.makeConstraints { (m) in
            m.size.equalTo(25)
            m.top.equalToSuperview().offset(5)
            m.left.equalToSuperview()
        }
        
        imgLock.image = UIImage.init(named: "lock")
        imgLock.isHidden = true


        self.contentView.backgroundColor = .clear
//        self.contentView.layer.cornerRadius =  5
//        self.contentView.clipsToBounds = true
        
        imgBgBlue.backgroundColor = .clear
        imgCenter.backgroundColor = .clear
        /*
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2.0)
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        */
    }
    
    func updateUI()  {
        self.lblName.text = folder.display_name ?? ""
        //self.imgBgBlue.backgroundColor = .white//UIColor.RGB(73,173,234)
        
        if folder.folder_passcode != nil, folder.folder_passcode!.count > 0 {
            self.imgLock.isHidden = false
        }else{
            self.imgLock.isHidden = true
        }
        
        if folder.folder_typeEnum == .trash {
            imgCenter.image = UIImage.init(named: "trash_folder_icon")
        }else{
            imgCenter.image = UIImage.init(named: "folder_icon")

        }

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.10, animations: {
            self.transform = CGAffineTransform.identity.scaledBy(x: 0.95, y: 0.95)
        }) { (success) in
            
//            UIView.animate(withDuration: 0.15, animations: {
//                self.transform = CGAffineTransform.identity
//            }) { (success) in
//
//            }
        }
        super.touchesBegan(touches, with: event)
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {

        UIView.animate(withDuration: 0.10,delay:0.10, animations: {
            self.transform = CGAffineTransform.identity
        }) { (success) in
            
        }
        super.touchesEnded(touches, with: event)

    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
                    UIView.animate(withDuration: 0.15,delay:0.15, animations: {
            self.transform = CGAffineTransform.identity
        }) { (success) in
            
        }
        super.touchesCancelled(touches, with: event)
    }
    
    
    
}

