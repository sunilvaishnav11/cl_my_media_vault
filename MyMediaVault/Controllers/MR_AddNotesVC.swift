//
//  AddNotesVC.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit
import RealmSwift

class MR_AddNotesVC: SVBaseController {

    var noteLocal:NoteLocal!
    let txtTitle = TextViewNote()
    let txtNote = TextViewNote()

    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.title = "Note"
        self.addLeftBarButton(title: "Cancel")
        self.addRightBarButton(title: "Done")
        self.setupUI()

        self.loadNoteInformation()
    }
    
    override func setupUI() {
        super.setupUI()
        
        view.addSubview(txtTitle)
        view.addSubview(txtNote)
        
        txtTitle.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin).offset(10)
            make.left.right.equalToSuperview().inset(20)
        }
        
        txtNote.snp.makeConstraints { (make) in
            make.top.equalTo(txtTitle.snp.bottom).offset(5)
            make.left.right.equalToSuperview().inset(20)
            make.bottom.lessThanOrEqualToSuperview().offset(-100)

            
        }
        
        txtTitle.placeholder = "Title"
        txtNote.placeholder = "Note"

        txtTitle.textAlignment = .left
        txtNote.textAlignment = .left
        
        txtTitle.textColor = .black
        txtNote.textColor = .black
        
        txtTitle.autocorrectionType = .no
        txtNote.autocorrectionType = .no

        txtTitle.autocapitalizationType = .sentences
        txtNote.autocapitalizationType = .sentences

        txtTitle.font = UIFont.boldSystemFont(ofSize: 19)
        txtNote.font = UIFont.systemFont(ofSize: 17)


    }
    
    override func actionLeftBarTapped(sender: UIBarButtonItem) {
           self.dismiss(animated: true, completion: nil)
    }
    override func actionRightBarTapped(sender: UIBarButtonItem) {
        self.btnAddTapped()
    }
    override func btnAddTapped() {
        
        self.view.endEditing(true)
        
        let noteTitle = self.txtTitle.text
        let noteContent = self.txtNote.text
        
        if noteTitle?.isEmpty != true ||  noteContent?.isEmpty != true{

            let realm = try! Realm()
            try! realm.write {
                self.noteLocal.title = noteTitle
                self.noteLocal.note = noteContent
                self.noteLocal.date_updated = Date()
                realm.add(self.noteLocal)
            }
            
            self.doAfterDelay(inMain: 1) {
                SVSharedClass.shared.appRequestReview()
            }

            self.dismiss(animated: true, completion: nil)

        }

    }

    func loadNoteInformation()  {
        self.txtTitle.text = self.noteLocal.title ?? ""
        self.txtNote.text = self.noteLocal.note ?? ""

    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if self.txtNote.isFirstResponder == true{
            self.txtNote.resignFirstResponder()
        }else{
            self.txtNote.becomeFirstResponder()
        }
    }


}

class TextViewNote: GrowingTextView {

    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)

    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }


    func setupUI()  {
        
    }
    
  
}

