//
//  AddContactVC.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit
import RealmSwift
class MR_AddContactVC: SVBaseController {
    
    let fFirstName = InpputFieldView()
    let fLastName = InpputFieldView()
    let fCompanyName = InpputFieldView()
    let fMobileNumber = InpputFieldView()
    let fHomeNumber = InpputFieldView()
    let fWorkNumber = InpputFieldView()
    
    let fEmail = InpputFieldView()
    let fUrl = InpputFieldView()
    let fNote = InpputFieldView()

    let scrView = UIScrollView()
    let vContent = IQPreviousNextView()
    var isForDisplayPurpose:Bool = false
    
    var contact:ContactLocal!
    var isUpdate:Bool = false


    override func viewDidLoad() {
        super.viewDidLoad()

        if isForDisplayPurpose == true {
            self.addRightBarButton(title: "Done")

        }else{
            self.addRightBarButton(title: "Done")
           // addRightBarButton(title: "Done")

        }
        self.addLeftBarButton(title: "Cancel")

        self.setupUI()
        self.loadContactInformation()
        
        self.btnAdd?.isHidden = true

    }
    
    override func setupUI() {
        
        view.addSubview(scrView)
        scrView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.topMargin.equalTo(self.view.snp.topMargin)
            //make.bottomMargin.equalTo(self.view.snp.bottomMargin).offset(-20)
        }
        
        scrView.addSubview(vContent)
        vContent.snp.makeConstraints { (m) in
            m.left.top.right.bottom.equalToSuperview()
            m.width.equalTo(self.view)
        }
        
        vContent.addSubview(fFirstName)
        vContent.addSubview(fLastName)
        vContent.addSubview(fCompanyName)
        vContent.addSubview(fMobileNumber)
        vContent.addSubview(fHomeNumber)
        vContent.addSubview(fWorkNumber)
        
        vContent.addSubview(fEmail)
        vContent.addSubview(fUrl)
        vContent.addSubview(fNote)
        

        
        fFirstName.lblTitle.text = "First Name"
        fFirstName.txtValue.placeholder = "First Name"
        fFirstName.txtValue.autocapitalizationType = .words

        fLastName.lblTitle.text = "Last Name"
        fLastName.txtValue.placeholder = "Last Name"
        fLastName.txtValue.autocapitalizationType = .words

        fCompanyName.lblTitle.text = "Company"
        fCompanyName.txtValue.placeholder = "Company Name"
        fCompanyName.txtValue.autocapitalizationType = .words

        fMobileNumber.lblTitle.text = "Mobile"
        fMobileNumber.txtValue.placeholder = "Mobile Phone"

        fHomeNumber.lblTitle.text = "Home"
        fHomeNumber.txtValue.placeholder = "Home Phone"

        fWorkNumber.lblTitle.text = "Work"
        fWorkNumber.txtValue.placeholder = "Work Phone"

        fEmail.lblTitle.text = "Email"
        fEmail.txtValue.placeholder = "Email"

        fUrl.lblTitle.text = "URL"
        fUrl.txtValue.placeholder = "URL"

        fNote.lblTitle.text = "Note"
        fNote.txtValue.placeholder = "Note"
        
         if isForDisplayPurpose == true {
            
            let arrField = [fFirstName,fLastName,fCompanyName,fMobileNumber,fHomeNumber,fWorkNumber,fEmail,fUrl,fNote]
            arrField.forEach { (field) in
                field.changeEditingMode(isEditing: false)
            }
        }
        
        fFirstName.snp.makeConstraints { (m) in
            m.left.equalTo(20)
            m.top.equalTo(20)
        }
        
        fLastName.snp.makeConstraints { (m) in
            m.right.equalTo(-20)
            m.top.equalTo(fFirstName.snp.top)
            m.left.equalTo(fFirstName.snp.right).offset(10)
            m.width.equalTo(fFirstName.snp.width)
        }
        
        fCompanyName.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview().inset(20)
            m.top.equalTo(fFirstName.snp.bottom).offset(10)
        }
        
        
        fMobileNumber.snp.makeConstraints { (m) in
            m.left.equalTo(fFirstName.snp.left)
            m.top.equalTo(fCompanyName.snp.bottom).offset(40)
        }
        
        fHomeNumber.snp.makeConstraints { (m) in
            m.right.equalTo(fCompanyName.snp.right)
            m.top.equalTo(fMobileNumber.snp.top)
            m.left.equalTo(fMobileNumber.snp.right).offset(10)
            m.width.equalTo(fMobileNumber.snp.width)
        }
        fWorkNumber.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview().inset(20)
            m.top.equalTo(fMobileNumber.snp.bottom).offset(10)
        }

        fEmail.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview().inset(20)
            m.top.equalTo(fWorkNumber.snp.bottom).offset(40)
        }
        
        fUrl.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview().inset(20)
            m.top.equalTo(fEmail.snp.bottom).offset(10)
        }
        
        fNote.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview().inset(20)
            m.top.equalTo(fUrl.snp.bottom).offset(10)
            m.bottom.equalToSuperview().offset(-20)
        }

        if isForDisplayPurpose == false {
            self.insertAddButtonUnder(title: "DONE", viewTop: self.scrView)
        }else{
            self.insertNoteUnder(note: "Tap on field value to copy", viewTop: self.scrView)

        }
        
        scrView.addGestureRecognizer(UITapGestureRecognizer.init(handler: { [weak self] (tap) in
            guard let self = self else { return }
            self.view.endEditing(true)
        }))
        
//        let arrField = [fFirstName,fLastName,fCompanyName,fMobileNumber,fHomeNumber,fWorkNumber,fEmail,fUrl,fNote]
//        arrField.forEach { (field) in
//            field.lblTitle.isHidden = true
//            field.box.backgroundColor = self.view.backgroundColor
//            field.box.addBottomBorderWithHeight(height: 1, color: .lightGray, leftOffset: 4, rightOffset: 4, bottomOffset: 0)
//            field.box.layer.borderWidth = 0
//
//            field.txtValue.attributedPlaceholder = NSAttributedString(string: field.txtValue.placeholder ?? "",
//                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray.withAlphaComponent(0.8)])
//        }

    }
    override func actionLeftBarTapped(sender: UIBarButtonItem) {
           self.dismiss(animated: true, completion: nil)
    }
    override func actionRightBarTapped(sender: UIBarButtonItem) {
        self.btnAddTapped()
    }

    func loadContactInformation()  {
        self.fFirstName.txtValue.text = self.contact.first_name ?? ""
        self.fLastName.txtValue.text  = self.contact.last_name ?? ""
        self.fCompanyName.txtValue.text  = self.contact.company_name ?? ""
        
        self.fMobileNumber.txtValue.text  = self.contact.mobile_number ?? ""
        self.fHomeNumber.txtValue.text = self.contact.home_number ?? ""
        self.fWorkNumber.txtValue.text = self.contact.work_number ?? ""
        
        self.fEmail.txtValue.text = self.contact.email ?? ""
        self.fUrl.txtValue.text = self.contact.url ?? ""
        self.fNote.txtValue.text = self.contact.note ?? ""
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.view.endEditing(true)
    }

    override func btnAddTapped() {

        self.view.endEditing(true)
        
        let first = self.fFirstName.txtValue.text
        let last = self.fLastName.txtValue.text
        let compnay = self.fCompanyName.txtValue.text

        let mobile = self.fMobileNumber.txtValue.text
        let home = self.fHomeNumber.txtValue.text
        let work = self.fWorkNumber.txtValue.text

        let email = self.fEmail.txtValue.text
        let url = self.fUrl.txtValue.text
        let note = self.fNote.txtValue.text
        
        if first?.isEmpty != true ||  last?.isEmpty != true || compnay?.isEmpty != true || mobile?.isEmpty != true ||  home?.isEmpty != true || work?.isEmpty != true || email?.isEmpty != true ||  url?.isEmpty != true || note?.isEmpty != true {
            let realm = try! Realm()
            try! realm.write {
                contact.first_name = first
                contact.last_name = last
                contact.company_name = compnay

                contact.mobile_number = mobile
                contact.home_number = home
                contact.work_number = work

                contact.email = email
                contact.url = url
                contact.note = note
                contact.date_updated = Date()

                realm.add(contact)
            }
            
            self.doAfterDelay(inMain: 1) {
                SVSharedClass.shared.appRequestReview()
            }

            self.navigationController?.dismiss(animated: true, completion: nil)

        }

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
