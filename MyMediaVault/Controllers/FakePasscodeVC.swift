//
//  FakePasscodeVC.swift
//  SecretVault
//
//  Created by Macbook on 17/08/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit

private struct KeySettings {
    static let enable_fake_passcode:String = "enable_fake_passcode"
    static let fake_passcode_enter:String = "fake_passcode_enter"


}
class FakePasscodeVC: SVBaseController {

    let tblSettings:UITableView = UITableView()
    var arrSettings:[SettingMenu] = []

    override func viewDidLoad() {
        super.viewDidLoad()


        self.title = "Fake Passcode"
        setupUI()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
    override func setupUI() {
        super.setupUI()
        
        //For iOS 9 and Above
        if #available(iOS 9, *) {
            //self.tblSettings.cellLayoutMarginsFollowReadableWidth = false
        }
        
        
        view.addSubview(tblSettings)
        tblSettings.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        tblSettings.backgroundColor = .white

        tblSettings.register(CellSetting.self, forCellReuseIdentifier: "CellSetting")
        tblSettings.delegate = self
        tblSettings.dataSource = self
        tblSettings.tableFooterView = UIView()

        
    }
    
    override func updateUI()  {
        //---menu
        var arrSection:[SettingMenu] = []
        var menu:SettingMenu = SettingMenu()
        menu.name = "Fake Passcode"
        menu.keyId = KeySettings.enable_fake_passcode
        arrSection.append(menu)
        
        menu = SettingMenu()
        if SVSharedClass.shared.fakePasscode.count > 0 {
            menu.name = "Change Fake Passcode"
        }else{
            menu.name = "Set Fake Passcode"
        }
        menu.keyId = KeySettings.fake_passcode_enter
        arrSection.append(menu)
        
        arrSettings = arrSection

        self.tblSettings.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FakePasscodeVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettings.count
    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 50
//    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if SVSharedClass.shared.fakePasscode.count == 0 {
            return 80
//        }
//        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       // if SVSharedClass.shared.fakePasscode.count == 0 {
            let lblTitle = UILabel()
            lblTitle.text = "Enable and set Fake passcode\n All your data protected by real PASSCODE\n and will be hidden"
            lblTitle.textColor = .darkGray
            lblTitle.textAlignment = .center
            lblTitle.font = UIFont.systemFont(ofSize: 14)
            lblTitle.numberOfLines = 0
            lblTitle.backgroundColor = .white
            return lblTitle
       // }
       // return nil
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellSetting", for: indexPath) as! CellSetting
        let menu = arrSettings[indexPath.row]

        cell.lblName.text = menu.name
//        cell.imgView.setImage(self.arrSettings[indexPath.row].icon, for: .normal)
        
        let keyID = menu.keyId
        
        if keyID == KeySettings.enable_fake_passcode {
            cell.switchOption.isHidden = false
            cell.accessoryType = .none
        }else{
            cell.switchOption.isHidden = true
            cell.accessoryType = .disclosureIndicator
        }
        

        if keyID == KeySettings.enable_fake_passcode{
            cell.switchOption.isOn = SVSharedClass.shared.enableFakePasscode
            cell.blockSwitchChanged = {[weak self] in
                guard let self = self else { return }
                SVSharedClass.shared.enableFakePasscode = cell.switchOption.isOn
                self.tblSettings.reloadData()
            }
        }
        
        if keyID == KeySettings.fake_passcode_enter{
            if SVSharedClass.shared.enableFakePasscode {
                cell.lblName.textColor = .black
                
                if SVSharedClass.shared.fakePasscode.count == 0 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        cell.shake()
                    }
                }else{
                    cell.lblValue.text = SVSharedClass.shared.fakePasscode
                    cell.lblValue.isHidden = false
                }
            }else{
                cell.lblValue.isHidden = true
                cell.lblName.textColor = .lightGray
                cell.isUserInteractionEnabled = false
            }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menu = arrSettings[indexPath.row]
        tblSettings.deselectRow(at: indexPath, animated: false)
        
        if menu.keyId == KeySettings.fake_passcode_enter{
            AppDel.showSetFakePasscodeController()
        }
        
    }
    
}
