//
//  PasscodeVC.swift
//  SecretVault
//
//  Created by Macbook on 13/08/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit
import LocalAuthentication
import DKCamera

class PasscodeVC: UIViewController {
//    var cameraVC:DKCamera!
    let imgIcon:UIImageView = UIImageView()
    let vPinInput:ViewPinInput = ViewPinInput()
    let btnClose = UIButton()
    let btnForgot = UIButton()
    var blockForgotTapped: (() -> Void)?

    var isCameraIncluded:Bool = false
    
//    var statusBarHidden = true {
//      didSet {
//        setNeedsStatusBarAppearanceUpdate()
//      }
//    }
//
//    override var prefersStatusBarHidden: Bool {
//      return statusBarHidden
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let originalTransform = self.vPinInput.pinDisplay.transform
        self.vPinInput.pinDisplay.transform = originalTransform.translatedBy(x: 1200, y: 0)
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.vPinInput.pinDisplay.transform = .identity
        }, completion: nil)
        
        
        SVSharedClass.shared.isFaceDownDetected = false
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        
    }
    func animatePinToLeft(completion:@escaping (()->Void))  {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.vPinInput.pinDisplay.transform = self.vPinInput.pinDisplay.transform.translatedBy(x: -1200, y: 0)
        }){ (success) in
            completion()
        }
    }
    init(isCameraIncluded:Bool = false) {
        self.isCameraIncluded = isCameraIncluded
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
     func setupUI() {
       // super.setupUI()
        
        if isCameraIncluded == true {
//            self.addCameraView()
        }
        
        let viewTemp = UIView()
        viewTemp.backgroundColor = .black
        view.addSubview(viewTemp)
        viewTemp.snp.makeConstraints { (m) in
            m.edges.equalToSuperview()
        }
        
        view.addSubview(btnClose)
        btnClose.snp.makeConstraints { (m) in
            m.size.equalTo(50)
            m.left.equalTo(10)
            m.top.equalTo(24)
        }
        
        view.addSubview(btnForgot)
        btnForgot.snp.makeConstraints { (m) in
            m.right.equalTo(-10)
            m.top.equalTo(24)
        }

        view.addSubview(vPinInput)
        vPinInput.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.topMargin.equalTo(self.view.snp.topMargin).offset(150)
        }
        
        let imgIcon = UIImageView()
        view.addSubview(imgIcon)
        imgIcon.snp.makeConstraints { (m) in
            m.size.equalTo(80)
            m.centerX.equalToSuperview()
            m.bottom.equalTo(vPinInput.snp.top).offset(-5)
        }
        imgIcon.contentMode = .scaleAspectFit
        imgIcon.image = UIImage.init(named: "iTunesArtwork")
        imgIcon.layer.cornerRadius = 5
        imgIcon.layer.borderWidth = 0.5
        imgIcon.layer.borderColor = UIColor.darkGray.withAlphaComponent(0.6).cgColor
        
        vPinInput.pinButtons.btnCamera.addTarget(self, action: #selector(btnCameraTapped), for: .touchUpInside)
        
        btnClose.setImage(UIImage.init(named: "close_screen")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnClose.tintColor = .appBlue
        btnClose.addTarget(self, action: #selector(btnCloseTapped), for: .touchUpInside)
        btnClose.isHidden = true
        let offset:CGFloat  = 15
        btnClose.imageEdgeInsets = .init(top: offset, left: offset, bottom: offset, right: offset)
           
        
        btnForgot.setTitle("Forgot?", for: .normal)
        btnForgot.setTitleColor(.appBlue, for: .normal)
        btnForgot.addTarget(self, action: #selector(btnForgotTapped), for: .touchUpInside)
        btnForgot.isHidden = true
        
        vPinInput.pinButtons.btnCamera.alpha = 0

        imgIcon.isHidden = true
    }
    @objc func btnCloseTapped()  {
        AppDel.gotoHome()
    }
    
    @objc func btnForgotTapped()  {
        blockForgotTapped?()
    }
    
    func addCameraView()  {
        /*
        if cameraVC == nil {
            cameraVC = DKCamera()
            cameraVC.defaultCaptureDevice = .front
            cameraVC.showsCameraControls = false
            cameraVC.flashMode = .off
            
            addChild(cameraVC)
            view.addSubview(cameraVC.view)
            cameraVC.view.snp.makeConstraints { (m) in
                m.edges.equalToSuperview()
            }
            cameraVC.willMove(toParent: self)
            cameraVC.didMove(toParent: self)
            self.view.sendSubviewToBack(cameraVC.view)
        }
        */
    }
    
    @objc func btnCameraTapped()  {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        present(vc, animated: true)
    }
    
    //---action------------------------------------------------
    class func PresentPasscodeVC(_ oneController:UIViewController?)  {
        var controllerSource:UIViewController?
        
        if let oneController = oneController {
            controllerSource = oneController
        }else{
            if let controller = AppDel.getVisibleViewController(nil) {
                controllerSource = controller
            }else if let window = UIApplication.shared.keyWindow, let controller = window.rootViewController{
                controllerSource = controller
            }
        }
        
        let passcode = PasscodeVC()
        passcode.modalPresentationStyle = .fullScreen
        let navigationController = UINavigationController(rootViewController: passcode)
        navigationController.isNavigationBarHidden = false
        navigationController.modalPresentationStyle = .fullScreen
        controllerSource?.present(navigationController, animated: false, completion: nil)
        
    }



}

class ViewPinInput: UIView {
    let lblText:UILabel = UILabel()
    var pin:String = ""{
        didSet{
            self.updateUI()
        }
    }
    
    
    let pinDisplay:PinDisplay = PinDisplay()
    let pinButtons:PinButtons = PinButtons()
    let btnTouchId:UIButton = UIButton.init(type: .system)

    var blockDidEnterPin: ((String) -> Void)?


    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        
        addSubview(lblText)
        lblText.snp.makeConstraints { (m) in
            m.top.equalToSuperview()
            m.centerX.equalToSuperview()
            m.height.equalTo(40)
        }
        
        addSubview(pinDisplay)
        pinDisplay.snp.makeConstraints { (m) in
            m.top.equalTo(lblText.snp.bottom).offset(10)
            m.centerX.equalToSuperview()
        }
        
        addSubview(pinButtons)
        pinButtons.snp.makeConstraints { (m) in
            m.top.equalTo(pinDisplay.snp.bottom).offset(30)
            m.centerX.equalToSuperview()
        }
        
        addSubview(btnTouchId)
        btnTouchId.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.top.equalTo(pinButtons.snp.bottom).offset(10)
            m.bottom.equalToSuperview()
        }
        

        
        self.lblText.textAlignment = .center
        self.lblText.textColor = .appBlue
        self.lblText.font = UIFont.systemFont(ofSize: 22)
        self.lblText.text = "Enter Passcode"

        pinButtons.btnsAll.forEach({$0.addTarget(self, action: #selector(actionPinButtonsDidTapped(button:)), for: .touchUpInside)})
        pinButtons.btnDelete.addTarget(self, action: #selector(actionPinDeleteDidTapped(button:)), for: .touchUpInside)


        btnTouchId.setTitle("Touch ID", for: .normal)
        btnTouchId.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        btnTouchId.setTitleColor(UIColor.appBlue, for: .normal)
        btnTouchId.addTarget(self, action: #selector(actionTouchButtonIDDidTapped), for: .touchUpInside)
        btnTouchId.isHidden = true
    }
    func shakeForWrongPin()  {
        pinDisplay.shake()
    }
    
    func updateUI()  {
        pinDisplay.filledPinCount = self.pin.count
        pinButtons.btnDelete.isEnabled = (self.pin.count > 0)
    }
    
    @objc func actionTouchButtonIDDidTapped()  {
        let context = LAContext()
        var error: NSError?

        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"

            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [weak self] success, authenticationError in

                DispatchQueue.main.async {
                    if success {
                        if let block  = self?.blockDidEnterPin{
                            block(SVSharedClass.shared.passcode);
                        }

                    } else {
                        // error
                    }
                }
            }
        } else {
            // no biometry
        }
    }
    
    @objc func actionPinButtonsDidTapped(button:UIButton)  {
        Haptico.shared().generate(.heavy)
        
        if self.pin.count < 4{
            self.pin.append("\(button.tag)")
        }
        
        
        if self.pin.count == 4 {
            self.isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.35) {[weak self] in
                guard let self = self else { return }
                if let block  = self.blockDidEnterPin{
                    
                    /*
                    UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                        self.pinDisplay.transform = self.pinDisplay.transform.translatedBy(x: -1200, y: 0)
                    }){ (success) in
                        block(self.pin);
                    }
                    */
                    block(self.pin);

                }
                self.isUserInteractionEnabled = true
                
            }
        }
        
    }
    @objc func actionPinDeleteDidTapped(button:UIButton)  {
        if self.pin.count > 0{
            self.pin.removeLast()
        }
    }
    
    func removePin()  {
        self.pin = ""
    }
}
class PinDisplay:UIView{
    let img1:RoundInput = RoundInput()
    let img2:RoundInput = RoundInput()
    let img3:RoundInput = RoundInput()
    let img4:RoundInput = RoundInput()
    
    var filledPinCount:Int = 0{
        didSet{
            self.updateUI()
        }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        let arrImages = [img1,img2,img3,img4]
        arrImages.forEach { (imageV) in
            imageV.setFilled(filled: false)
        }
        let stack = UIStackView.init(arrangedSubviews: arrImages)
        stack.distribution = .fillEqually
        stack.axis = .horizontal
        stack.spacing = 10
        arrImages.forEach { (roundInput) in
            roundInput.snp.makeConstraints { (m) in
                let size = 15.0
                m.size.equalTo(size)
                roundInput.layer.cornerRadius = CGFloat(size/2)
            }
        }
        
        addSubview(stack)
        stack.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
    }
    func updateUI()  {
        let arrImages = [img1,img2,img3,img4]
        arrImages.forEach({$0.setFilled(filled: false)})
        if filledPinCount > 0 {
            for index in 0...filledPinCount-1{
                if index < arrImages.count {
                    let round = arrImages[index]
                    round.setFilled(filled: true)
                }
            }
        }
    }
    

}
class RoundInput:UIView{
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        self.backgroundColor = .clear
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.appBlue.cgColor
    }
    
    func setFilled(filled:Bool) {
        if filled == true {
            self.backgroundColor = UIColor.appBlue
        }else{
            self.backgroundColor = .clear
        }
    }
    
    
}


class PinButtons:UIView{
    let btn1 = RoundPinButton()
    let btn2 = RoundPinButton()
    let btn3 = RoundPinButton()
    
    let btn4 = RoundPinButton()
    let btn5 = RoundPinButton()
    let btn6 = RoundPinButton()

    let btn7 = RoundPinButton()
    let btn8 = RoundPinButton()
    let btn9 = RoundPinButton()

    let btn0 = RoundPinButton()
    let btnDelete = UIButton()
    let btnCamera = UIButton()

    var btnsAll:[RoundPinButton] = []
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        btnsAll = [btn0,btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn0]

        let arrBtnRow1 = [btn1,btn2,btn3]
        let arrBtnRow2 = [btn4,btn5,btn6]
        let arrBtnRow3 = [btn7,btn8,btn9]
        let arrBtnRow4 = [btnCamera,btn0,btnDelete]

        let stack1 = UIStackView.init(arrangedSubviews: arrBtnRow1)
        let stack2 = UIStackView.init(arrangedSubviews: arrBtnRow2)
        let stack3 = UIStackView.init(arrangedSubviews: arrBtnRow3)
        let stack4 = UIStackView.init(arrangedSubviews: arrBtnRow4)

        let arrAllRows = [stack1,stack2,stack3,stack4]
        
        let spacingBtn = (AppDel.window!.frame.width * 0.040)
        arrAllRows.forEach { (stackRow) in
            addSubview(stackRow)
            stackRow.distribution = .fillEqually
            stackRow.axis = .horizontal
            stackRow.spacing = spacingBtn
            stackRow.arrangedSubviews.forEach { (roundInput) in
                roundInput.snp.makeConstraints { (m) in
                    let heightBtn = (AppDel.window!.frame.width * 0.19)
                    m.size.equalTo(heightBtn)
                    roundInput.layer.cornerRadius = CGFloat((heightBtn/2.0))
                }
            }
            
        }
        
        let stackFinal = UIStackView.init(arrangedSubviews: arrAllRows)
        stackFinal.distribution = .fillEqually
        stackFinal.axis = .vertical
        stackFinal.spacing = spacingBtn
            
        addSubview(stackFinal)
        stackFinal.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        
        btn1.setTitle("1", for: .normal)
        btn2.setTitle("2", for: .normal)
        btn3.setTitle("3", for: .normal)
        btn4.setTitle("4", for: .normal)
        btn5.setTitle("5", for: .normal)
        btn6.setTitle("6", for: .normal)
        btn7.setTitle("7", for: .normal)
        btn8.setTitle("8", for: .normal)
        btn9.setTitle("9", for: .normal)
        btn0.setTitle("0", for: .normal)
        
        btn1.tag = 1
        btn2.tag = 2
        btn3.tag = 3
        btn4.tag = 4
        btn5.tag = 5
        btn6.tag = 6
        btn7.tag = 7
        btn8.tag = 8
        btn9.tag = 9
        btn0.tag = 0

        btnCamera.setImage(UIImage.init(named: "camera")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnCamera.tintColor = UIColor.lightGray.withAlphaComponent(0.6)
        
        btnDelete.setTitle("Delete", for: .normal)
        btnDelete.setTitleColor(.appBlue, for: .normal)
        btnDelete.setTitleColor(.darkGray, for: .disabled)
        btnDelete.isEnabled = false
    }

}
class RoundPinButton:UIButton{
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        self.backgroundColor = UIColor.RGB(50,51,52)
        
        self.setTitleColor(.white, for: .normal)
        self.setTitleColor(.black, for: .highlighted)

        self.setTitle("", for: .normal)
        
        
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        self.titleLabel?.textAlignment = .center
        self.setBackgroundColor(color: UIColor.RGB(50,51,52), forState: .normal)
        self.setBackgroundColor(color: UIColor.appBlue, forState: .highlighted)
    }
    
    
    
}

