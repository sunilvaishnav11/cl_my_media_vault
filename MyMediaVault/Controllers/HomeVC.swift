//
//  HomeVC.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit

enum HomeMenu {
    case photos
    case videos
    case passwords
    case notes
    case contacts
    case browser
    case settings

    var icon_bg_color:UIColor{
        switch self {
        case .photos:
            return UIColor.RGB(88,185,166)
        case .videos:
            return UIColor.RGB(215,112,75)
        case .passwords:
            return UIColor.RGB(76,130,225)
        case .notes:
            return UIColor.RGB(216,94,173)
        case .contacts:
            return UIColor.RGB(117,83,220)
        case .browser:
            return UIColor.RGB(83,166,191)
        case .settings:
            return UIColor.RGB(215,112,75)
        }
    }
    
        
    var bg_color:UIColor{
        switch self {
        case .photos:
            return UIColor.RGB(135,210,197)
        case .videos:
            return UIColor.RGB(241,171,144)
        case .passwords:
            return UIColor.RGB(161,190,239)
        case .notes:
            return UIColor.RGB(234,164,211)
        case .contacts:
            return UIColor.RGB(183,164,236)
        case .browser:
            return UIColor.RGB(154,198,209)
        case .settings:
            return UIColor.RGB(241,171,144)
        }
    }
    var icon_image:UIImage{
        switch self {
        case .photos:
            return UIImage.init(named: "icon_photos_vault")!
        case .videos:
            return UIImage.init(named: "icon_video_vault")!
        case .passwords:
            return UIImage.init(named: "icon_password_vault")!
        case .notes:
            return UIImage.init(named: "icon_notes_vault")!
        case .contacts:
            return UIImage.init(named: "icon_contacts_vault")!
        case .browser:
            return UIImage.init(named: "icon_browser_vault")!
        case .settings:
            return UIImage.init(named: "icon_settings_vault")!
        }
    }
    
    var title:String{
        switch self {
        case .photos:
            return "Media"
        case .videos:
            return "Videos"
        case .passwords:
            return "Passwords"
        case .notes:
            return "Notes"
        case .contacts:
            return "Contacts"
        case .browser:
            return "Browser"
        case .settings:
            return "Setting"
        }
    }

}
class HomeVC: SVBaseController {

    let arrMenu:[HomeMenu] = [.photos,.passwords,.notes,.contacts,.browser,.settings]
    let clcMenu:UICollectionView = UICollectionView.init(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Vault"
        self.setupUI()
    }
    
    override func setupUI() {
        let layout = CollectionViewWaterfallLayout()
        let spacing:CGFloat = 15
        layout.sectionInset = .init(top: spacing, left: spacing, bottom: spacing, right: spacing)
        layout.headerInset = .init(top: 5, left: 0, bottom: 0, right: 0)
        layout.footerInset = .init(top: 0, left: 0, bottom: 5, right: 0)
        layout.headerHeight = Float(spacing)
        layout.footerHeight = Float(spacing)
        layout.minimumColumnSpacing = Float(spacing)
        layout.minimumInteritemSpacing = Float(spacing)
        layout.columnCount = 2
        
        clcMenu.collectionViewLayout = layout
        clcMenu.backgroundColor = .white
        self.view.backgroundColor = clcMenu.backgroundColor
        clcMenu.delegate = self
        clcMenu.dataSource = self
        clcMenu.register(CellHomeMenu.self, forCellWithReuseIdentifier: "CellHomeMenu")
        
        view.addSubview(clcMenu)
        clcMenu.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }

        updateUI()
    }
    override func updateUI() {
        clcMenu.reloadData()
    }
    
    func actionPhotosTapped()  {
        let photos = MR_MediaVaultVC()
        self.navigationController?.pushViewController(photos, animated: true)
    }
    func actionVideosTapped()  {
//        let videovs = VideoVC()
//        self.navigationController?.pushViewController(videovs, animated: true)

    }
    func actionPasswordTapped()  {
        let passwordvc = MR_PasswordsVC()
        self.navigationController?.pushViewController(passwordvc, animated: true)

    }
    func actionNotesTapped()  {
        let notesvc = MR_NotesVC()
        self.navigationController?.pushViewController(notesvc, animated: true)

    }
    func actionContactsTapped()  {
        let contactvc = MR_ContactsVC()
        self.navigationController?.pushViewController(contactvc, animated: true)

    }
    func actionBrowserTapped()  {
        
        if ISalesManager.shared.isSubscribed() == false{
            SalesVC.PresentSubscriptionPage(self)
            return
        }

        let browservc = MR_BrowserVC()
        self.navigationController?.pushViewController(browservc, animated: true)

    }
    func actionSettingsTapped()  {
        let settings = SettingsVC()
        self.navigationController?.pushViewController(settings, animated: true)

    }

}

extension HomeVC:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMenu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellHomeMenu", for: indexPath) as! CellHomeMenu
        cell.homeMenu = arrMenu[indexPath.row]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.20) {
            //push
            switch self.arrMenu[indexPath.row] {
            case .photos:
                self.actionPhotosTapped()
            case .videos:
                self.actionVideosTapped()
            case .passwords:
                self.actionPasswordTapped()
            case .notes:
                self.actionNotesTapped()
            case .contacts:
                self.actionContactsTapped()
            case .browser:
                self.actionBrowserTapped()
            case .settings:
                self.actionSettingsTapped()
            }

        }

    }
    func collectionView(_ collectionView: UICollectionView, shouldBeginMultipleSelectionInteractionAt indexPath: IndexPath) -> Bool {
        return true
    }
    func collectionView(_ collectionView: UICollectionView, didBeginMultipleSelectionInteractionAt indexPath: IndexPath) {
    }
    func collectionViewDidEndMultipleSelectionInteraction(_ collectionView: UICollectionView) {
        
    }
    
    
}

extension HomeVC:CollectionViewWaterfallLayoutDelegate{
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = self.view.window!.frame.width/CGFloat(3)
        return .init(width: width, height: width)
    }
}


class CellHomeMenu: UICollectionViewCell {
    var homeMenu:HomeMenu = HomeMenu.photos{
        didSet{
            self.updateUICell()
        }

    }
    let imgBGColor:UIImageView = UIImageView()
    let imgRound:UIImageView = UIImageView()
    let imgIcon:UIImageView = UIImageView()
    let lblTitle:UILabel = UILabel()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUICell()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupUICell(){
        self.contentView.addSubview(self.imgBGColor)
        self.contentView.addSubview(self.imgRound)
        self.imgRound.addSubview(self.imgIcon)
        self.contentView.addSubview(self.lblTitle)

        imgBGColor.snp.makeConstraints { (m) in
            m.edges.equalToSuperview()//.inset(10)
        }
        
        imgRound.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.top.equalTo(30)
            m.size.equalTo(90)
        }
        imgIcon.snp.makeConstraints { (m) in
            m.center.equalToSuperview()
            m.width.equalToSuperview().multipliedBy(0.5)
            m.height.equalTo(imgIcon.snp.width)
        }
        lblTitle.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.top.equalTo(imgRound.snp.bottom)
            m.bottom.equalToSuperview()
        }
        
        self.contentView.backgroundColor = .clear
        self.contentView.clipsToBounds = true
        
        //---image round-------
        self.imgRound.layer.cornerRadius = 45
        
        self.imgIcon.contentMode = .scaleAspectFit
        
        //----title
        self.lblTitle.textAlignment = .center
        self.lblTitle.textColor = UIColor.white
        self.lblTitle.font = UIFont.boldSystemFont(ofSize: 20)
        
    }
    
    func updateUICell() {
        self.imgBGColor.layer.cornerRadius = 10
        self.imgBGColor.backgroundColor = self.homeMenu.bg_color
        self.imgRound.backgroundColor = self.homeMenu.icon_bg_color
        
        self.imgIcon.tintColor = .white
        self.imgIcon.image = self.homeMenu.icon_image.withRenderingMode(.alwaysTemplate)
        self.lblTitle.text = self.homeMenu.title
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.imgBGColor.backgroundColor = self.homeMenu.icon_bg_color
        self.imgRound.backgroundColor = self.homeMenu.bg_color
        
        UIView.animate(withDuration: 0.10, animations: {
            self.transform = CGAffineTransform.identity.scaledBy(x: 0.95, y: 0.95)
            self.imgRound.transform = CGAffineTransform.identity.scaledBy(x: 1.15, y: 1.15)
            
        }) { (success) in
            
        }
        super.touchesBegan(touches, with: event)
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.imgBGColor.backgroundColor = self.homeMenu.icon_bg_color
        self.imgRound.backgroundColor = self.homeMenu.bg_color
        
        UIView.animate(withDuration: 0.10,delay:0.10, animations: {
            self.transform = CGAffineTransform.identity
            self.imgRound.transform = CGAffineTransform.identity
            
        }) { (success) in
            self.imgBGColor.backgroundColor = self.homeMenu.bg_color
            self.imgRound.backgroundColor = self.homeMenu.icon_bg_color
            
        }
        super.touchesEnded(touches, with: event)
        
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.imgBGColor.backgroundColor = self.homeMenu.icon_bg_color
        self.imgRound.backgroundColor = self.homeMenu.bg_color
        
        UIView.animate(withDuration: 0.15,delay:0.15, animations: {
            self.transform = CGAffineTransform.identity
            self.imgRound.transform = CGAffineTransform.identity

        }) { (success) in
            self.imgBGColor.backgroundColor = self.homeMenu.bg_color
            self.imgRound.backgroundColor = self.homeMenu.icon_bg_color
            
        }
        super.touchesCancelled(touches, with: event)
    }
    

    
    
}
