//
//  WaitUntilFailAttemptVC.swift
//  SecretVault
//
//  Created by Macbook on 01/09/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit

class WaitUntilFailAttemptVC: SVBaseController {
    
    let lblTitle:UILabel = UILabel()
    let lblDescr:UILabel = UILabel()
    let lblTime:UILabel = UILabel()

    var timerTest : Timer?
    override func viewDidLoad() {
        super.viewDidLoad()


        self.setupUI()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateTime()
        timerTest = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    deinit {
        self.invalidateTimer()
    }
    override func setupUI() {
        super.setupUI()
        
        self.view.backgroundColor = .black
        view.addSubview(lblTitle)
        view.addSubview(lblTime)
        view.addSubview(lblDescr)
        
        lblTitle.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.width.equalToSuperview().offset(-20)
            m.top.equalTo(self.view.snp.topMargin).offset(50)
        }
        
        lblTime.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.centerY.equalToSuperview()
            m.width.equalToSuperview()
        }
        lblDescr.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.width.equalToSuperview().offset(-20)
            m.bottom.equalTo(self.view.snp.bottomMargin).offset(-50)
        }

        lblTitle.textAlignment = .center
        lblTime.textAlignment = .center
        lblDescr.textAlignment = .center
        
        lblTitle.numberOfLines = 0
        lblDescr.numberOfLines = 0

        lblTitle.textColor = .white
        lblTime.textColor = .white
        lblDescr.textColor = .white

        lblTitle.font = UIFont.systemFont(ofSize: 18)
        lblTime.font = UIFont.systemFont(ofSize: 65)
        lblDescr.font = UIFont.systemFont(ofSize: 17)
        
        
        lblTitle.text = "Wrong Passcode entered 3 times.\nPlease wait untill..."
        lblDescr.text = "Sorry for the inconvenience. We do our best to protect you against any intruder attack."
        
    }
    
    @objc func updateTime()  {
        if let dateAppLockUntil = SVSharedClass.shared.appLockedUntil,Date() <= dateAppLockUntil {
             let seconds = dateAppLockUntil.seconds(from: Date())
            let timeMMHH = secondsToHoursMinutesSeconds(seconds: seconds)
            
            
            self.lblTime.text = String.init(format: "%02d:%02d", timeMMHH.1,timeMMHH.2)
        }else{
            self.invalidateTimer()
            AppDel.showPasscodeController()
        }
    }
    
    func invalidateTimer()  {
        timerTest?.invalidate()
        timerTest = nil
        Timer.cancelPreviousPerformRequests(withTarget: self, selector: #selector(updateTime), object: nil)
        
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
      return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
