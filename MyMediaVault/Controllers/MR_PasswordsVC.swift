//
//  PasswordsVC.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit
import RealmSwift

class MR_PasswordsVC: SVBaseController {

    let tablePassword:UITableView = UITableView()
    var arrPassword:[PasswordLocal] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Passwords"
        self.addRightBarButton(image: UIImage.init(systemName: "plus")!)
        setupUI()
        
        
    }
    
    override func actionRightBarTapped(sender: UIBarButtonItem) {
        self.btnAddTapped()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
    override func setupUI() {
        
        view.addSubview(tablePassword)
        tablePassword.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
        }
        

        tablePassword.register(CellPasswordInfo.self, forCellReuseIdentifier: "CellPasswordInfo")
        tablePassword.delegate = self
        tablePassword.dataSource = self
        tablePassword.tableFooterView = UIView()
        
        tablePassword.separatorStyle = .singleLine
        //tblPassword.separatorInset = UIEdgeInsets(top: 0, left: 80, bottom: 0, right: 0)
        tablePassword.separatorInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 0)

        tablePassword.emptyDataSetSource = self
        tablePassword.emptyDataSetDelegate = self
        tablePassword.tableFooterView = UIView()

        self.insertAddButtonUnder(title: "Add Password", viewTop: tablePassword)

    }
    override func updateUI() {
        let realm = try! Realm()
        let result = realm.objects(PasswordLocal.self).filter("is_fake == \(SVSharedClass.shared.isFakePasscode)")
        self.arrPassword = Array(result)
        self.arrPassword = Array(result).sorted(by: { (obj1, obj2) -> Bool in
            return obj1.date_created! > obj2.date_created!
        })

        tablePassword.reloadData()

    }
    
    override func btnAddTapped() {
        let addNew = MR_AddPasswordVC()
        addNew.passwordLocal = PasswordLocal()
        let navi = UINavigationController.init(rootViewController: addNew)
        navi.modalPresentationStyle = .fullScreen
        self.present(navi, animated: true, completion: nil)

    }


}

extension MR_PasswordsVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPassword.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.arrPassword.count > 0,SVSharedClass.shared.launchCountValue < ConstantValue.swipeHelpShowCount {
            return 50
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.arrPassword.count > 0,SVSharedClass.shared.launchCountValue < ConstantValue.swipeHelpShowCount {
            let lblTitle = UILabel()
            lblTitle.text = ConstantValue.swipeHelpText
            lblTitle.font = UIFont.systemFont(ofSize: 14)
            lblTitle.textColor = .darkGray
            lblTitle.textAlignment = .center
            lblTitle.backgroundColor = .white
            return lblTitle
        }
        return nil
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellPasswordInfo", for: indexPath) as! CellPasswordInfo
        cell.passwordLocal = arrPassword[indexPath.row]
        cell.delegate = self
        cell.btnShowPassword.onTap {
            cell.btnShowPassword.isSelected = !cell.btnShowPassword.isSelected
            cell.updateUI()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let passwordLocal = arrPassword[indexPath.row]

        let addNew = MR_AddPasswordVC()
        addNew.passwordLocal = passwordLocal
        addNew.isForDisplayPurpose = true
        let navi = UINavigationController.init(rootViewController: addNew)
        navi.modalPresentationStyle = .fullScreen
        self.present(navi, animated: true, completion: nil)

//        self.navigationController?.pushViewController(addNew, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }

        
        let editAction = SwipeAction(style: .default, title: nil) {  [weak self]  action, indexPath in
            // handle action by updating model with deletion
            guard let self = self else { return }
            
            self.cellEditTappedAt(indexPath: indexPath)
        }
        editAction.image = UIImage(named: "edit_contact2")?.withRenderingMode(.alwaysTemplate)
        editAction.textColor = .black
        editAction.backgroundColor = .appBlue

        
        let deleteAction = SwipeAction(style: .destructive, title: nil) { [weak self] action, indexPath in
            // handle action by updating model with deletion
            guard let self = self else { return }

            self.cellDeleteTappedAt(indexPath: indexPath)
            
        }
        deleteAction.textColor = .black

        // customize the action appearance
        deleteAction.image = UIImage(named: "delete_trash")?.withRenderingMode(.alwaysTemplate)

        return [deleteAction,editAction]
    }
    
    func cellEditTappedAt(indexPath:IndexPath)  {
        let passwordLocal = arrPassword[indexPath.row]
        
        let addNew = MR_AddPasswordVC()
        addNew.passwordLocal = passwordLocal
        addNew.isUpdate = true
        let navi = UINavigationController.init(rootViewController: addNew)
        navi.modalPresentationStyle = .fullScreen
        self.present(navi, animated: true, completion: nil)

//        self.navigationController?.pushViewController(addNew, animated: true)
        
    }
    func cellDeleteTappedAt(indexPath:IndexPath)  {
        let passwordLocal = arrPassword[indexPath.row]
        
        let realm = try! Realm()
        try! realm.write {
            realm.delete(passwordLocal)
        }
        self.updateUI()
    }
    
   
}
extension MR_PasswordsVC : SwipeTableViewCellDelegate{
    
  func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .none
        options.transitionStyle = .drag
        return options
    }
}

extension MR_PasswordsVC:DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let nameBuilder = AttributedStringBuilder()
        nameBuilder.text("No password added", attributes: [.font(UIFont.systemFont(ofSize: 30)),.textColor(.black),.alignment(.center)])
        
        return nameBuilder.attributedString

    }
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let nameBuilder = AttributedStringBuilder()
        nameBuilder.text("Start adding your Passwords\nAll data are securely store on your iPhone. Nobody can access this information", attributes: [.font(UIFont.systemFont(ofSize: 18)),.textColor(.darkGray),.alignment(.center)])
        
        return nameBuilder.attributedString

    }
    
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        if self.arrPassword.count > 0 {
            return false
        }
        return true
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "no_password")
    }

}


class CellPasswordInfo: SwipeTableViewCell {
    var lblUrl:UILabel = UILabel()
    var lblUsername:UILabel = UILabel()
    var lblPassword:UILabel = UILabel()
    var btnShowPassword:UIButton = UIButton()
    
    var passwordLocal:PasswordLocal!{
        didSet{
            updateUI()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // code common to all your cells goes here
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setupUI()   {
        contentView.addSubview(lblUrl)
        contentView.addSubview(lblUsername)
        contentView.addSubview(lblPassword)
        contentView.addSubview(btnShowPassword)
        
        lblUrl.snp.makeConstraints { (m) in
            m.left.equalTo(30)
            m.bottom.equalTo(lblUsername.snp.top)
        }
        lblUsername.snp.makeConstraints { (m) in
            m.top.equalTo(lblUrl.snp.bottom)
            m.left.equalTo(lblUrl.snp.left)
            m.centerY.equalToSuperview()
        }
        lblPassword.snp.makeConstraints { (m) in
            m.top.equalTo(lblUsername.snp.bottom)
            m.left.equalTo(lblUrl.snp.left)
        }
        
        btnShowPassword.snp.makeConstraints { (m) in
            m.centerY.equalToSuperview()
            m.right.equalTo(-10)
            m.width.equalTo(45)
            m.height.equalTo(30)
        }
        
        btnShowPassword.setTitleColor(.appBlue, for: .normal)
        btnShowPassword.setTitle("SHOW", for: .normal)
        btnShowPassword.setTitle("HIDE", for: .selected)
        btnShowPassword.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)

        lblUrl.textAlignment = .left
        lblUrl.textColor = .black
        lblUrl.font = UIFont.boldSystemFont(ofSize:  17)
        
        lblUsername.textAlignment = .left
        lblUsername.textColor = .RGB(138,138,145)
        lblUsername.font = UIFont.boldSystemFont(ofSize:  15)
        
        lblPassword.textAlignment = .left
        lblPassword.textColor = .RGB(138,138,145)
        lblPassword.font = UIFont.systemFont(ofSize:  15)
        
        
    }
    
    
    func updateUI()  {
        self.lblUrl.text = (self.passwordLocal.url ?? "")
        self.showUsernamePassword(show: btnShowPassword.isSelected)

    }
    
    func showUsernamePassword(show:Bool)  {
        if show ==  true {
            self.lblUsername.text = ((self.passwordLocal.username?.isEmpty == true) ? "NO USERNAME" : self.passwordLocal.username!)
            self.lblPassword.text = ((self.passwordLocal.password?.isEmpty == true) ? "NO PASSWORD" : self.passwordLocal.password!)

        }else{
            self.lblUsername.text = "USERNAME"
            self.lblPassword.text = "PASSWORD"
        }
    }
}
