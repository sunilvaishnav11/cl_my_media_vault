//
//  MasterKeyVC.swift
//  SecretVault
//
//  Created by Macbook on 29/08/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit
import Messages
import MessageUI

class MasterKeyVC: SVBaseController,MFMailComposeViewControllerDelegate {
    let containerView = UIView()
    let lblCode = UILabel()
    let lblTitle = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = "Master Recovery Key"
        view.backgroundColor = .appBackgroundtable
        self.setupUI()
        
        containerView.isUserInteractionEnabled = true
        containerView.addTapGesture {[weak self] (tap) in
            guard let self = self else { return }
            self.actionEmailTpped()
        }
    }
        
    override func setupUI() {
        super.setupUI()
        
        view.addSubview(containerView)
        containerView.snp.makeConstraints { (m) in
            m.width.equalToSuperview().offset(-25)
            m.center.equalToSuperview()
        }
        
        containerView.addSubview(lblCode)
        lblCode.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.top.equalTo(30)
        }
        
        
        self.view.addSubview(lblTitle)
        lblTitle.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.bottom.equalTo(containerView.snp.top).offset(-15)
        }
        
        lblTitle.text = "Master Recovery Key"
        lblTitle.textAlignment = .center
        lblTitle.textColor = UIColor.darkGray
        lblTitle.font = UIFont.boldSystemFont(ofSize: 28)
        
        let lblCodeHelp = UILabel()
        containerView.addSubview(lblCodeHelp)
        lblCodeHelp.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.top.equalTo(lblCode.snp.bottom).offset(10)
            m.width.equalToSuperview().offset(-20)
            m.bottom.equalTo(-30)
        }
        
        let btnRegenerate = UIBarButtonItem.init(barButtonSystemItem: .refresh) {[weak self] in
            guard let self = self else { return }
            self.actionRegenerateTapped()
        }
//        view.addSubview(btnRegenerate)
//        btnRegenerate.snp.makeConstraints { (m) in
//            m.size.equalTo(40)
//            m.left.equalTo(containerView.snp.left)
//            m.top.equalTo(containerView.snp.bottom).offset(5)
//        }
        
        let btnEmail = UIBarButtonItem.init(title: "Email Yourself", style: .plain) {[weak self]in
            guard let self = self else { return }
            self.actionEmailTpped()
        }
        let btnShare = UIBarButtonItem.init(barButtonSystemItem: .action) {[weak self] in
            guard let self = self else { return }
            self.actionShareTpped()
        }
        let bar = UIToolbar.init()
        bar.translatesAutoresizingMaskIntoConstraints = false
        bar.items = [btnRegenerate,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, handler: {
            
        }),btnEmail,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, handler: {
            
        }),btnShare]
        bar.tintColor = .appBlue
        view.addSubview(bar)
        bar.snp.makeConstraints { (m) in
            m.top.equalTo(containerView.snp.bottom).offset(20)
            m.left.equalTo(containerView.snp.left)
            m.right.equalTo(containerView.snp.right)

        }
        bar.barTintColor = self.view.backgroundColor
        bar.layer.borderWidth = 1;
        bar.layer.borderColor = self.view.backgroundColor?.cgColor
        bar.clipsToBounds = true
//        view.addSubview(btnShare)
//        btnShare.snp.makeConstraints { (m) in
//            m.size.equalTo(40)
//            m.right.equalTo(containerView.snp.right)
//            m.top.equalTo(containerView.snp.bottom).offset(5)
//        }
        
        
        lblCode.textAlignment = .center
        lblCode.font = UIFont.systemFont(ofSize: 33)
        lblCode.textColor = .black
        
        lblCodeHelp.textAlignment = .center
        lblCodeHelp.font = UIFont.systemFont(ofSize: 16)
        lblCodeHelp.textColor = .darkGray
        lblCodeHelp.numberOfLines = 0
        
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 6
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOffset = CGSize(width: 1, height: 2.0)
        containerView.layer.shadowRadius = 6
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.masksToBounds = false


        lblCode.text = SVSharedClass.shared.master_recovery_key
        lblCodeHelp.text = "Save this key in safe place\n\nso you can use it when you forgot your passcode. Tap on key to email  it to yourself. Never share this key to anybody else."
    }
    
    
    func actionRegenerateTapped()  {

         let alert = UIAlertController.alertViewController(withTitle: "Re-Generate Master Key", message: "Are you sure you want to re-generate master recovery key? This action will invalid existing key.")
        alert.addCancelButton()
        alert.addButton(withTitle: "Re-Generate") { (action) in
            SVSharedClass.shared.master_recovery_key = randomMasterKey()
            self.lblCode.alpha = 0.0
            UIView.animate(withDuration: 0.5) {
                self.lblCode.text = SVSharedClass.shared.master_recovery_key
                self.lblCode.alpha = 1
            }
        }
        alert.show()
    }
    func actionShareTpped()  {

        let textToShare = [ SVSharedClass.shared.master_recovery_key ]
        let activityViewController = UIActivityViewController(activityItems: textToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)

    }
    func actionEmailTpped()  {

        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self

            mail.setSubject("Master Recovery Key")
            mail.setMessageBody("Master Recovery Key:\n\n\(SVSharedClass.shared.master_recovery_key) \n\n\n\nHow to use it\n1)Open app.\n2)Tap on forgot button on passcode screen.\n3)Enter this key and set new passcode.\n4)It will also remove folder passcode.", isHTML: false)
            
            present(mail, animated: true, completion: nil)
        } else {
            print("Cannot send mail")
            // give feedback to the user
        }


    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?){

        switch result {
        case .cancelled:
            print("Cancelled")
        case .sent:
            print("Sent")
            SVSharedClass.shared.appRequestReview()
            //set count tip to 5 so it will not visible
        case .failed:
            print("Error")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }

    


}
