//
//  SalesVC.swift
//  MyMediaVault
//
//  Created by Macbook on 30/11/21.
//

import UIKit

class SalesVC: UIViewController {

    let scrollview = UIScrollView()
    let contentView = UIView()

    var selectedOption:PurchaseOption = .year//.month

    let btnRestore = UIButton()
    let btnClose = UIButton()
    
    let viewTop = UIView()
    let imgIcon = UIImageView()
    let lblTitle = UILabel()
    
    let viewBottom = UIView()
    let btnContinue = UIButton()
    let btnTerms = UIButton()
    let btnPrivacy = UIButton()
    
    let monthPlanView = ViewPlan()
    let yearPlanView = ViewPlan()

    let imgArrow:UIImageView = UIImageView()

    var blockCompletion:BlockCompletion?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        self.updateUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseIn,.autoreverse,.repeat], animations: {
            self.imgArrow.transform = CGAffineTransform(translationX: 6, y: 0)
        }, completion: nil)


        updateUI()
    }
    func setupUI()  {
        self.view.backgroundColor = .black
        contentView.backgroundColor = .clear
        scrollview.backgroundColor = .clear

        let viewTopNotch = UIView()
        view.addSubview(viewTopNotch)

        view.addSubview(scrollview)
        view.addSubview(viewBottom)
        scrollview.addSubview(contentView)
        
        //top notch
        viewTopNotch.snp.makeConstraints { (m) in
            m.left.equalToSuperview()
            m.right.equalToSuperview()
            m.height.equalTo(400)
            m.top.equalTo(self.view.snp.topMargin).offset(-100)
        }
        
        scrollview.snp.makeConstraints { (m) in
            m.top.equalTo(self.view.snp.topMargin)
            m.left.right.equalToSuperview()
            m.bottom.equalTo(self.view.snp.bottomMargin)
        }
        
        contentView.snp.makeConstraints { (m) in
            m.left.top.right.equalToSuperview()
            m.width.equalToSuperview()
            m.bottom.equalToSuperview()
        }
        viewBottom.snp.makeConstraints { (m) in
//            m.top.equalTo(scrollview.snp.bottom)
            m.left.right.equalToSuperview()
            m.bottom.equalTo(self.view.snp.bottomMargin)
            m.height.equalTo(100)
        }
        
        contentView.addSubview(viewTop)
        viewTop.snp.makeConstraints { (m) in
            m.left.equalToSuperview()
            m.top.equalToSuperview()
            m.right.equalToSuperview()
            m.height.equalTo(AppDel.window!.frame.height/3.2) //.multipliedBy(0.3)
        }
        
        viewTop.addSubview(imgIcon)
        viewTop.addSubview(lblTitle)
        
        imgIcon.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.size.equalTo(80)
            m.bottom.equalTo(viewTop.snp.centerY)
        }
        lblTitle.snp.makeConstraints { (m) in
            m.centerX.equalToSuperview()
            m.top.equalTo(imgIcon.snp.bottom)
            m.bottom.equalToSuperview()
        }
 
        
        scrollview.backgroundColor = UIColor.white
        viewBottom.backgroundColor = .clear

        self.view.backgroundColor = UIColor.RGB(242,241,246)
        viewTop.backgroundColor = UIColor.RGB(242,241,246)
        viewTopNotch.backgroundColor = viewTop.backgroundColor
        
        imgIcon.image = UIImage.init(named: "app_icon_sale")
        imgIcon.backgroundColor = .clear
        imgIcon.contentMode = .scaleAspectFit
        imgIcon.clipsToBounds = true
        imgIcon.layer.cornerRadius = 12
        /*
        let shadow = UIView()
        imgIcon.superview!.addSubview(shadow)
        shadow.snp.makeConstraints { (m) in
            m.edges.equalTo(imgIcon)
        }
        imgIcon.superview?.bringSubviewToFront(imgIcon)
        
        shadow.backgroundColor = .clear
        shadow.clipsToBounds = false
        shadow.layer.masksToBounds = false
        shadow.layer.shadowColor = UIColor.black.cgColor
        shadow.layer.shadowOffset = .zero
        shadow.layer.shadowOpacity = 0.8
        shadow.layer.shadowRadius = 2
        */
        

        lblTitle.textColor = .black
        lblTitle.numberOfLines = 0
        lblTitle.textAlignment = .center
        lblTitle.font = UIFont.boldSystemFont(ofSize: 24)

        scrollview.addSubview(btnClose)
        scrollview.addSubview(btnRestore)
        
        btnClose.snp.makeConstraints { (m) in
            m.left.equalTo(10)
            m.size.equalTo(23)
            m.top.equalToSuperview()
        }
        btnClose.tintColor = UIColor.lightGray.withAlphaComponent(0.55)
        btnClose.setImage(UIImage.init(systemName: "xmark"), for: .normal)

        
        btnRestore.snp.makeConstraints { (m) in
            m.right.equalTo(-10)
            m.centerY.equalTo(btnClose.snp.centerY)
        }
        btnRestore.setTitle("Restore", for: .normal)
        btnRestore.setTitleColor(.lightGray, for: .normal)
        
        //feature label
        let arrStrFeature:[String] = ["Unlimited storage for media",
                                      "Store Password, Note, Contact",
                                      "Break-in Alerts",
                                      "Secure Browser",
                                      ]
        
        let arrViewFeature = arrStrFeature.map { (strFeature) -> ViewLabelFeature in
            let vlblFeature = ViewLabelFeature()
            vlblFeature.lblFeature.text = strFeature
            return vlblFeature
        }
        let stackFeature = UIStackView.init(arrangedSubviews: arrViewFeature)
        contentView.addSubview(stackFeature)
        stackFeature.snp.makeConstraints { (make) in
            
            make.centerX.equalToSuperview()
            make.top.equalTo(lblTitle.snp.bottom).offset(30)
//             make.width.equalToSuperview()
            
        }
        stackFeature.spacing = 6
        stackFeature.axis = .vertical
        
        //------------------------------------------------------------------------------
        //------------------------------------------------------------------------------
        let arrPlanViews = [yearPlanView,monthPlanView]
        let stackPlan = UIStackView.init(arrangedSubviews: arrPlanViews)
        contentView.addSubview(stackPlan)
        
        arrPlanViews.forEach { (viewplan) in
            viewplan.snp.makeConstraints { (m) in
                m.height.equalTo(60)
            }
        }
        stackPlan.snp.makeConstraints { (m) in
            m.left.equalTo(20)
            m.right.equalTo(-20)
//            m.top.equalTo(stackFeature.snp.bottom).offset(30)
            m.top.equalTo(stackFeature.snp.bottom).offset(30)
            m.bottom.equalToSuperview().offset(-110)
        }
        stackPlan.spacing = 15
        stackPlan.axis = .vertical

        viewBottom.addSubview(btnContinue)
        btnContinue.snp.makeConstraints { (m) in
            m.bottomMargin.equalTo(self.view.snp.bottomMargin).offset(-50)
            //make.width.equalToSuperview().multipliedBy(0.77)
            m.left.equalTo(20)
            m.right.equalTo(-20)
            m.height.equalTo(60)
            m.centerX.equalToSuperview()

        }
        
        btnContinue.setTitle("Continue", for: .normal)
        btnContinue.setTitleColor(.white, for: .normal)
        btnContinue.backgroundColor = UIColor.appBlue//.RGB(47,124,246)
        btnContinue.layer.cornerRadius = 6
        btnContinue.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25)

        if #available(iOS 13.0, *) {
            imgArrow.image = UIImage.init(systemName: "arrow.right.square.fill")?.withRenderingMode(.alwaysTemplate)
        } else {
            // Fallback on earlier versions
        }
        imgArrow.isUserInteractionEnabled = false
        imgArrow.tintColor = .white
        btnContinue.addSubview(imgArrow)
        imgArrow.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-20)
            make.size.equalTo(27)
        }
        
        viewBottom.addSubview(btnTerms)
        viewBottom.addSubview(btnPrivacy)
        btnTerms.snp.makeConstraints { (m) in
            m.right.equalTo(viewBottom.snp.centerX).offset(-10)
            m.top.equalTo(btnContinue.snp.bottom)
            m.bottom.equalToSuperview()
        }
        btnPrivacy.snp.makeConstraints { (m) in
            m.left.equalTo(viewBottom.snp.centerX).offset(10)
            m.top.equalTo(btnContinue.snp.bottom)
            m.bottom.equalToSuperview()
            m.width.equalTo(btnTerms.snp.width)
        }
        

        btnTerms.titleLabel?.textAlignment = .right
        btnPrivacy.titleLabel?.textAlignment = .left

        btnTerms.setTitle("Terms of Use", for: .normal)
        btnPrivacy.setTitle("Privacy Policy", for: .normal)
        
        btnTerms.setTitleColor(UIColor.darkGray.withAlphaComponent(0.7), for: .normal)
        btnPrivacy.setTitleColor(UIColor.darkGray.withAlphaComponent(0.7), for: .normal)
        
        btnTerms.addTarget(self, action: #selector(btnTermsOfUseTaapped), for: .touchUpInside)
        btnPrivacy.addTarget(self, action: #selector(btnPrivacyPolicyTaapped), for: .touchUpInside)
        
        btnTerms.titleLabel?.font = UIFont.boldSystemFont(ofSize: 11)
        btnPrivacy.titleLabel?.font = UIFont.boldSystemFont(ofSize: 11)
        
        btnPrivacy.addLeftBorderWithWidth(width: 2, color: UIColor.lightGray.withAlphaComponent(0.6), leftOffset: -11, topOffset: 10, bottomOffset: 10)

        btnContinue.addTarget(self, action: #selector(btnSubscribedTapped), for: .touchUpInside)
        btnClose.addTarget(self, action: #selector(btnCloseTaapped), for: .touchUpInside)
        btnRestore.addTarget(self, action: #selector(btnRestoreTapped), for: .touchUpInside)

        monthPlanView.addTapGesture {[weak self] (tap) in
            guard let self = self else { return }
            self.planViewDidTap(viewPlan: self.monthPlanView)
        }
        yearPlanView.addTapGesture {[weak self] (tap) in
            guard let self = self else { return }
            self.planViewDidTap(viewPlan: self.yearPlanView)
        }

        
        self.selectViewPlan(viewPlan: self.yearPlanView)
        lblTitle.text = "Unlimited Access\nTo All Features"
        
//        if self.selectedOption == .month {
//            self.selectViewPlan(viewPlan: self.monthPlanView)
//        }else if selectedOption == .year{
//            self.selectViewPlan(viewPlan: self.yearPlanView)
//        }
//        self.yearPlanView.isHidden = true

    }
    
    func updateUI()  {
        self.updatePriceInformation()

    }
    
    func updatePriceInformation()  {
        monthPlanView.lblTopFree.text = "3 days free"
        monthPlanView.lblPrice.text = "then $$$$ / month"
        monthPlanView.lblPerMonth.text = "$$$$ / month"

        yearPlanView.lblTopFree.text = "3 days free"
        yearPlanView.lblPrice.text = "then $$$$ / year"
        yearPlanView.lblPerMonth.text = "$$$$ / month"

        if let product = ISalesManager.shared.monthProduct,let price = product.localizedPrice  {
            monthPlanView.lblPrice.text = monthPlanView.lblPrice.text?.replacePrice(newWord: price)
            monthPlanView.lblPerMonth.text = monthPlanView.lblPerMonth.text?.replacePrice(newWord: price)

        }else{
            monthPlanView.lblPrice.text = monthPlanView.lblPrice.text?.replacePrice(newWord: "$11.99")
            monthPlanView.lblPerMonth.text = monthPlanView.lblPerMonth.text?.replacePrice(newWord: "$11.99")
        }
        
        if let product = ISalesManager.shared.yearProduct,let price = product.localizedPrice  {
            yearPlanView.lblPrice.text = yearPlanView.lblPrice.text?.replacePrice(newWord: price)
            let monthlyPrice = product.price.doubleValue/12
            
            
            let currencyFormatter = NumberFormatter()
            currencyFormatter.usesGroupingSeparator = true
            currencyFormatter.numberStyle = .currency
            currencyFormatter.minimumFractionDigits = 2
            currencyFormatter.maximumFractionDigits = 2
            currencyFormatter.locale = product.priceLocale
            
            if let weeklyPriceInLocal = currencyFormatter.string(from: NSNumber.init(value: monthlyPrice)){
                yearPlanView.lblPerMonth.text = yearPlanView.lblPerMonth.text?.replacePrice(newWord: weeklyPriceInLocal)
            }else{
                yearPlanView.lblPerMonth.text = yearPlanView.lblPerMonth.text?.replacePrice(newWord: "$4.00")

            }
        }else{
            yearPlanView.lblPrice.text = yearPlanView.lblPrice.text?.replacePrice(newWord: "$49.99")
            yearPlanView.lblPerMonth.text = yearPlanView.lblPerMonth.text?.replacePrice(newWord: "$4.17")
        }

        
    }
    /*
    func updatePriceInformation1()  {
        monthPlanView.lblTopFree.text = "3 days free"
        monthPlanView.lblPrice.text = "then $$$$ / month"
        monthPlanView.lblPerMonth.text = "$$$$ / month"

        yearPlanView.lblTopFree.text = "3 days free"
        yearPlanView.lblPrice.text = "then $$$$ / year"
        yearPlanView.lblPerMonth.text = "$$$$ / month"

        if let product = ISalesManager.shared.monthProduct,let price = product.localizedPrice  {
            monthPlanView.lblPrice.text = monthPlanView.lblPrice.text?.replacePrice(newWord: price)
            monthPlanView.lblPerMonth.text = monthPlanView.lblPerMonth.text?.replacePrice(newWord: price)

        }else{
            monthPlanView.lblPrice.text = monthPlanView.lblPrice.text?.replacePrice(newWord: "$11.99")
            monthPlanView.lblPerMonth.text = monthPlanView.lblPerMonth.text?.replacePrice(newWord: "$11.99")
        }
        
        if let product = ISalesManager.shared.yearProduct,let price = product.localizedPrice  {
            yearPlanView.lblPrice.text = yearPlanView.lblPrice.text?.replacePrice(newWord: price)
            let monthlyPrice = product.price.doubleValue/12
            
            
            let currencyFormatter = NumberFormatter()
            currencyFormatter.usesGroupingSeparator = true
            currencyFormatter.numberStyle = .currency
            currencyFormatter.minimumFractionDigits = 2
            currencyFormatter.maximumFractionDigits = 2
            currencyFormatter.locale = product.priceLocale
            
            if let weeklyPriceInLocal = currencyFormatter.string(from: NSNumber.init(value: monthlyPrice)){
                yearPlanView.lblPerMonth.text = yearPlanView.lblPerMonth.text?.replacePrice(newWord: weeklyPriceInLocal)
            }else{
                yearPlanView.lblPerMonth.text = yearPlanView.lblPerMonth.text?.replacePrice(newWord: "$4.00")

            }
        }else{
            yearPlanView.lblPrice.text = yearPlanView.lblPrice.text?.replacePrice(newWord: "$49.99")
            yearPlanView.lblPerMonth.text = yearPlanView.lblPerMonth.text?.replacePrice(newWord: "$11.99")
        }

        
    }
     */

    //---action------------------------------------------------
    @discardableResult
    class func PresentSubscriptionPage(_ oneController:UIViewController?) -> SalesVC {
        
        var controllerSource:UIViewController?
        
        if let oneController = oneController {
            controllerSource = oneController
        }else{
            if let controller = AppDel.getVisibleViewController(nil) {
                controllerSource = controller
            }else if let window = AppDel.window, let controller = window.rootViewController{
                controllerSource = controller
            }
        }
        
        let subscriptionVC = SalesVC()
        
        
        subscriptionVC.modalPresentationStyle = .fullScreen
        let navigationController = UINavigationController(rootViewController: subscriptionVC)
        navigationController.isNavigationBarHidden = true
        navigationController.modalPresentationStyle = .fullScreen
        controllerSource?.present(navigationController, animated: true, completion: nil)
        
        return subscriptionVC
    }
    
    func planViewDidTap(viewPlan:ViewPlan)  {
        self.selectViewPlan(viewPlan: viewPlan)
        self.btnSubscribedTapped()
    }
    func selectViewPlan(viewPlan:ViewPlan)  {
        monthPlanView.isPlanSelected = false
        yearPlanView.isPlanSelected = false
        
        viewPlan.isPlanSelected = true
        if viewPlan == monthPlanView {
            selectedOption = .month
        }else if viewPlan == yearPlanView{
            selectedOption = .year
        }

    }


    @objc func btnSubscribedTapped()  {
        
        SVProgressHUD.show()
        ISalesManager.shared.buy(self.selectedOption.productID) {
            SVProgressHUD.dismiss()
            if ISalesManager.shared.isSubscribed(){
                self.btnCloseTaapped()
            }else{
                UIAlertController.showOKAlertViewController(withTitle: "Something went wrong", message: "")
            }
        }

    }
    
    @objc func btnRestoreTapped()  {
        SVProgressHUD.show()
        ISalesManager.shared.restore {
            SVProgressHUD.dismiss()
            if ISalesManager.shared.isSubscribed(){
                self.btnCloseTaapped()
            }else{
                UIAlertController.showOKAlertViewController(withTitle: "Something went wrong", message: "")
            }
        }
    }
    @objc func btnCloseTaapped()  {
        if self.blockCompletion != nil {
            self.blockCompletion!()
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func btnTermsOfUseTaapped()  {
        if let url = URL.init(string: ConstantKey.urlTermsOfService) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }

    }
    @objc func btnPrivacyPolicyTaapped()  {
        if let url = URL.init(string: ConstantKey.urlPrivacyPolicy) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }

    }


}


class ViewLabelFeature: UIView {
    
    var lblFeature = UILabel()
    var imgBullet = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupUI()  {
        
        self.addSubview(imgBullet)
        imgBullet.snp.makeConstraints { (make) in
            make.width.height.equalTo(18)
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(2)
        }
        imgBullet.image = UIImage.init(systemName: "checkmark")
        imgBullet.tintColor = .black
        imgBullet.contentMode = .scaleToFill
        
        self.addSubview(lblFeature)
        lblFeature.snp.makeConstraints { (make) in
            make.left.equalTo(imgBullet.snp.right).offset(8)
            make.right.equalToSuperview().offset(-20)
            make.top.equalTo(2)
            make.height.greaterThanOrEqualTo(24)
            make.bottom.equalTo(-2)
        }
        lblFeature.textColor = UIColor.black
        
        lblFeature.textAlignment = .left
        lblFeature.font = UIFont.systemFont(ofSize: 17)
        lblFeature.numberOfLines = 0
        
        
    }
    
}


class ViewPlan: UIView {
    
    var lblTopFree = UILabel()
    var lblPrice = UILabel()
    var lblPerMonth = UILabel()

    var isPlanSelected:Bool = false{
        didSet{
            self.updateSelection()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupUI()  {
        
//        addSubview(lblTopFree)
//        addSubview(lblPrice)
        addSubview(lblPerMonth)
        
        let stackLeft = UIStackView.init(arrangedSubviews: [lblTopFree,lblPrice])
        addSubview(stackLeft)
        stackLeft.snp.makeConstraints { (m) in
            m.left.equalTo(20)
            m.centerY.equalToSuperview()
        }
        stackLeft.axis = .vertical
        stackLeft.spacing = 0
        
        
        addSubview(lblPerMonth)
        lblPerMonth.snp.makeConstraints { (m) in
            m.right.equalTo(-20)
            m.centerY.equalToSuperview()
            m.left.equalTo(stackLeft.snp.right)
        }
        
        self.updateSelection()

        
        self.layer.cornerRadius = 6
        self.layer.borderWidth = 2

        self.lblTopFree.isUserInteractionEnabled = false
        self.lblPrice.isUserInteractionEnabled = false
        self.lblPerMonth.isUserInteractionEnabled = false
        self.isUserInteractionEnabled = true
        
        self.lblTopFree.textAlignment = .left
        self.lblTopFree.textColor = UIColor.black
        self.lblTopFree.font = UIFont.systemFont(ofSize: 12)

        self.lblPerMonth.textAlignment = .right
        self.lblPerMonth.textColor = UIColor.black
        self.lblPerMonth.font = UIFont.systemFont(ofSize: 12)

        self.lblPrice.textAlignment = .left
        self.lblPrice.textColor = UIColor.black
        self.lblPrice.font = UIFont.boldSystemFont(ofSize: 13)

        

    }
    
    func updateSelection() {
        if self.isPlanSelected == true {
            self.layer.borderColor = UIColor.RGB(97,173,229).cgColor

        }else{
            self.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        }
    }
    
}
