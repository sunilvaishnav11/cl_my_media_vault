//
//  ContactsVC.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit
import RealmSwift

class MR_ContactsVC: SVBaseController {
    let tableContacts:UITableView = UITableView()
    var arrContact:[ContactLocal] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Contacts"
        self.addRightBarButton(image: UIImage.init(systemName: "plus")!)
        self.setupUI()
        
        tableContacts.register(CellContactInfo.self, forCellReuseIdentifier: "CellContactInfo")
        tableContacts.delegate = self
        tableContacts.dataSource = self
        tableContacts.tableFooterView = UIView()
        
        tableContacts.separatorStyle = .singleLine
        tableContacts.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        
//        SwiftMultiSelect.delegate       = self


        tableContacts.emptyDataSetSource = self
        tableContacts.emptyDataSetDelegate = self
        tableContacts.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateUI()
    }
    override func updateUI()  {
        let realm = try! Realm()
        let result = realm.objects(ContactLocal.self).filter("is_fake == \(SVSharedClass.shared.isFakePasscode)")
        self.arrContact = Array(result).sorted(by: { (obj1, obj2) -> Bool in
            return obj1.date_created! > obj2.date_created!
        })

        tableContacts.reloadData()

    }
    
    override func setupUI() {
        super.setupUI()
        
        view.addSubview(tableContacts)
        tableContacts.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
        }
        
        self.insertAddButtonUnder(title: "Add Contact", viewTop: tableContacts)

        
    }
    override func actionRightBarTapped(sender: UIBarButtonItem) {
        self.btnAddTapped()
    }
    //MARK:- --------Action---------------------------------------------------------
    override func btnAddTapped() {

        let actionController = YoutubeActionController()
        
        actionController.addAction(Action(ActionData(title: "Add New", image: UIImage(named: "add_new")!), style: .default, handler: {[weak self] action  in
            guard let self = self else { return }
            self.addNewContactTapped()
        }))
        actionController.addAction(Action(ActionData(title: "Import Contact", image: UIImage(named: "contact")!), style: .default, handler: {[weak self] action in
            guard let self = self else { return }
            self.importFromContactTapped()

        }))
        actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "close")!), style: .cancel, handler: nil))
        
        
        present(actionController, animated: true, completion: nil)
        

        
    }
    
    func addNewContactTapped()  {
        let addNew = MR_AddContactVC()
        addNew.contact = ContactLocal()
        let navi = UINavigationController.init(rootViewController: addNew)
        navi.modalPresentationStyle = .fullScreen
        self.present(navi, animated: true, completion: nil)

//        self.navigationController?.pushViewController(addNew, animated: true)
        
    }
    func importFromContactTapped()  {
       // SwiftMultiSelect.Show(to: self)
        let contactPicker = ContactsPicker(delegate: self, multiSelection:true, subtitleCellType: .phoneNumber)
        let navigationController = UINavigationController(rootViewController: contactPicker)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func cellEditTappedAt(indexPath:IndexPath)  {
        let contact = arrContact[indexPath.row]
        
        let addNew = MR_AddContactVC()
        addNew.contact = contact
        addNew.isUpdate = true
        let navi = UINavigationController.init(rootViewController: addNew)
        navi.modalPresentationStyle = .fullScreen
        self.present(navi, animated: true, completion: nil)

//        self.navigationController?.pushViewController(addNew, animated: true)

       }
    func cellDeleteTappedAt(indexPath:IndexPath)  {
        let contact = arrContact[indexPath.row]
        
        let realm = try! Realm()
        try! realm.write {
            realm.delete(contact)
        }
        self.updateUI()
    }
    



}
extension MR_ContactsVC: ContactsPickerDelegate{
    func contactPicker(_ picker: ContactsPicker, didContactFetchFailed error: NSError){
        picker.dismiss(animated: true, completion:  nil)

    }
    func contactPickerDidCancel(_ picker: ContactsPicker){
        picker.dismiss(animated: true, completion:  nil)

    }
    func contactPicker(_ picker: ContactsPicker, didSelectContact contact: Contact){
        picker.dismiss(animated: true, completion:  nil)

    }
    func contactPicker(_ picker: ContactsPicker, didSelectMultipleContacts contacts: [Contact]){
        let realm = try! Realm()

        try! realm.write {
            let arrContactLocal = contacts.map { (contact) -> ContactLocal in
                let contactLocal = ContactLocal()
                contactLocal.first_name = contact.firstName
                contactLocal.last_name = contact.lastName
                contactLocal.company_name = contact.company

                contactLocal.mobile_number = contact.phoneNumbers.count > 0 ? contact.phoneNumbers[0].phoneNumber : ""
                contactLocal.home_number =  contact.phoneNumbers.count > 1 ? contact.phoneNumbers[1].phoneNumber : ""
                contactLocal.work_number =  contact.phoneNumbers.count > 2 ? contact.phoneNumbers[2].phoneNumber : ""
                
                contactLocal.email = contact.emails.count > 0 ? contact.emails[0].email : ""
                contactLocal.url = ""
                contactLocal.note = ""

                return contactLocal
            }
            realm.add(arrContactLocal)
            
            self.updateUI()
        }
        
        picker.dismiss(animated: true, completion:  nil)
    }
}
extension MR_ContactsVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrContact.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.arrContact.count > 0,SVSharedClass.shared.launchCountValue < ConstantValue.swipeHelpShowCount {
            return 50
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.arrContact.count > 0,SVSharedClass.shared.launchCountValue < ConstantValue.swipeHelpShowCount {
            let lblTitle = UILabel()
            lblTitle.text = ConstantValue.swipeHelpText
            lblTitle.font = UIFont.systemFont(ofSize: 14)
            lblTitle.textColor = .darkGray
            lblTitle.textAlignment = .center
            lblTitle.backgroundColor = .white
            return lblTitle
        }
        return nil
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellContactInfo", for: indexPath) as! CellContactInfo
        cell.contact = arrContact[indexPath.row]
        cell.delegate = self

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contact = arrContact[indexPath.row]
        
        let addNew = MR_AddContactVC()
        addNew.contact = contact
        addNew.isForDisplayPurpose = true
        let navi = UINavigationController.init(rootViewController: addNew)
        navi.modalPresentationStyle = .fullScreen
        self.present(navi, animated: true, completion: nil)
//        self.navigationController?.pushViewController(addNew, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }

        
        let editAction = SwipeAction(style: .default, title: nil) {  [weak self]  action, indexPath in
            // handle action by updating model with deletion
            guard let self = self else { return }
            self.cellEditTappedAt(indexPath: indexPath)
        }
        editAction.image = UIImage(named: "edit_contact2")?.withRenderingMode(.alwaysTemplate)
        editAction.textColor = .black
        editAction.backgroundColor = .appBlue

        
        let deleteAction = SwipeAction(style: .destructive, title: nil) { [weak self] action, indexPath in
            // handle action by updating model with deletion
            guard let self = self else { return }
            self.cellDeleteTappedAt(indexPath: indexPath)
            
        }
        deleteAction.textColor = .black

        // customize the action appearance
        deleteAction.image = UIImage(named: "delete_trash")?.withRenderingMode(.alwaysTemplate)

        return [deleteAction,editAction]
    }
    
   
}
extension MR_ContactsVC : SwipeTableViewCellDelegate{
    
  func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .none
        options.transitionStyle = .drag
        return options
    }
}

extension MR_ContactsVC:DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let nameBuilder = AttributedStringBuilder()
        nameBuilder.text("No contacts added", attributes: [.font(UIFont.systemFont(ofSize: 30)),.textColor(.black),.alignment(.center)])
        
        return nameBuilder.attributedString

    }
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let nameBuilder = AttributedStringBuilder()
        nameBuilder.text("Start adding your\nprivate contacts", attributes: [.font(UIFont.systemFont(ofSize: 18)),.textColor(.darkGray),.alignment(.center)])
        
        return nameBuilder.attributedString

    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        if self.arrContact.count > 0 {
            return false
        }
        return true
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.init(named: "no_contact")
    }

}


class CellContactInfo: SwipeTableViewCell {
    var imgContact:UIImageView = UIImageView()
    var lblName:UILabel = UILabel()
    var lblNumber:UILabel = UILabel()
    
    var contact:ContactLocal!{
        didSet{
            updateUI()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // code common to all your cells goes here
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setupUI()   {
        contentView.addSubview(imgContact)
        contentView.addSubview(lblName)
        contentView.addSubview(lblNumber)
        
        imgContact.snp.makeConstraints { (m) in
            m.height.equalTo(self.contentView.snp.height).multipliedBy(0.75)
            m.width.equalTo(self.imgContact.snp.height)
            m.centerY.equalToSuperview().offset(2)
            m.left.equalTo(10)
        }
        lblName.snp.makeConstraints { (m) in
            m.bottom.equalTo(self.contentView.snp.centerY).offset(2)
            m.left.equalTo(imgContact.snp.right).offset(5)
        }
        lblNumber.snp.makeConstraints { (m) in
            m.top.equalTo(lblName.snp.bottom)
            m.left.equalTo(lblName.snp.left)
        }
        
        imgContact.contentMode = .scaleAspectFit
        imgContact.clipsToBounds = true
        
        lblName.textAlignment = .left
        lblName.textColor = .black
        lblName.font = UIFont.boldSystemFont(ofSize:  17)
        
        lblNumber.textAlignment = .left
        lblNumber.textColor = .RGB(138,138,145)
        lblNumber.font = UIFont.systemFont(ofSize:  15)
        
    }
    
    
    func updateUI()  {
        self.imgContact.image = UIImage.init(named: "no_profile")
        self.lblName.text = (self.contact.first_name ?? "") + " " + (self.contact.last_name ?? "")
        self.lblNumber.text = self.contact.mobile_number ?? (self.contact.home_number ?? (self.contact.work_number ?? ""))
        
    }
}
