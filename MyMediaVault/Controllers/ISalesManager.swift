//
//  ISalesManager.swift
//  MyMediaVault
//
//  Created by Macbook on 30/11/21.
//

import UIKit
import StoreKit

enum PurchaseOption {
    case month
    case year

    var productID:String{
        switch self {
        case .month:
            return ConstantKey.monthlyProductId
        case .year:
            return ConstantKey.yearlyProductId
        }
    }
}


class ISalesManager: NSObject {
    static let shared = ISalesManager()

    var allProductIDs = [ConstantKey.monthlyProductId,ConstantKey.yearlyProductId]
    var monthProduct:SKProduct?
    var yearProduct:SKProduct?

    //--------------------------------------------------------------------------
    
    func retriveProductInfo()  {
        SwiftyStoreKit.retrieveProductsInfo(Set.init(allProductIDs)) { (retrieveResults) in
            print(retrieveResults.retrievedProducts)
            if let error = retrieveResults.error {
                print("error \(error.localizedDescription)")
            }else if retrieveResults.retrievedProducts.count > 0{
                if let week = retrieveResults.retrievedProducts.first(where: { $0.productIdentifier ==  ConstantKey.monthlyProductId}){
                    self.monthProduct = week
                }
                if let year = retrieveResults.retrievedProducts.first(where: { $0.productIdentifier ==  ConstantKey.yearlyProductId}){
                    self.yearProduct = year
                }

            }
        }
    }
    func completeTransaction(completion:BlockCompletion)  {
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                // Unlock content
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
    }

    func isPlanActive(_ plan:String) -> Bool {
        return UserDefaults.standard.bool(forKey: plan)
    }
    func buy(_ plan:String,  completion: @escaping BlockCompletion) {
        SwiftyStoreKit.purchaseProduct(plan) { (purchaseResult) in
            
            if case .success(let purchase) = purchaseResult {

                SwiftyStoreKit.finishTransaction(purchase.transaction)
                self.validateSubscriptionStatus {
                    completion()
                }
                
            }else if case .error(let error) = purchaseResult{

                SVProgressHUD.dismiss()
                UIAlertController.showOKAlertViewController(withTitle: "Error", message: error.localizedDescription)
            }
            
        }
        
        
    }
    func restore(completion: @escaping BlockCompletion)  {
        SwiftyStoreKit.restorePurchases { (restoreResult) in
            self.validateSubscriptionStatus {
                completion()
            }
        }
    }
    func validateSubscriptionStatus(completion: @escaping BlockCompletion)  {
        //get receipt
        //test sandbox - production
        var service:AppleReceiptValidator.VerifyReceiptURLType = AppleReceiptValidator.VerifyReceiptURLType.production
//        if FFSharedClass.shared.isLiveApp == false {
//            service = AppleReceiptValidator.VerifyReceiptURLType.sandbox
//        }
        let appleValidator = AppleReceiptValidator(service: service, sharedSecret: ConstantKey.sharedSecretKey)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            switch result {
            case .success(let receipt):
                
                for productId in self.allProductIDs{
                    // Verify the purchase of a Subscription
                    
                    let purchaseResult = SwiftyStoreKit.verifySubscription(
                        ofType: .autoRenewable, // or .nonRenewing (see below)
                        productId: productId,
                        inReceipt: receipt)
                    print("----------------------------------------------------------------------")
                    switch purchaseResult {
                    case .purchased(let expiryDate, let items):
                        UserDefaults.standard.set(true, forKey: productId)
                        let timeRemain:Int = Int(expiryDate.timeIntervalSince1970 - Date().timeIntervalSince1970)
                        print("\(productId) is valid until \(timeRemain)\n\(items)\n")
                    case .expired(let expiryDate, let items):
                        UserDefaults.standard.set(false, forKey: productId)
                        print("\(productId) is expired since \(expiryDate)\n\(items)\n")
                        
                    case .notPurchased:
                        UserDefaults.standard.set(false, forKey: productId)
                        print("The user has never purchased \(productId)")
                    }
                    print("----------------------------------------------------------------------")
                }
                completion()
            case .error(let error):
                
                print("Receipt verification failed: \(error)")
                self.validateSubscriptionStatusSandbox(completion: completion)
                
                //completion()
            }
        }
    }
    func validateSubscriptionStatusSandbox(completion: @escaping BlockCompletion)  {
        //get receipt
        //test sandbox - production
        var service:AppleReceiptValidator.VerifyReceiptURLType = AppleReceiptValidator.VerifyReceiptURLType.sandbox
//        if FFSharedClass.shared.isLiveApp == false {
//            service = AppleReceiptValidator.VerifyReceiptURLType.sandbox
//        }
        let appleValidator = AppleReceiptValidator(service: service, sharedSecret: ConstantKey.sharedSecretKey)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            switch result {
            case .success(let receipt):
                
                for productId in self.allProductIDs{
                    // Verify the purchase of a Subscription
                    
                    let purchaseResult = SwiftyStoreKit.verifySubscription(
                        ofType: .autoRenewable, // or .nonRenewing (see below)
                        productId: productId,
                        inReceipt: receipt)
                    print("----------------------------------------------------------------------")
                    switch purchaseResult {
                    case .purchased(let expiryDate, let items):
                        UserDefaults.standard.set(true, forKey: productId)
                        let timeRemain:Int = Int(expiryDate.timeIntervalSince1970 - Date().timeIntervalSince1970)
                        print("\(productId) is valid until \(timeRemain)\n\(items)\n")
                    case .expired(let expiryDate, let items):
                        UserDefaults.standard.set(false, forKey: productId)
                        print("\(productId) is expired since \(expiryDate)\n\(items)\n")
                        
                    case .notPurchased:
                        UserDefaults.standard.set(false, forKey: productId)
                        print("The user has never purchased \(productId)")
                    }
                    print("----------------------------------------------------------------------")
                }
                completion()
            case .error(let error):
                print("Receipt verification failed: \(error)")
                completion()
            }
        }
    }
    //--------------------------------------------------------------------------
    
    func isSubscribed() -> Bool {
        //test
        return true
        
        //one day free trial
        //
//        if SVSharedClass.shared.launchCountValue <= 2 {
//            return true
//        }
        
        for productID in allProductIDs {
            if self.isPlanActive(productID) {
                return true
            }
        }
        
        return false
    }


   

}
extension String{
    func replacePrice(oldWord:String = "$$$$", newWord:String) -> String {
        return self.replacingOccurrences(of: oldWord, with: newWord)
    }
    func replaceDuration(oldWord:String = "$duration", newWord:String) -> String {
        return self.replacingOccurrences(of: oldWord, with: newWord)
    }

    private func replaceWord(oldWord:String, newWord:String) -> String {
        return self.replacingOccurrences(of: oldWord, with: newWord)
    }

}
