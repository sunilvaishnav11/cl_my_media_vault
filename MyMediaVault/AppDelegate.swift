//
//  AppDelegate.swift
//  MyMediaVault
//
//  Created by Macbook on 24/11/21.
//

import UIKit
import RealmSwift
import CoreMotion
import AudioToolbox


let AppDel = (UIApplication.shared.delegate as! AppDelegate)
let appDataFolder  = AppFolder.Documents.appending("appdata/").url

@main
class AppDelegate: UIResponder, UIApplicationDelegate,UITextFieldDelegate {

    var window: UIWindow?
    var motionManager: CMMotionManager!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //---create folder if not exist
        if FileManager.default.fileExists(atPath: appDataFolder.absoluteString) == false{
            do {
                try FileManager.default.createDirectory(at: appDataFolder, withIntermediateDirectories: true, attributes: nil)
            } catch  {
                
            }
        }

        //---count
        SVSharedClass.shared.launchCountValue = SVSharedClass.shared.launchCountValue + 1

        if SVSharedClass.shared.launchCountValue == 1 {
            SVSharedClass.shared.enableTouchID = true
            SVSharedClass.shared.enableBreakInAlert = true
            SVSharedClass.shared.enableRemoveAfterImport = true
            SVSharedClass.shared.enableFakePasscode = true
        }

        //--------------------------------------------------------------------------------
        if SVSharedClass.shared.master_recovery_key.isEmpty == true {
            SVSharedClass.shared.master_recovery_key = randomMasterKey()
        }

        //--------------------------------------------------------------------------------
        //------realm
        //URL(fileURLWithPath: RLMRealmPathForFile("default.realm")
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            fileURL:appDataFolder.appendingPathComponent("default.realm"),
            schemaVersion: 1,

            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
            })

        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        //--------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------
        IQKeyboardManager.shared.enable = true

        self.trackOrientationChanges()
        
        ToastManager.shared.position = .bottom

        
        return true
    }

    func initAppWindow()  {
        
        if ISalesManager.shared.isSubscribed() == false {
            self.gotoIntroPage()
        }else{
            self.showPasscodeSceen()
        }
    }
    func gotoIntroPage()  {
        let introvc = AppIntroVC()
        let navigtion = UINavigationController.init(rootViewController: introvc)
        navigtion.navigationBar.isTranslucent = false
        AppDel.window?.rootViewController = navigtion
        AppDel.window?.makeKeyAndVisible()

    }
    func showPasscodeSceen()  {
        if SVSharedClass.shared.passcode.isEmpty == true {
            self.showSetFirstTimePasscodeController()
        }else{
            self.showPasscodeController()
        }
    }
    
    func gotoHome()  {
        SVSharedClass.shared.isFakePasscode = false
        let home = HomeVC()
        let navigtion = UINavigationController.init(rootViewController: home)
        navigtion.navigationBar.isTranslucent = false
        AppDel.window?.rootViewController = navigtion
        AppDel.window?.makeKeyAndVisible()
    }
    func gotoFakeHome()  {
        SVSharedClass.shared.isFakePasscode = true
        let home = HomeVC()
        let navigtion = UINavigationController.init(rootViewController: home)
        navigtion.navigationBar.isTranslucent = false
        AppDel.window?.rootViewController = navigtion
        AppDel.window?.makeKeyAndVisible()

    }

    func showSetFirstTimePasscodeController(title:String? = "Set your passcode",isChaningPasscode:Bool = false)  {

        let passcodevc = PasscodeVC()
        passcodevc.vPinInput.lblText.text = title
        AppDel.window?.rootViewController = passcodevc
        AppDel.window?.makeKeyAndVisible()
        passcodevc.btnClose.isHidden = !isChaningPasscode

        passcodevc.vPinInput.blockDidEnterPin = { [weak self] passcode in
            guard let self = self else { return }
            
            //fake and original can't be same
            guard SVSharedClass.shared.fakePasscode != passcode else {
                passcodevc.vPinInput.removePin()
                passcodevc.view.makeToast("Fake passcode and original passcode should be different")
                passcodevc.vPinInput.shakeForWrongPin()
                return
            }

            passcodevc.animatePinToLeft {
                let passcodevc = PasscodeVC()
                passcodevc.btnClose.isHidden = !isChaningPasscode
                passcodevc.vPinInput.lblText.text = "Confirm your Passcode"
                AppDel.window?.rootViewController = passcodevc
                AppDel.window?.makeKeyAndVisible()
                passcodevc.btnClose.isHidden = !isChaningPasscode

                passcodevc.vPinInput.blockDidEnterPin = { [weak self] passcodeConfirm in
                    passcodevc.animatePinToLeft {
                        if passcode != passcodeConfirm {
                            self?.showSetFirstTimePasscodeController(title: "Try Again",isChaningPasscode:isChaningPasscode )
                        }else{
//                            if isChaningPasscode == false {
//                                SVSharedClass.shared.showMasterKeySaveAlert = true
//                            }
                            self?.window?.isUserInteractionEnabled = false
                            SVSharedClass.shared.passcode = passcodeConfirm
                            self?.gotoHome()
                            self?.window?.isUserInteractionEnabled = true
                            
                        }
                    }
                }
            }

        }

    }
    func showSetFakePasscodeController(title:String? = "Set Fake passcode")  {

        let passcodevc = PasscodeVC()
        passcodevc.vPinInput.lblText.text = title
        AppDel.window?.rootViewController = passcodevc
        AppDel.window?.makeKeyAndVisible()
        passcodevc.btnClose.isHidden = false

        passcodevc.vPinInput.blockDidEnterPin = { [weak self] passcode in
            guard let self = self else { return }
            
            //fake and original can't be same
            guard SVSharedClass.shared.passcode != passcode else {
                passcodevc.vPinInput.removePin()
                passcodevc.view.makeToast("Fake passcode and original passcode should be different")
                passcodevc.vPinInput.shakeForWrongPin()
                return
            }
            
            passcodevc.animatePinToLeft {
                let passcodevc = PasscodeVC()
                passcodevc.vPinInput.lblText.text = "Confirm Fake Passcode"
                AppDel.window?.rootViewController = passcodevc
                AppDel.window?.makeKeyAndVisible()
                passcodevc.btnClose.isHidden = false
                passcodevc.vPinInput.blockDidEnterPin = { [weak self] passcodeConfirm in
                    if passcode != passcodeConfirm {
                        self?.showSetFakePasscodeController(title: "Try Again")
                    }else{

                        SVSharedClass.shared.fakePasscode = passcodeConfirm
                        self?.gotoHome()
                        SVProgressHUD.showSuccess(withStatus: "Done")
                        
                        self?.doAfterDelay(inMain: 1) {
                            SVSharedClass.shared.appRequestReview()
                        }
                    }
                }

            }
        }

    }

    
    func showPasscodeController(title:String? = "Enter your Passcode")  {
        
        if let dateLocked = SVSharedClass.shared.appLockedUntil {
            if Date() <= dateLocked {
                self.showWaitUntilDateDueToFailAttempt()
                return
            }
        }
        
        let passcodevc = PasscodeVC(isCameraIncluded: true)
        passcodevc.vPinInput.lblText.text = title
        passcodevc.vPinInput.btnTouchId.isHidden = !SVSharedClass.shared.enableTouchID
        AppDel.window?.rootViewController = passcodevc
        AppDel.window?.makeKeyAndVisible()
        passcodevc.btnForgot.isHidden = false
        passcodevc.blockForgotTapped = {[weak self] in
            guard let self = self else { return }
            self.showEnterMasterRecoverKey()
        }

        passcodevc.vPinInput.blockDidEnterPin = { [weak self] passcode in
            guard let self = self else { return }
            if SVSharedClass.shared.passcode == passcode {
                //---Correct passcode----
                SVSharedClass.shared.failAttemptCount = 0
                SVSharedClass.shared.appLockedUntil = nil
                SVSharedClass.shared.waitSeconds = 0 //it will return default value 30
                self.gotoHome()
            }else{
                //---Wrong passcode-----
                passcodevc.vPinInput.removePin()
                
                SVSharedClass.shared.failAttemptCount = (SVSharedClass.shared.failAttemptCount + 1)

                if SVSharedClass.shared.enableFakePasscode == true {
                    if passcode == SVSharedClass.shared.fakePasscode{

                        AppDel.gotoFakeHome()
                        return
                    }
                }

                if SVSharedClass.shared.enableBreakInAlert == true {

                    if Device.type() != TypeIOS.iPod{
                        let breakalert = BreakInAlert()
//                        breakalert.image_path = file_name
                        breakalert.wrong_passcode = passcode
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(breakalert)
                        }

                        /*
                        passcodevc.addCameraView()
                        passcodevc.cameraVC.didFinishCapturingImage = {[weak self] image,info in
                            guard let self = self else { return }
                            let file_name = "break_in_" + randomString(length: 10) + ".jpeg"
                            let full_path = appDataFolder.appendingPathComponent(file_name)
                            if let data = image.jpegData(compressionQuality: 1.0){
                                if FileManager.write(data: data, absolutePath: full_path.path) == true{
                                    let breakalert = BreakInAlert()
                                    breakalert.image_path = file_name
                                    breakalert.wrong_passcode = passcode
                                    let realm = try! Realm()
                                    try! realm.write {
                                        realm.add(breakalert)
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                            passcodevc.cameraVC.takePicture()
                        }
                        */

                    }


                }
                                                                
                passcodevc.vPinInput.shakeForWrongPin()
                
                if SVSharedClass.shared.failAttemptCount >= 3 {
                    //detect 3 failed attempt
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {[weak self] in
                        guard let self = self else { return }
                        //go to wait screen
                        if SVSharedClass.shared.appLockedUntil != nil{
                            SVSharedClass.shared.waitSeconds = 2*SVSharedClass.shared.waitSeconds
                        }
                        let waitSeconds = SVSharedClass.shared.waitSeconds
                        SVSharedClass.shared.appLockedUntil = Date().add(waitSeconds.seconds)
                        self.showWaitUntilDateDueToFailAttempt()
                    }
                }

            }

        }
        
    }
    func showWaitUntilDateDueToFailAttempt() {
        SVSharedClass.shared.failAttemptCount = 0
        
        AppDel.window?.rootViewController = WaitUntilFailAttemptVC()
        AppDel.window?.makeKeyAndVisible()

        
    }
    func showEnterMasterRecoverKey(title:String = "Master Key") {
        let alert = UIAlertController.alertViewController(withTitle: title, message: "Enter 10-digit numeric 'Master Recovery Key' to reset passcode.")
        alert.addTextField { (txt) in
            txt.placeholder = "Master Key"
            txt.font = UIFont.systemFont(ofSize: 17)
            txt.autocorrectionType = .no
            txt.autocapitalizationType = .none
            txt.tag = 100
            txt.keyboardType = .numberPad
            txt.delegate = self
            txt.snp.makeConstraints { (make) in
                make.height.equalTo(27)
            }
        }
        alert.addCancelButton()
        alert.addButton(withTitle: "Reset Passcode") { (action) in
            if let keyEntered = alert.textFields?.first?.text{

                if SVSharedClass.shared.master_recovery_key == keyEntered{
                    //remove exisitng password and set new
                    SVSharedClass.shared.passcode = ""
                    
                    //remove passcode from all folder
                    FolderLocal.RemovePasswordFromAllFolders()
                    
                    self.showSetFirstTimePasscodeController(title: "Set your NEW passcode", isChaningPasscode: false)
                }else{
                    self.showEnterMasterRecoverKey(title: "Incorrect Master Key\nTry Again")
                }
            }
        }
        alert.show()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 100 {
            guard let preText = textField.text as NSString?,
                preText.replacingCharacters(in: range, with: string).count <= 10 else {
                return false
            }
        }
        return true
    }

    func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
        var rootVC = rootViewController
        if rootVC == nil {
            rootVC = self.window!.rootViewController
        }
        
        if rootVC?.isKind(of: UITabBarController.self) ?? false {
            let tabBarController = rootVC as! UITabBarController
            rootVC = tabBarController.selectedViewController!
        }
        
        if rootVC?.isKind(of: UIAlertController.self) ?? false {
            if let vc = (rootVC as! UIAlertController).parent{
                return vc
            }
        }
        
        if rootVC?.presentedViewController == nil {
            if (rootVC?.isKind(of: UINavigationController.self))! {
                let navigationController = rootVC as! UINavigationController
                return navigationController.viewControllers.last!
            }
            return rootVC
        }
        
        if let presented = rootVC?.presentedViewController {
            if presented.isKind(of: UINavigationController.self) {
                let navigationController = presented as! UINavigationController
                return navigationController.viewControllers.last!
            }
            
            if presented.isKind(of: UITabBarController.self) {
                let tabBarController = presented as! UITabBarController
                return tabBarController.selectedViewController!
            }
            
            return getVisibleViewController(presented)
        }
        return nil
        
    }

    func trackOrientationChanges() {
//        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
//        NotificationCenter.default.addObserver(forName: UIDevice.orientationDidChangeNotification, object: nil, queue: nil, using:
//        { notificiation in
//            if UIDevice.current.orientation == .faceDown {
//                print("Device is face down")
//                SVSharedClass.shared.doFaceDownAction()
//            } else {
//                print("Device is not face down")
//            }
//        })
        
        
        self.motionManager = CMMotionManager()
        self.motionManager.startAccelerometerUpdates(to: .main) { (p, error) in
            if error == nil{
                if let p = p {
                    if(p.acceleration.x > -0.3 && p.acceleration.x < 0.3 && p.acceleration.z < -0.95) {
//                        print("face up")
                        SVSharedClass.shared.isFaceDownDetected = false
                    }
                    else if(p.acceleration.x > -0.3 && p.acceleration.x < 0.3 && p.acceleration.z > 0.95) {
                        print("face down")
                        if SVSharedClass.shared.isFaceDownDetected == false {
                            print("Opne app ......")

                            SVSharedClass.shared.isFaceDownDetected = true
                            SVSharedClass.shared.doFaceDownAction()
                        }
                    }else{
                        SVSharedClass.shared.isFaceDownDetected = false
                    }
//                    else {
//                        print(
//                            abs( p.acceleration.y ) < abs( p.acceleration.x )
//                                ?   p.acceleration.x > 0 ? "Right"  :   "Left"
//                                :   p.acceleration.y > 0 ? "Down"   :   "Up"
//                        )
//                    }

                }
            }
        }
        
    }




    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

