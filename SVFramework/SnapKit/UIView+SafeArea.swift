//
//  UIView+SafeArea.swift
//  TrainIron
//

import Foundation
import UIKit
extension UIView {
  var safeAreaTop: ConstraintItem {
    if #available(iOS 11, *) {
      return safeAreaLayoutGuide.snp.top
    }
    return snp.top
  }
  
  var safeAreaBottom: ConstraintItem {
    if #available(iOS 11, *) {
      return safeAreaLayoutGuide.snp.bottom
    }
    return snp.bottom
  }
  
  var safeAreaLeading: ConstraintItem {
    if #available(iOS 11, *) {
      return safeAreaLayoutGuide.snp.leading
    }
    return snp.leading
  }
  
  var safeAreaTrailing: ConstraintItem {
    if #available(iOS 11, *) {
      return safeAreaLayoutGuide.snp.trailing
    }
    return snp.trailing
  }
  
  var safeAreaWidth: ConstraintItem {
    if #available(iOS 11, *) {
      return safeAreaLayoutGuide.snp.width
    }
    return snp.width
  }
  
  var safeAreaHeight: ConstraintItem {
    if #available(iOS 11, *) {
      return safeAreaLayoutGuide.snp.height
    }
    return snp.height
  }
  
  var safeAreaBoundsHeight: CGFloat {
    if #available(iOS 11.0, *) {
      return bounds.height - safeAreaInsets.top - safeAreaInsets.bottom
    }
    return bounds.height
  }
}
