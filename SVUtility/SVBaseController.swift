//
//  SVBaseController.swift
//  SecretVault
//
//  Created by Macbook on 25/07/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit
import RealmSwift

class SVBaseController: UIViewController {

    var btnAdd:LGButton?
    var btnDelete:LGButton?

    var constHeightBtnAdd:Constraint?
    
    var lblHelpNote:UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        
    }
    
    func setupUI()  {
        
    }
    func updateUI()  {
        
    }

    
    func insertAddButtonUnder(title:String,viewTop:UIView) {
        let btnAdd = LGButton()
        view.addSubview(btnAdd)
        btnAdd.snp.makeConstraints { (m) in
//            m.left.right.equalToSuperview().inset(15)
//            m.bottomMargin.equalTo(self.view.snp.bottomMargin).offset(-20)
//            self.constHeightBtnAdd = m.height.equalTo(50).constraint
            m.top.equalTo(viewTop.snp.bottom).offset(15)
            m.centerX.equalToSuperview()
            m.bottomMargin.equalTo(self.view.snp.bottomMargin).offset(-20)
            m.size.equalTo(60)
        }
        
        btnAdd.leftImageSrc = UIImage.init(systemName: "plus")//title
        btnAdd.titleColor = .white
        btnAdd.bgColor = .appBlue
//        btnAdd.leftImageSrc = nil //UIImage.init(named: "add_new")?.withRenderingMode(.alwaysTemplate)
        btnAdd.tintColor  = UIColor.white
        btnAdd.titleFontSize = 17
        btnAdd.spacingTitleIcon =  0
        btnAdd.spacingLeading = 0
        btnAdd.spacingTrailing = 0
        btnAdd.spacingTop = 0
        btnAdd.spacingBottom = 0
        btnAdd.titleFontName = "HelveticaNeue-Bold"

        btnAdd.cornerRadius = 30
        btnAdd.addTarget(self, action: #selector(btnAddTapped), for: .touchUpInside)
        self.btnAdd = btnAdd
    }
    func insertDeleteButtonUnder(title:String,viewTop:UIView) {
        let btnDelete = LGButton()
        view.addSubview(btnDelete)
        btnDelete.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview().inset(15)
            m.bottomMargin.equalTo(self.view.snp.bottomMargin).offset(-20)
            m.height.equalTo(50)
            m.top.equalTo(viewTop.snp.bottom).offset(15)
        }
        
        btnDelete.titleString = title
        btnDelete.titleColor = .white
        btnDelete.bgColor = .red
        btnDelete.leftImageSrc = nil //UIImage.init(named: "add_new")?.withRenderingMode(.alwaysTemplate)
        btnDelete.tintColor  = UIColor.white
        btnDelete.titleFontSize = 17
        btnDelete.spacingTitleIcon =  0
        btnDelete.spacingLeading = 20
        
        btnDelete.cornerRadius = 6
        btnDelete.addTarget(self, action: #selector(btnDeleteTapped), for: .touchUpInside)
        self.btnDelete = btnDelete
    }
    func insertNoteUnder(note:String,viewTop:UIView) {
        let lblNote = UILabel()
        view.addSubview(lblNote)
        lblNote.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview().inset(15)
            m.bottomMargin.equalTo(self.view.snp.bottomMargin).offset(-20)
            m.height.equalTo(30)
            m.top.equalTo(viewTop.snp.bottom).offset(15)
        }
        
        lblNote.backgroundColor = UIColor.white
        lblNote.textAlignment = .center
        lblNote.textColor = .darkGray
        lblNote.numberOfLines = 0
        lblNote.font = UIFont.systemFont(ofSize: 14)
        lblNote.text = note
        self.lblHelpNote = lblNote
    }

    func insertAddButtonUnder(title:String) {
        let btnAdd = LGButton()
        view.addSubview(btnAdd)
        btnAdd.snp.makeConstraints { (m) in
            m.left.right.equalToSuperview().inset(15)
            m.bottomMargin.equalTo(self.view.snp.bottomMargin).offset(-20)
            m.height.equalTo(50)
        }
        
        btnAdd.titleString = title
        btnAdd.titleColor = .white
        btnAdd.bgColor = .appBlue
        btnAdd.leftImageSrc = nil //UIImage.init(named: "add_new")?.withRenderingMode(.alwaysTemplate)
        btnAdd.tintColor  = UIColor.white
        btnAdd.titleFontSize = 17
        btnAdd.spacingTitleIcon =  0
        btnAdd.spacingLeading = 20
        
        btnAdd.cornerRadius = 6
        btnAdd.addTarget(self, action: #selector(btnAddTapped), for: .touchUpInside)
        self.btnAdd = btnAdd
    }
    
    
    @objc func btnAddTapped()  {
        
    }
    @objc func btnDeleteTapped()  {
        
    }





}

protocol ActionableBar {
    func actionLeftBarTapped(sender: UIBarButtonItem)
    func actionRightBarTapped(sender: UIBarButtonItem)
    
}
extension UIViewController:ActionableBar{
    
    @objc func actionLeftBarTapped(sender: UIBarButtonItem) {
        
    }
    
    @objc func actionRightBarTapped(sender: UIBarButtonItem) {
        
    }
    
    func addLeftBarButton(title:String)  {
        let btnLeftBar = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(actionLeftBarTapped(sender:)))
        self.navigationItem.leftBarButtonItem = btnLeftBar
    }
    func addRightBarButton(title:String)  {
        let btnRightBar = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(self.actionRightBarTapped(sender:)))
        self.navigationItem.rightBarButtonItem = btnRightBar
    }
    
    func addRightBarButton(image:UIImage)  {
        let btnRightBar = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.actionRightBarTapped(sender:)))
        self.navigationItem.rightBarButtonItem = btnRightBar
    }
    func addLeftBarButton(image:UIImage)  {
        let btnLeftBar = UIBarButtonItem(image: image.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(self.actionLeftBarTapped(sender:)))
        self.navigationItem.leftBarButtonItem = btnLeftBar
    }

    
    
    
    
}
