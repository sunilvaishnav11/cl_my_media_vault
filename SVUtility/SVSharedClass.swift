//
//  SVSharedClass.swift
//  SecretVault
//
//  Created by Macbook on 26/07/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit
import Photos
import BoltsSwift
import DKImagePickerController
import RealmSwift
import StoreKit


struct ConstantKey{
    //change secret key week and month

    //test
    static let sharedSecretKey = "a30b04eace06488dba7aff8c1f37254d"
    static let monthlyProductId = "monthly_pdf_printer"
    static let yearlyProductId = "yearly_pdf_printer1"

    //test
    static let app_id = "1599086375"
    
    static let urlTermsOfService = "https://docs.google.com/document/d/e/2PACX-1vQ2ciWnT32z2n0naSL49h3Pn4EGXbwFXj7T-UBUC3M2s4lOYsox9tPBt-NhVR2DEQ/pub"
    static let urlPrivacyPolicy = "https://docs.google.com/document/d/e/2PACX-1vRuh_EfrEIoN1oZddIgcpHzB-pPgUkHpahxv9f0CuGmA61vcO-qmyZI0P-yJZrKNA/pub"


}


struct ConstantValue {
   static let swipeHelpShowCount = 100
    static let swipeHelpText = "Swipe to left on row to Edit or Delete"

}
class SVSharedClass: NSObject {
    
    static let shared = SVSharedClass()
    var folder_received:FolderLocal?

//    var showMasterKeySaveAlert:Bool = false
    var homeURL:String = "https://www.google.com/"

    var isFaceDownDetected:Bool = false

    var enableRemoveAfterImport:Bool {
        set{
            UserDefaults.standard.set(newValue, forKey: "enableRemoveAfterImport")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: "enableRemoveAfterImport")
        }
    }
    var enableTouchID:Bool {
        set{
            UserDefaults.standard.set(newValue, forKey: "enableTouchID")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: "enableTouchID")
        }
    }
    var enableBreakInAlert:Bool {
        set{
            UserDefaults.standard.set(newValue, forKey: "enableBreakInAlert")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: "enableBreakInAlert")
        }
    }
    
    var launchCountValue:Int {
        set{
            UserDefaults.standard.set(newValue, forKey: "launchCount")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.integer(forKey: "launchCount")
        }
    }
    
    var facedown_option:FaceDownOption {
        set{
            UserDefaults.standard.set(newValue.rawValue, forKey: "facedown_option")
            UserDefaults.standard.synchronize()
        }
        get{
            let key = UserDefaults.standard.string(forKey: "facedown_option") ?? FaceDownOption.disable.rawValue
            return FaceDownOption.init(rawValue: key) ?? FaceDownOption.disable
        }
    }
    var passcode:String {
        set{
            UserDefaults.standard.set(newValue, forKey: "passcode")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey: "passcode") ?? ""
        }
    }
    var master_recovery_key:String {
        set{
            UserDefaults.standard.set(newValue, forKey: "master_recovery_key")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey: "master_recovery_key") ?? ""
        }
    }

    var enableFakePasscode:Bool {
           set{
               UserDefaults.standard.set(newValue, forKey: "enableFakePasscode")
               UserDefaults.standard.synchronize()
           }
           get{
               return UserDefaults.standard.bool(forKey: "enableFakePasscode")
           }
       }
    var fakePasscode:String {
        set{
            UserDefaults.standard.set(newValue, forKey: "fakePasscode")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey: "fakePasscode") ?? ""
        }
    }
    
    var failAttemptCount:Int {
        set{
            UserDefaults.standard.set(newValue, forKey: "failAttemptCount")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.integer(forKey: "failAttemptCount")
        }
    }
    var waitSeconds:Int {
        set{
            UserDefaults.standard.set(newValue, forKey: "waitSeconds")
            UserDefaults.standard.synchronize()
        }
        get{
            var seconds = UserDefaults.standard.integer(forKey: "waitSeconds")
            if seconds == 0 {
                seconds = 30
            }
            return seconds
        }
    }

    var appLockedUntil:Date? {
        set{
            UserDefaults.standard.set(newValue, forKey: "appLockedUntil")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.object(forKey: "appLockedUntil") as? Date
        }
    }

    var isFakePasscode:Bool = false

    
    func createDefaultFolderIfNeeded()  {
        let realm = try! Realm()
        let result = realm.objects(FolderLocal.self).filter("is_fake == \(SVSharedClass.shared.isFakePasscode)")
        if result.count == 0{
            
            let realm = try! Realm()
            try! realm.write {
                var folder = FolderLocal()
                folder.display_name = "Default"
                folder.can_delete = false
                folder.folder_typeEnum = .main
                realm.add(folder)
                
//                folder = FolderLocal()
//                folder.display_name = "Videos"
//                folder.can_delete = true
//                realm.add(folder)
//
                folder = FolderLocal()
                folder.display_name = "Documents"
                folder.can_delete = true
                realm.add(folder)

                folder = FolderLocal()
                folder.display_name = "Trash"
                folder.folder_typeEnum = .trash
                folder.can_delete = false
                realm.add(folder)

            }

        }
        
    }

    ///0 type for image and 1 for video: Return
    func generateNames(for Type:Int) -> (file_name: String,thumb_file_name:String,full_path:URL,thumb_full_path:URL) {
        
        if Type == 0 {
            //image
            let file_name = randomString(length: 10) + ".jpeg"
            let thumb_file_name = "thumb_" + file_name
            
            let full_path = appDataFolder.appendingPathComponent(file_name)
            let thumb_full_path = appDataFolder.appendingPathComponent(thumb_file_name)

            return (file_name,thumb_file_name,full_path,thumb_full_path)
        }
        
        let name = randomString(length: 10)
        let file_name = name + ".mov"
        let thumb_file_name = "thumb_" + name + ".jpeg"
        
        let full_path = appDataFolder.appendingPathComponent(file_name)
        let thumb_full_path = appDataFolder.appendingPathComponent(thumb_file_name)
        return (file_name,thumb_file_name,full_path,thumb_full_path)

    }
    
    func importAssetMedia(asset:DKAsset, in folder:FolderLocal) -> Task<MediaLocal?>{
        let tcs = TaskCompletionSource<MediaLocal?>()
        
        if asset.type == .photo {
            let names = self.generateNames(for: 0)
            let file_name = names.file_name
            let thumb_file_name = names.thumb_file_name
            
            let full_path = names.full_path
            let thumb_full_path = names.thumb_full_path
            
            let options = PHImageRequestOptions()
            options.isSynchronous = true
            
            asset.fetchOriginalImage(options: options) { (image, info) in
                let isDegraded = (info?[PHImageResultIsDegradedKey] as? Bool) ?? false
                if isDegraded {
                    return
                }
                
                guard let image = image,let dataImage = image.jpegData(compressionQuality: 1.0) else {
                    tcs.set(error: NSError.init(domain: "error", code: 1, userInfo: nil))
                    return
                }
                
                guard let dataThumb = Toucan.init(image: image).resize(.init(width: 400, height: 400), fitMode: .clip).image?.jpegData(compressionQuality: 1.0) else {
                    tcs.set(error: NSError.init(domain: "error", code: 1, userInfo: nil))
                    return
                }

                guard FileManager.write(data: dataThumb, absolutePath: thumb_full_path.path) == true else {
                    tcs.set(error: NSError.init(domain: "error", code: 1, userInfo: nil))
                    return
                }
                guard FileManager.write(data: dataImage, absolutePath: full_path.path) == true else {
                    tcs.set(error: NSError.init(domain: "error", code: 1, userInfo: nil))
                    return
                }

                DispatchQueue.main.async {
                    let realm = try! Realm()
                    let media = MediaLocal()
                    media.media_type = 0
                    media.thumb_image_name = thumb_file_name
                    media.media_name = file_name
                    media.folder_local = folder
                    try! realm.write {
                        realm.add(media)
                    }
                    tcs.set(result: media)
                }
            }

        }else if asset.type == .video{
            let names = self.generateNames(for: 1)
            let file_name = names.file_name
            let thumb_file_name = names.thumb_file_name
            
            let full_path = names.full_path
            let thumb_full_path = names.thumb_full_path

            let options = PHImageRequestOptions()
            options.isSynchronous = true
            
            asset.fetchImage(with: CGSize(width: 200, height: 200)) { (image, info) in
                let isDegraded = (info?[PHImageResultIsDegradedKey] as? Bool) ?? false
                if isDegraded {
                    return
                }
                
                guard let image = image,let dataThumb = image.jpegData(compressionQuality: 1.0) else {
                    tcs.set(error: NSError.init(domain: "error", code: 1, userInfo: nil))
                    return
                }
                guard FileManager.write(data: dataThumb, absolutePath: thumb_full_path.path) == true else {
                    tcs.set(error: NSError.init(domain: "error", code: 1, userInfo: nil))
                    return
                }
                
                asset.fetchAVAsset { (avasset, nil) in
                    guard let urlAsset = avasset as? AVURLAsset else {
                        tcs.set(error: NSError.init(domain: "error", code: 1, userInfo: nil))
                        return
                    }
                    let localVideoUrl = urlAsset.url
                    
                    guard let videoData = try? Data.init(contentsOf: localVideoUrl),FileManager.write(data: videoData, absolutePath: full_path.path) else {
                        tcs.set(error: NSError.init(domain: "error", code: 1, userInfo: nil))
                        return
                    }
                    DispatchQueue.main.async {
                        let realm = try! Realm()
                        let media = MediaLocal()
                        media.media_type = 1
                        media.thumb_image_name = thumb_file_name
                        media.media_name = file_name
                        media.folder_local = folder
                        try! realm.write {
                            realm.add(media)
                        }
                        tcs.set(result: media)
                    }
                }
            }

        }
        return tcs.task
    }

    func downloadAndSaveToMainFolder(image:UIImage) -> Task<MediaLocal?> {
        let tcs = TaskCompletionSource<MediaLocal?>()

        guard let main = FolderLocal.Main_folder else {
            return tcs.task
        }
        
        
        let names = self.generateNames(for: 0)
        let file_name = names.file_name
        let thumb_file_name = names.thumb_file_name
        
        let full_path = names.full_path
        let thumb_full_path = names.thumb_full_path


        guard let dataImage = image.jpegData(compressionQuality: 1.0) else {
            tcs.set(error: NSError.init(domain: "error", code: 1, userInfo: nil))
            return tcs.task
        }
        
        guard let dataThumb = Toucan.init(image: image).resize(.init(width: 400, height: 400), fitMode: .clip).image?.jpegData(compressionQuality: 1.0) else {
            tcs.set(error: NSError.init(domain: "error", code: 1, userInfo: nil))
            return tcs.task
        }

        guard FileManager.write(data: dataThumb, absolutePath: thumb_full_path.path) == true else {
            tcs.set(error: NSError.init(domain: "error", code: 1, userInfo: nil))
            return tcs.task
        }
        guard FileManager.write(data: dataImage, absolutePath: full_path.path) == true else {
            tcs.set(error: NSError.init(domain: "error", code: 1, userInfo: nil))
            return tcs.task
        }

        DispatchQueue.main.async {
            let realm = try! Realm()
            let media = MediaLocal()
            media.media_type = 0
            media.thumb_image_name = thumb_file_name
            media.media_name = file_name
            media.folder_local = main
            try! realm.write {
                realm.add(media)
            }
            tcs.set(result: media)
        }
        
        return tcs.task

    }

    
    func deleteAfterImportIfEnabled(assets:[PHAsset])  {
        if SVSharedClass.shared.enableRemoveAfterImport{
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.deleteAssets(NSArray(array: assets))
            }) { (success, error) in
                if let error = error{
                    print(error)
                }
            }
        }

    }

    
    func doFaceDownAction()  {
        switch SVSharedClass.shared.facedown_option {
        case .mail:
            self.faceDownActionOpenMail()
            break
        case .messages:
            self.faceDownActionOpenMessage()
            break
        case .music:
            self.faceDownActionOpenMusic()
            break
        case .safari:
            self.faceDownActionOpenSafari()
        case .contacts:
            self.faceDownActionOpenContacts()
        case .map:
            self.faceDownActionOpenMap()
        case .calendar:
            self.faceDownActionOpenCalendar()
        case .whatsapp:
            self.faceDownActionOpenWhatsapp()
        case .instagram:
            self.faceDownActionOpenInstagram()
        case .facebook:
            self.faceDownActionOpenFacebook()

            break
        default: break
            
        }
    }
    func faceDownActionOpenSafari()  {
        if let url = URL(string: "https://www.google.com") {
            UIApplication.shared.open(url)
        }
    }
    func faceDownActionOpenMusic()  {
        let url = URL(string: "music://")
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
    func faceDownActionOpenMail()  {
        let url = URL(string: "message://")
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)

    }
    func faceDownActionOpenMessage()  {
        if let url = URL(string: "sms://"),UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    

    func faceDownActionOpenContacts()  {
        if let url = URL(string: "contacts://"),UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    func faceDownActionOpenMap()  {
        if let url = URL(string: "map://"),UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    func faceDownActionOpenCalendar()  {
        if let url = URL(string: "calshow://"),UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    func faceDownActionOpenWhatsapp()  {
        if let url = URL(string: "whatsapp://"),UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    func faceDownActionOpenInstagram()  {
        if let url = URL(string: "instagram://"),UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    func faceDownActionOpenFacebook()  {
        if let url = URL(string: "fb://"),UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

}

extension SVSharedClass{
    func appRequestReview()  {
        //test rating. hide
        if Device.type() != TypeIOS.simulator {
            SKStoreReviewController.requestReview()
        }
    }
}
