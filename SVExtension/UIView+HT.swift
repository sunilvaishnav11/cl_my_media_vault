
import UIKit

extension UIView{
        //----top
        func addTopBorderWithHeight(height:CGFloat,color:UIColor,leftOffset:CGFloat,rightOffset:CGFloat,topOffset:CGFloat)  {
            let vBorder = UIView()
            vBorder.backgroundColor = color
            addSubview(vBorder)
            vBorder.snp.makeConstraints { (make) in
                make.left.equalTo(self).offset(leftOffset)
                make.right.equalTo(self).offset(-rightOffset)
                make.top.equalTo(self).offset(topOffset)
                make.height.equalTo(height)
            }
        }
        
        //----bottom
        func addBottomBorderWithHeight(height:CGFloat,color:UIColor,leftOffset:CGFloat,rightOffset:CGFloat,bottomOffset:CGFloat)  {
            let vBorder = UIView()
            vBorder.backgroundColor = color
            addSubview(vBorder)
            vBorder.snp.makeConstraints { (make) in
                make.left.equalTo(self).offset(leftOffset)
                make.right.equalTo(self).offset(-rightOffset)
                make.bottom.equalTo(self).offset(-bottomOffset)
                make.height.equalTo(height)
            }
        }
        
        //----left
        func addLeftBorderWithWidth(width:CGFloat,color:UIColor,leftOffset:CGFloat,topOffset:CGFloat,bottomOffset:CGFloat)  {
            let vBorder = UIView()
            vBorder.backgroundColor = color
            addSubview(vBorder)
            vBorder.snp.makeConstraints { (make) in
                make.top.equalTo(self).offset(topOffset)
                make.left.equalTo(self).offset(leftOffset)
                make.bottom.equalTo(self).offset(-bottomOffset)
                make.width.equalTo(width)
            }
        }
        
        //----right
        func addRightBorderWithWidth(width:CGFloat,color:UIColor,rightOffset:CGFloat,topOffset:CGFloat,bottomOffset:CGFloat)  {
            let vBorder = UIView()
            vBorder.backgroundColor = color
            addSubview(vBorder)
            vBorder.snp.makeConstraints { (make) in
                make.top.equalTo(self).offset(topOffset)
                make.right.equalTo(self).offset(-rightOffset)
                make.bottom.equalTo(self).offset(-bottomOffset)
                make.width.equalTo(width)
            }
        }
        

    func makeRound()  {
        self.layer.cornerRadius = self.frame.width / 2.0
        self.layer.masksToBounds = true
        
    }
    func removeRound()  {
        self.layer.cornerRadius = 0
        self.layer.masksToBounds = false
    }
    
    func showShadow()  {
//        self.layer.shadowColor = UIColor.black.cgColor
//        self.layer.shadowRadius = 10
//        self.layer.shadowOpacity = 0.2
//        self.layer.shadowOffset = CGSize.init(width: 0, height: 0)
//        self.layer.cornerRadius = 5
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2.0)
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false

    }
    func hideShadow()  {
        self.layer.shadowColor = UIColor.clear.cgColor
        self.layer.shadowRadius = 0
        self.layer.shadowOpacity = 0
        self.layer.shadowOffset = CGSize.init(width: 0, height: 0)
    }

    
    func setAnchorPoint(_ point: CGPoint) {
        var newPoint = CGPoint(x: bounds.size.width * point.x, y: bounds.size.height * point.y)
        var oldPoint = CGPoint(x: bounds.size.width * layer.anchorPoint.x, y: bounds.size.height * layer.anchorPoint.y);
        
        newPoint = newPoint.applying(transform)
        oldPoint = oldPoint.applying(transform)
        
        var position = layer.position
        
        position.x -= oldPoint.x
        position.x += newPoint.x
        
        position.y -= oldPoint.y
        position.y += newPoint.y
        
        layer.position = position
        layer.anchorPoint = point
    }
    
    func makeTopCornerRound(radius:CGFloat)  {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: radius, height: radius)).cgPath
        maskLayer.borderColor = UIColor.lightGray.cgColor
        maskLayer.borderWidth = 2.5
        layer.mask = maskLayer
    }
    func makeBottomCornerRound(radius:CGFloat)  {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: radius, height: radius)).cgPath
        maskLayer.borderColor = UIColor.lightGray.cgColor
        maskLayer.borderWidth = 2.5
        layer.mask = maskLayer
    }
    func makeTopBottomCornerRound(radius:CGFloat)  {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.topLeft, .topRight,.bottomLeft, .bottomRight], cornerRadii: CGSize(width: radius, height: radius)).cgPath
        maskLayer.borderColor = UIColor.lightGray.cgColor
        maskLayer.borderWidth = 2.5
        layer.mask = maskLayer
    }

    func removeCornerRounds (){
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [], cornerRadii: CGSize(width: 0, height: 0)).cgPath
        maskLayer.borderColor = UIColor.lightGray.cgColor
        maskLayer.borderWidth = 2.5
        layer.mask = maskLayer
    }


}
