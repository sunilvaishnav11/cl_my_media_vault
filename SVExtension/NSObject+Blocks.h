//
//  NSObject+Blocks.h
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <mach/mach.h>
#import <mach/mach_host.h>


#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_4_0
#error Blocks aren't supported if the Deployment target < 4.0
#else

typedef void (^BlockCompletion) (void);
typedef void (^BlockCompletionIndex) (NSInteger index);
typedef void (^BlockCompletionBool) (BOOL isComplete);
typedef void (^BlockCompletionBool_Array) (BOOL isComplete,NSMutableArray *arrofUsers);
typedef void (^BlockCompletionObject) (id obj);
typedef void (^BlockCompletionService) (NSInteger code ,NSString *message,id objResponse);
typedef void (^BlockProgress) (CGFloat floatProgress);

@interface NSObject (Blocks)
- (void)runBlock:(void (^)(void))block;
- (void)performAfterDelay:(NSTimeInterval)delay block:(void (^)(void))block;  ///< not tested
- (void)doAfterDelay:(NSTimeInterval)delay block:(void (^)(void))block;
- (void)doAsync:(void (^)(void))block;
- (void)doInMainThread:(void (^)(void))block;
- (void)doAsyncMainThread:(void (^)(void))block;
- (void)doAfterDelayInMain:(NSTimeInterval)delay block:(void (^)(void))block;
- (void)doUpto:(NSInteger)times AfterDelay:(NSTimeInterval)delay block:(void (^)(NSInteger index))block;

-(void)setProperty:(id)obj withKey:(NSString*)aKey;
-(id)propertyWithKey:(NSString*)aKey;
-(void)removePropertyWithKey:(NSString*)aKey;
-(NSDictionary*)dictionary;
@end

#endif




