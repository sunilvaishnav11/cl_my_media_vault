//
//  ExtensionHelper.swift
//  SecretVault
//
//  Created by Macbook on 26/07/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit

extension UIColor{
    
    class func RGB(_ red:Int,_ green:Int,_ blue:Int,_ alpha:CGFloat = 1.0) -> UIColor {
        return UIColor.init(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha:alpha)
    }
    
    class var appBlue:UIColor{
        return UIColor.RGB(26,188,156)
    }
    
    class var appLightGray:UIColor {
        return .RGB(135,136,142)
    }

    class var appBackgroundtable:UIColor {
        return .RGB(245,246,247)
    }

    

}
extension Array where Element: Equatable
{
    mutating func move(_ element: Element, to newIndex: Index) {
        if let oldIndex: Int = self.firstIndex(of: element) { self.move(from: oldIndex, to: newIndex) }
    }
}

extension Array
{
    mutating func move(from oldIndex: Index, to newIndex: Index) {
        // Don't work for free and use swap when indices are next to each other - this
        // won't rebuild array and will be super efficient.
        if oldIndex == newIndex { return }
        if abs(newIndex - oldIndex) == 1 { return self.swapAt(oldIndex, newIndex) }
        self.insert(self.remove(at: oldIndex), at: newIndex)
    }
}

extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        self.clipsToBounds = true  // add this to maintain corner radius
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: forState)
        }
    }
}

extension UIView {

    func takeScreenshot() -> UIImage {
            return UIGraphicsImageRenderer(size: bounds.size).image { _ in
          drawHierarchy(in: CGRect(origin: .zero, size: bounds.size), afterScreenUpdates: true)
        }
        
    }
    
    func takeScreenshotSquare() -> UIImage {
        let size = CGSize.init(width: self.frame.size.width, height: self.frame.size.width)
        return UIGraphicsImageRenderer(size: size).image { _ in
          drawHierarchy(in: CGRect(origin: .zero, size: size), afterScreenUpdates: true)
        }
        
    }

}
extension UIViewController {
  func isVisible() -> Bool {
    return (self.isViewLoaded == true) && self.view.window != nil
  }
}

extension UISearchBar {

    /// Returns the`UITextField` that is placed inside the text field.
    var textField: UITextField {
        if #available(iOS 13, *) {
            return searchTextField
        } else if let txt = self.value(forKey: "_searchField") as? UITextField{
            return txt
        } else if let txt = findInView(self){
            return txt
        }
        return UITextField() //fake
    }
    
    
    func findInView(_ view: UIView) -> UITextField? {
      for subview in view.subviews {
          if let textField = subview as? UITextField {
              return textField
          }
          else if let v = findInView(subview) {
              return v
          }
      }
      return nil
    }
    

}
